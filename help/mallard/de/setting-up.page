<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="setting-up">
<info>
  <link type="guide" xref="index"/>
  <link type="seealso" xref="adding-downloads"/>
  <link type="seealso" xref="remote-access"/>
  <link type="seealso" xref="news-servers"/>
  <desc>Mit dem Herunterladen zu beginnen</desc>
  <revision pkgversion="0.6" version="0.1" date="2010-09-13" status="review"/>
  <credit type="author">
    <name>Severin Heiniger</name>
    <email>severinheiniger@gmail.com</email>
  </credit>
  <copyright>
    <year>2010</year>
    <name>LottaNZB-Entwicklungsteam</name>
  </copyright>
  <license>
    <p>Creative Commons Namensnennung-Weitergabe unter gleichen Bedingungen 3.0</p>
  </license>
</info>
<title><app>LottaNZB</app> einrichten</title>
<section>
  <title>Voraussetzungen</title>
  <p>Um mit <app>LottaNZB</app> loslegen zu können, benötigen Sie:</p>
  <list>
    <item>
      <p>Ein <em>News-Server-Konto</em>, das den Zugang zum Usenet ermöglicht. Es gibt zahlreiche News-Server zu verschiedenen Preisen und unterschiedlichen <em>Rückhaltezeiten</em>. Eine Rückhaltezeit von 500 Tagen bedeutet, dass eine Datei 500 Tage nachdem Sie hochgeladen wurde, gelöscht wird.</p>
    </item>
    <item>
      <p>Eine <em>Usenet-Suchmaschine</em> mit der SIe nach Dateien im Usenet suchen und die entsprechenden NZB-Dateien herunterladen können.</p>
    </item>
  </list>
</section>
<section>
  <title>Erster Start</title>
  <p>Wenn die Anwendung zum ersten Mal gestartet wird, werden Sie gefragt, ob Sie auf Ihren Rechner herunterladen, oder stattdessen <link xref="remote-access">aus der Entfernung die Downloads eines anderen Rechner verwalten</link> möchten. Lesen Sie weiter, um herauszufinden, wie ersteres gemacht wird.</p>
  <steps>
    <item>
      <p>Für das Herunterladen von Dateien aus dem Usenet benötigt <app>LottaNZB</app> eine Anwendung namens <app>SABnzbd</app>. Wenn diese Software nicht auf Ihrem Rechner gefunden werden kann, werden Sie aufgefordert, diese zu installieren. Folgen Sie dazu den bereitgestellten Anweisungen.</p>
    </item>
    <item>
      <p>Als nächstes geben Sie die Informationen zu Ihrem News-Server-Konto ein.</p>
      <p>Normalerweise enthält die Webseite des News-Servers die Adresse und den Port, die eingegeben werden müssen. Je nach News-Server ist es möglich, die auf Ihren Rechner heruntergeladenen Daten mittels SSL zu verschlüsseln. Kreuzen Sie das entsprechende Feld an, wenn dies der Fall ist.</p>
      <p>In dem meisten Fällen muss ein Benutzername und ein Passwort eingegeben werden. Einige kostenlose News-Server erlauben den Anonymen Zugang und benötigen kein Benutzername und Passwort.</p>
    </item>
  </steps>
  <p>Ein Klick auf <gui style="button">OK</gui> bringt das Hauptfenster zum Vorschein, wo Sie Ihren <link xref="adding-downloads">ersten Download hinzufügen</link> können.</p>
  <media type="image" mime="image/png" src="figures/lottanzb-main-window.png"><app>LottaNZB</app>-Hauptfenster</media>
</section>
</page>
