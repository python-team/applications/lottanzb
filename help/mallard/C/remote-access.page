<page xmlns="http://projectmallard.org/1.0/"
      type="topic"
      id="remote-access">
<info>
  <link type="guide" xref="index#advanced-functions"/>
  <link type="guide" xref="prefs"/>
  <link type="seealso" xref="web-interface"/>
  <desc>Manage the downloads of another computer</desc>
  <revision pkgversion="0.6" version="0.1" date="2010-09-13" status="review"/>
  <credit type="author">
    <name>Severin Heiniger</name>
    <email>severinheiniger@gmail.com</email>
  </credit>
  <copyright>
    <year>2010</year>
    <name>LottaNZB Development Team</name>
  </copyright>
  <license>
    <p>Creative Commons Attribution-Share Alike 3.0</p>
  </license>
</info>
<title>Remote Access</title>
<p>It's possible to manage the downloads of another computer in the same way as
downloads to the same computer.
There are many situation where this is useful:</p>
<list>
  <item>
    <p>The computer at home is downloading and you want manage the downloads
    using your laptop while you're away.</p>
  </item>
  <item>
    <p>You want to make it possible for other people in the local network
    to <link xref="adding-downloads">add downloads</link>.</p>
  </item>
  <item>
    <p>The other computer is actually a device without a screen, such as a
    <em>Network-Attached Storage (NAS)</em>.</p>
  </item>
</list>
<p>By default, it's not possible for any other computer to manage the
downloads of your computer for the sake of your privacy.</p>
<section>
  <title>Getting Started</title>
  <p>Take the following steps to securely start using remote access.</p>
  <steps>
    <item>
      <p>On the computer where downloads are taking place, allow remote access
      by selecting <guiseq>
      <gui style="menu">Edit</gui><gui style="menuitem">Preferences</gui>
      </guiseq>, navigating to the
      <gui style="tab">Remote Access</gui> tab and activating the check-box
      <gui style="checkbox">Allow access from other computers</gui>.
      It's recommended to protect the remote access by activating
      <gui style="checkbox">Require authentication</gui> and entering
      a username and a password.</p>
      <p>Confirm your changes by closing the window.</p>
    </item>
    <item>
      <p>On the computer using which you want to manage the downloads:</p>
      <list>
        <item>
          <p>If <app>LottaNZB</app> is started for the first time, select 
          <gui style="checkbox">Manage the downloads of another computer</gui>
          in the <gui style="label">Start Downloading</gui> dialog.</p>
        </item>
        <item>
          <p>If you've already been using <app>LottaNZB</app>, select <guiseq>
          <gui style="menu">File</gui><gui style="menuitem">Monitor another
          Computer...</gui></guiseq>.</p>
        </item>
      </list>
      <p>Enter the name or IP address of the computer whose downloads you would
      like to manage. When asked, also enter the username and password
      you provided. Click <gui style="button">OK</gui> to start managing the
      downloads.</p>
    </item>
  </steps>
</section>
</page>
