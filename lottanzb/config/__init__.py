# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""
Distributed configuration system with validation support, nested sections...

This module aims to simplify the handling of configuration data in an
application. It doesn't contain any information about the actual configuration
used by the application such as its structure, default values, validation
routines etc.

Configuration files are parsed and generated using Python's ConfigParser class,
so their fully standard-compliant. Subsections, which aren't covered by the
standard are implemented using the dot character, e. g. [section.subsection].

Each section and all of its content is stored as an instance of ConfigSection.
ConfigSection lets you enforce certain option value types and specify
validation routines, but this is not obligatory.

If this module needs to create a `ConfigSection` instance, it will look for a
class named `Config` in the application module that matches the section's name.
(e. g. the class `Config` in the modes module for the section called [modes]).
If it doesn't find such a customized class, it falls back to `ConfigSection`.

The class `Config` in this module loads and saves the configuration file.

Both options and subsections can be accessed either as attributes or using
brackets. E. g.:

    print App().config.modes.active
    App().config["plugins"].categories["enabled"] = False
    App().config.save()
"""

from lottanzb.config.errors import *
from lottanzb.config.section import *
from lottanzb.config.roots import *
