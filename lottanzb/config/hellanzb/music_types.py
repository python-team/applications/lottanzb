# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

# This import statement is necessary so that loading default HellaNZB
# configuration files doesn't raise an unwanted exception.

from lottanzb.util.gobject_extras import gproperty
from lottanzb.config.section import ListConfigSection, ConfigSection

__all__ = ["Config", "MusicTypeConfig"]

class Config(ListConfigSection):
    def find_section_class(self, section):
        return MusicTypeConfig

class MusicTypeConfig(ConfigSection):
    """
    Defines a music file type and whether or not HellaNZB should attempt to
    decompress the music if it comes across this type of file.
    """
    
    extension = gproperty(type=str)
    decompressor = gproperty(type=str)
    decompressToType = gproperty(type=str)
