# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""
Contains classes that can be used to load, change and save HellaNZB
configuration files. It also contains default values, descriptions and
validation routines for all supported options.
"""

# This import statement is necessary so that loading default HellaNZB
# configuration files doesn't raise an unwanted exception.
import os

import logging
log = logging.getLogger(__name__)

from os.path import join, expanduser

from lottanzb.core.environ import _
from lottanzb.util.gobject_extras import gproperty
from lottanzb.util.misc import upper_ascii
from lottanzb.resources.xdg import DATA_HOME, USER_DIRS
from lottanzb.config.roots import LocalConfigRoot
from lottanzb.config.errors import InexistentOptionError

__all__ = ["HellaConfig"]

# The default HellaNZB data directory has been changed in order to adhere to
# the corresponding freedesktop.org standard. Existing users won't be affected.
_PREFIX_DIR = join(DATA_HOME, "hellanzb")

class Config(LocalConfigRoot):
    """
    Makes it possible to load, change and save HellaNZB configuration files.
    """
    
    _list_options = [
        "keep_file_types",
        "other_nzb_file_types",
        "not_required_file_types"
    ]
    
    prefix_dir = gproperty(
        type    = str,
        default = _PREFIX_DIR,
        nick    = "Directory prefix",
        blurb   = "This property doesn't have any effect in stand-alone mode.")
    
    queue_dir = gproperty(
        type    = str,
        default = join(_PREFIX_DIR, "nzb", "daemon.queue"),
        nick    = "Queue directory",
        blurb   = "Queued NZB files are stored here.")
    
    dest_dir = gproperty(
        type    = str,
        default = USER_DIRS.get("XDG_DOWNLOAD_DIR", expanduser("~/Downloads")),
        nick    = "Download directory",
        blurb   = "Completed downloads go here. Depending on your "
                  "configuration, they will already be validated and "
                  "extracted.")
    
    current_dir = gproperty(
        type    = str,
        default = join(_PREFIX_DIR, "nzb", "daemon.current"),
        nick    = "Current download directory",
        blurb   = "The NZB file currently being downloaded is stored here.")
    
    working_dir = gproperty(
        type    = str,
        default = join(_PREFIX_DIR, "nzb", "daemon.working"),
        nick    = "Working directory",
        blurb   = "The archive currently being downloaded is stored here.")
    
    postponed_dir = gproperty(
        type    = str,
        default = join(_PREFIX_DIR, "nzb", "daemon.postponed"),
        nick    = "Directory containing postponed downloads",
        blurb   = "Archives interrupted in the middle of downloading are "
                  "stored here temporarily.")
    
    processing_dir = gproperty(
        type    = str,
        default = join(_PREFIX_DIR, "nzb", "daemon.processing"),
        nick    = "Processing directory",
        blurb   = "Archives currently being processed are stored here. It "
                  "may contain archive directories or symbolic links to "
                  "archive directories.")
    
    processed_subdir = gproperty(
        type    = str,
        default = "processed",
        nick    = "Directory containing processed files",
        blurb   = "Sub-directory within the NZB archive directory to move "
                  "processed files to.")
    
    temp_dir = gproperty(
        type    = str,
        default = join(_PREFIX_DIR, "nzb", "daemon.temp"),
        nick    = "Temporary storage directory")
    
    max_rate = gproperty(
        type    = int,
        nick    = "Maximum download speed",
        blurb   = "Limit all server connections to the specified KB/s.")
    
    state_xml_file = gproperty(
        type    = str,
        default = join(_PREFIX_DIR, "nzb", "hellanzbState.xml"),
        nick    = "Location of HellaNZB's state file",
        blurb   = "This file is used to store HellaNZB's state when HellaNZB "
                  "isn't running. The state is intermittently written out as "
                  "XML to this file. It includes the order of the queue and "
                  "SmartPAR recovery information.")
    
    debug_mode = gproperty(
        type    = str,
        nick    = "Debug log file",
        blurb   = "Log debug messages to the specified file.")
    
    log_file = gproperty(
        type    = str,
        default = join(_PREFIX_DIR, "log"),
        nick    = "Log file",
        blurb   = "Log output to the specified file. Set to None for no "
                  "logging.")
    
    log_file_max_bytes = gproperty(
        type    = long,
        nick    = "Maximum log file size",
        blurb   = "Automatically roll over both log files when they reach "
                  "LOG_FILE_MAX_BYTES size.")
    
    log_file_backup_count = gproperty(
        type    = int,
        nick    = "Number of log files to be backed up")
    
    delete_processed = gproperty(
        type    = bool,
        default = True,
        nick    = "Remove the PROCESSED_SUBDIR if the archive was "
                  "successfully post-processed",
        blurb   = "Warning: The normal LOG_FILE should be enabled with "
                  "this option - for a record of what HellaNZB deletes.")
    
    cache_limit = gproperty(
        type    = str,
        default = "0",
        nick    = "Maximum amount of memory used to cache encoded article "
                  "data segments",
        blurb   = "HellaNZB will write article data to disk when this cache "
                  "is exceeded."
                  "Available settings: -1 for unlimited size, "
                  "0 to disable cache (only cache to disk), "
                  "> 0 to limit cache to this size, in bytes, KB, MB, "
                  "e.g. 1024 '1024KB' '100MB' '1GB'.")
    
    smart_par = gproperty(
        type    = bool,
        default = True,
        nick    = "Download PAR files only if necessary",
        blurb   = "Damaged downloads can be repaired using recovery files. "
                  "Downloading them only if necessary can significantly "
                  "decrease the data to transfer.")
    
    # Starting with LottaNZB 0.6, this module is only used to migrate existing
    # HellaNZB configurations to SABnzbd configurations. This is why it doesn't
    # matter what default values we chose for the following three options.
    # No costly calls to distutils.spawn.find_executable are actually needed.
    # SABnzbd even looks for 'unrar' and 'par2' exectables on its own.
    skip_unrar = gproperty(
        type    = bool,
        default = False,
        nick    = "Don't automatically extract downloads",
        blurb   = "Downloads regularly consists of compressed RAR archives. "
                  "HellaNZB can overtake the task of extracting the desired "
                  "files.")
    
    unrar_cmd = gproperty(
        type    = str,
        default = "",
        nick    = "Path to unrar command",
        blurb   = "The unrar application is used for automatic extraction of "
                  "downloaded files and is usually located in /usr/bin.")
    
    par2_cmd = gproperty(
        type    = str,
        default = "",
        nick    = "Path to the par2 command",
        blurb   = "The par2 application is used to check whether the "
                  "downloaded files are damaged or not and is usually located "
                  "in /usr/bin.")
    
    umask = gproperty(
        type    = int,
        nick    = "Force umask",
        blurb   = "HellaNZB inherits the umask from the current user's "
                  "environment (unless it's running in daemon mode).")
    
    max_decompression_threads = gproperty(
        type    = int,
        default = 2,
        nick    = "Maximum number of files to decompress at the same time",
        blurb   = "Please note that extracting downloaded files is a "
                  "ressource-demanding task and your system might become "
                  "unresponsive if many of these processes are running at "
                  "the same time.")
    
    xmlrpc_server = gproperty(
        type    = str,
        default = "localhost",
        nick    = "XML RPC hostname",
        blurb   = "Hostname for the XML RPC client to connect to. Defaults "
                  "to 'localhost'.")
    
    xmlrpc_password = gproperty(
        type    = str,
        default = "changeme",
        nick    = "XML RPC password",
        blurb   = "You might probably never use this, but the command line "
                  "XML RPC calls do - it should definitely be changed from "
                  "its default value. The XML RPC username is hardcoded as "
                  "'hellanzb'.")
    
    xmlrpc_port = gproperty(
        type    = int,
        default = 8760,
        minimum = 0,
        maximum = 2 ** 16 - 1,
        nick    = "XML RPC port number",
        blurb   = "Port number the XML RPC server will listen on and the "
                  "client will connect to. None for no XML RPC server.")
    
    xmlrpc_server_bind = gproperty(
        type    = str,
        default = "127.0.0.1",
        nick    = "Bind XML RPC server to IP address",
        blurb   = "IP address on which the XML RPC server will be bound "
                  "to. '0.0.0.0' for any interfaces, '127.0.0.1' will "
                  "disable remote access.")
    
    newzbin_username = gproperty(
        type    = str,
        nick    = "Newzbin.com username",
        blurb   = "Newzbin.com username for automatic NZB downloading")
    
    newzbin_password = gproperty(
        type    = str,
        nick    = "Newzbin.com password",
        blurb   = "Newzbin.com password for automatic NZB downloading")
    
    categorize_dest = gproperty(
        type    = bool,
        default = True,
        nick    = "Categorize Newzbin.com downloads",
        blurb   = "Save archives into a sub-directory of DEST_DIR named after "
                  "their Newzbin.com category  e.g. Apps, Movies, Music")
    
    macbinconv_cmd = gproperty(
        type    = str,
        nick    = "Path to the optional macbinconv command",
        blurb   = "This command is used to convert MacBinary files.")
    
    growl_notify = gproperty(
        type    = bool,
        default = False,
        nick    = "Enable Mac OS X Growl notifications")
    
    growl_server = gproperty(
        type    = str,
        default = "IP",
        nick    = "Growl notification server")
    
    growl_password = gproperty(
        type    = str,
        default = "password",
        nick    = "Growl password")
    
    libnotify_notify = gproperty(
        type    = bool,
        default = False,
        nick    = "Enable libnotify daemon notifications")
    
    disable_colors = gproperty(
        type    = bool,
        default = False,
        nick    = "Disable ANSI color codes in the main screen",
        blurb   = "Preserves the in-place scroller.")
    
    disable_ansi = gproperty(
        type    = bool,
        default = False,
        nick    = "Disable ALL ANSI color codes in the main screen",
        blurb   = "For terminals that don't support ANY ANSI codes.")
    
    nzb_zips = gproperty(
        type    = str,
        default = ".nzb.zip",
        nick    = "Support extracting NZBs from ZIP files with this suffix "
                  "in QUEUE_DIR",
        blurb   = "Defaults to '.nzb.zip'. Set to False to disable. Case "
                  "insensitive.")
    
    nzb_gzips = gproperty(
        type    = str,
        default = ".nzb.gz",
        nick    = "Support extracting NZBs from GZIP files with this suffix "
                  "in QUEUE_DIR",
        blurb   = "Defaults to '.nzb.gz'. Set to False to disable. Case "
                  "insensitive.")
    
    external_handler_script = gproperty(
        type    = str,
        nick    = "Optional external handler script",
        blurb   = "HellaNZB will run this script after having post-processed "
                  "a download.")
    
    nzbqueue_mdelay = gproperty(
        type    = float,
        default = 10.0,
        nick    = "NZB queue delay",
        blurb   = "Delay enqueueing new, recently modified NZB files added to "
                  "the QUEUE_DIR until this many seconds have passed since "
                  "the NZB's last modification time. Defaults to 10 seconds.")
    
    keep_file_types = gproperty(
        type    = object,
        nick    = "File types to keep",
        blurb   = "Don't get rid of these file types when finished "
                  "post-processing. Move them to PROCESSED_SUBDIR instead. "
                  "Case insensitive.")
    
    other_nzb_file_types = gproperty(
        type    = object,
        nick    = "Alternative NZB file extensions",
        blurb   = "List of alternative file extensions matched as NZB files "
                  "in the QUEUE_DIR. The 'nzb' file extension is always "
                  "matched.")
    
    not_required_file_types = gproperty(
        type    = object,
        nick    = "Not required file types",
        blurb   = "If any of the following file types are missing from the "
                  "archive and cannot be repaired, continue processing "
                  "because they are unimportant. Case insensitive.")
    
    servers = gproperty(type=object)
    music_types = gproperty(type=object)
    
    def __init__(self, config_file):
        LocalConfigRoot.__init__(self, config_file, "lottanzb.config.hellanzb")
    
    def __getattr__(self, key):
        """
        Makes is possible to access the options using their uppercase
        representation.
        """
        
        if key.islower():
            raise InexistentOptionError(self, key)
        else:
            return getattr(self, key.lower())
    
    def __setattr__(self, key, value):
        """
        Makes is possible to access the options using their uppercase
        representation.
        """
        
        if self.has_known_property(key.lower()):
            self.set_property(key.lower(), value)
        else:
            self.__dict__[key] = value
    
    def reset_object_options(self):
        """
        Assigns meaningful initial values to properties of type 'object'.
        
        GObject properties of type 'object' can't have default values.
        """
        
        self.keep_file_types = ["nfo", "txt"]
        self.other_nzb_file_types = []
        self.not_required_file_types = [
            "log", "m3u", "nfo", "nzb", "sfv", "txt"
        ]
    
    def get_property(self, key):
        """
        There are some HellaNZB configuration values which can be either
        integer or string for example. This method ensures that those
        properties are correctly saved.
        """
        
        value = LocalConfigRoot.get_property(self, key)
        none_keys = ["newzbin_username", "newzbin_password", "macbinconv_cmd"]
        
        if key in none_keys and not value:
            return None
        elif key in ["nzb_zips", "nzb_gzips"] and value == "False":
            return False
        elif key == "cache_limit":
            try:
                return int(value)
            except ValueError:
                pass
        
        return value
    
    def _read(self, file_object):
        """
        Loads the configuration data from the file object passed as an argument.
        """
        
        self.servers.reset()
        self.music_types.reset()
        
        def defineServer(**kwargs):
            self.servers.append(kwargs)
        
        def defineMusicType(extension, decompressor, decompressType):
            self.music_types.append({
                "extension": extension,
                "decompressor": decompressor,
                "decompressType": decompressType
            })
        
        # Syntax sugar. *g*
        Hellanzb = self # @UnusedVariable
        
        # Unfortunately, `execfile` doesn't support file objects.
        execfile(file_object.name)
        
        # LottaNZB 0.5.2 used to fix various common problems in the
        # configuration here such as the `skip_unrar` option as well as the
        # SSL flags. Starting with LottaNZB 0.6, this module is only used to
        # migrate existing HellaNZB configurations to SABnzbd configurations.
        # This is why we leave it up to the SABnzbd configuration code
        # to ensure the correctness.
    
    def _write(self, file_object):
        """
        Writes the configuration data to the file object passed as an argument.
        """    
    
        file_object.write("# -*- coding: utf-8 -*-\n")
        file_object.write("# HellaNZB configuration file - Managed by "
            "LottaNZB\n\n")
        
        # Write the `defineServer` methods.
        for server in self.servers.values():
            options = dict(server)
            del options["address"]
            del options["port"]
            
            if not options["username"]:
                options["username"] = None
            
            if not options["password"]:
                options["password"] = None
            
            args = ["%s=%s" % (key, repr(options[key])) for key in options]
            
            file_object.write("defineServer(%s)\n" % ", ".join(args))
        
        # Write the `defineMusicType` methods.
        for music_type in self.music_types.values():
            file_object.write("defineMusicType(%s, %s, %s)\n" % (
                repr(music_type.extension),
                repr(music_type.decompressor or None),
                repr(music_type.decompressToType or None)))
        
        # These preferences should be commented out 'if not value:'
        deactivable = [
            "external_handler_script",
            "macbinconv_cmd",
            "umask", 
            "unrar_cmd",
            "debug_mode"
        ]
        
        for key, value in self.get_options().items():
            nick = getattr(self.__class__, key).nick
            blurb = getattr(self.__class__, key).blurb
            
            file_object.write("\n# " + nick + "\n")
            
            if blurb:
                file_object.write("# " + blurb + "\n")
            
            if key in deactivable and not value:
                file_object.write("# ")
            
            # Don't use the locale dependent `upper` method of the key string.
            # Fixes bug #318328.
            file_object.write("Hellanzb.%s = %s\n" % \
                (upper_ascii(key), repr(value)))
    
    @property
    def newzbin_support(self):
        """
        Returns True if the configuration contains Newzbin credentials.
        
        Please note that this doesn't mean that the credentials are valid.
        """
        
        return bool(self.newzbin_username and self.newzbin_password)
    
    @property
    def remote_access(self):
        """
        Returns True if the HellaNZB daemon can be accessed from other computers
        on the network.
        """
        
        return not self.xmlrpc_server_bind in ("127.0.0.1", "localhost")
