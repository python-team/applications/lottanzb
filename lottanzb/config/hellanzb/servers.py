# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

# This import statement is necessary so that loading default HellaNZB
# configuration files doesn't raise an unwanted exception.

import logging
log = logging.getLogger(__name__)

from lottanzb.util.gobject_extras import gproperty
from lottanzb.config.section import ListConfigSection, ConfigSection

__all__ = ["Config", "ServerConfig"]

class Config(ListConfigSection):
    def set_unknown_property(self, index, server):
        # The default configuration file bundled with HellaNZB has a
        # dummy server entry which is completely useless. That's why
        # it's removed here.
        if server.id == "changeme" and server.username == "changeme":
            log.debug("Dummy server entry removed from HellaNZB configuration "
                "file.")
        else:
            ListConfigSection.set_unknown_property(self, index, server)
    
    def find_section_class(self, section):
        return ServerConfig

class ServerConfig(ConfigSection):
    id = gproperty(type=str)
    username = gproperty(type=str)
    password = gproperty(type=str)
    address = gproperty(type=str)
    port = gproperty(type=int)
    antiIdle = gproperty(type=int, default=270, minimum=0)
    idleTimeout = gproperty(type=int, default=30)
    connections = gproperty(type=int, default=8, minimum=1)
    ssl = gproperty(type=bool, default=False)
    fillserver = gproperty(type=int, default=0, minimum=0)
    enabled = gproperty(type=bool, default=True)
    skipGroupCmd = gproperty(type=bool, default=False)
    bindTo = gproperty(type=str)
    
    def _get_hosts(self):
        return ["{0.address}:{0.port}".format(self)]
    
    def _set_hosts(self, hosts):
        if hosts:
            self.address, self.port = hosts[0].split(":")
        else:
            self.address = ""
            self.port = 119
    
    hosts = gproperty(type=object, getter=_get_hosts, setter=_set_hosts)
    
    @property
    def needs_authentication(self):
        return bool(self.username and self.password)
