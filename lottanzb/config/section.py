# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import logging

LOG = logging.getLogger(__name__)

from threading import RLock

from lottanzb.util.gobject_extras import GObject, gsignal
from lottanzb.config.errors import (
    InvalidOptionError,
    InexistentOptionError,
    OptionError
)

__all__ = ["ConfigSection", "ListConfigSection"]

class ConfigSection(GObject):
    """
    Represents a configuration section and makes it possible to easily
    validate its data.
    
    Options can be stored as GObject properties, which means that they have
    a certain type and a default value. You can also override the
    set_property and get_property methods and add other custom methods.
    
    Make a subclass called "Config" in the module where the configuration data
    will be used, if the section name and the module name match, the config
    module will be able to automatically apply it.
    
    Subsections should be defined using "property(type=object)".
    Lists should be defined identically, but should be listed in the
    `_list_options` property or by using the name suffix "_list".
    """
    
    # The `property-changed` signal is emitted whenever an option or subsection
    # has been changed. Unlike the built-in `notify` event, it also covers
    # options and subsections that have not been specified using the `gproperty`
    # method.
    # Please note that this signal propagates to all parents until the root of
    # the configuration is reached.
    # Arguments:
    #     object: The very `ConfigSection` that holds the changed property.
    #     object: The key of the changed property, most likely a string.
    #     object: The value of the changed property.
    gsignal("property-changed", object, object, object)
    
    # Emitted when an unknown property is removed from the configuration
    # section. Has the same arguments as the `property-changed` signal.
    gsignal("property-deleted", object, object, object)
    
    # A list of the names of all object options that should be treated as
    # lists. Since GObject doesn't allow custom types, it's not possible to
    # use something like gproperty(type=list). Use gproperty(type=object) in
    # combination with this list instead.
    _list_options = []
    
    # Is set to True as soon as the constructor has been completely executed.
    # This means that all options passed to the constructor have been merged
    # into the configuration section.
    _initialized = False
    
    def __init__(self, name, parent=None, options=None):
        self._name = name
        self._parent = parent
        
        # Contains options and subsections that are not among the GObject
        # properties in this configuration class.
        self._unknown = {}
        
        # All functions in this list are called before the value of an option
        # is changed. The first argument is the option name and the second one
        # the new value. They may be used for validation and can raise an
        # InvalidOptionError. Otherwise a function must return the new value
        # for the option.
        # There are some predefined validation/conversion functions. But the
        # this list can be extended by subclasses or from elsewhere using the
        # `add_validation_function` method.
        self._validation_functions = [
            self._validate_dict,
            self._validate_section_parent
        ]
        
        # Contains a lock for each property.
        # The thread-safety is far from guaranteed though.
        self._locks = {}
        
        GObject.__init__(self)
        
        self.reset_object_options()
        
        if options:
            self.merge(options)
        
        # FIXME: Deserves to be more elegant. ^^ Unfortunately GObject doesn't
        # allow custom types such as ConfigSection or list, only object.
        for key, value in self.get_options().items():
            if not value and self.get_option_type(key) is object:
                if key.endswith("_list") or key in self._list_options:
                    # Make sure that list options are never None.
                    self[key] = []
                else:
                    # Seems to be a subsection.
                    self[key] = {}
        
        self._initialized = True
    
    def __eq__(self, other):
        """
        Overrides Python's built-in mechanism used to test for equality of two
        configuration sections. The section's parents may be different from
        each other, but the section names and all options and subsections must
        match.
        """
        
        try:
            return self is other or (\
                self.get_name() == other.get_name() and \
                self.get_sections() == other.get_sections() and \
                self.get_options() == other.get_options()
            )
        except AttributeError:
            return False
    
    def __ne__(self, other):
        """
        Checks if there are any differences between two configuration sections.
        """
        
        return not self.__eq__(other)
    
    def __contains__(self, key):
        """
        Checks if there's an option or subsection with a certain name.
        """
        
        return key in self.keys()
    
    def __getinitargs__(self):
        return [self._name]
    
    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self.get_full_name())
    
    def __str__(self):
        return str(self.to_dict())
    
    def __getattr__(self, key):
        """
        Called when accessing an option that hasn't been declared using
        util.gproperty. Check if the option is among the unknown options and
        return it.
        """
        
        return self.get_unknown_property(key)
    
    def __delitem__(self, key):
        """
        Makes is possible to remove an option or section that hasn't been
        declared using util.gproperty.
        """
        
        self.delete_unknown_property(key)
    
    def _get_lock(self, key):
        if not key in self._locks:
            self._locks[key] = RLock()
        
        return self._locks[key]
    
    def get_name(self):
        """Return the name of the section."""

        return self._name

    def get_parent(self):
        """Return the parent section."""

        return self._parent
    
    def keys(self):
        """
        Returns a list of all option and subsection names in this configuration
        section.
        """
        
        return GObject.keys(self) + self.get_unknown_keys()
    
    def get_option_type(self, key):
        """
        Returns the type of a certain option.
        
        If an option hasn't been declared using util.gproperty, it will
        simply return the type of the actual value.
        """
        
        try:
            return self.get_property_type(key)
        except AttributeError:
            return type(self[key])
    
    def add_validation_function(self, function):
        """
        Registers a function that is called before the value of an option or
        section is changed.
        
        Refer to the `_validation_functions` property for more information.
        """
        
        self._validation_functions.append(function)
    
    def remove_validation_function(self, function):
        """
        Does the opposite of the `add_validation_function` method.
        
        Refer to the `_validation_functions` property for more information.
        """
        
        self._validation_functions.remove(function)
    
    def get_property(self, key):
        """
        Get a certain option or subsection.
        
        Raises a InexistentOptionError instead of a plain TypeError if the
        option or subsection cannot be found.
        """
        
        with self._get_lock(key):
            try:
                return GObject.get_property(self, key)
            except TypeError:
                # The option hasn't been declared using util.gproperty.
                return self.get_unknown_property(key)
    
    def set_property(self, key, value):
        """
        Set the value of a certain option or a subsection.
        
        Returns True if the new value differs from the old one, False otherwise.
        """
        
        # Loop through all validation/conversion functions.
        for validation_function in self._validation_functions:
            value = validation_function(key, value)
        
        changed = False
        
        with self._get_lock(key):
            try:
                changed = GObject.set_property(self, key, value)
            except ValueError:
                raise InvalidOptionError(self, key)
            except (TypeError, AttributeError):
                changed = self.set_unknown_property(key, value)
            
            if changed:
                self.report_changed_property(key, value)
        
        return changed
    
    def has_known_property(self, key):
        """
        Returns True if the option specified by `key` has been declared using
        the `gproperty` method.
        """
        
        try:
            # We may not use `self.get_property` here because this would also
            # work for unknown properties stored in `self._unknown`.
            GObject.get_property(self, key)
        except TypeError:
            return False
        else:
            return True
    
    def get_unknown_keys(self):
        """
        Returns a list of keys of all options in this section that have not been
        declared using the `gproperty` method.
        """
        
        return self._unknown.keys()
    
    def get_unknown_property(self, key):
        """
        Return the value of an option specified by `key` that has not been
        declared using the `gproperty` method.
        
        Raises an `InexistentOptionError` if the option can not be found.
        
        Do not call this method directly. It will be called by the
        `get_property` method and can be overridden by subclasses.
        """
        
        try:
            return self._unknown[key]
        except KeyError:
            raise InexistentOptionError(self, key)
    
    def set_unknown_property(self, key, value):
        """
        Sets value of an option specified by `key` that has not been declared
        using the `gproperty` method.
        
        Do not call this method directly. It will be called by the
        `set_property` method and can be overridden by subclasses.
        
        Returns True if the new value differs from the old one, False otherwise.
        """
        
        if self._unknown.get(key, object()) == value:
            return False
        else:
            self._unknown[key] = value
            return True
    
    def delete_unknown_property(self, key):
        """
        Deletes an option specified by `key` that has not been declared
        using the `gproperty` method.
        """
        
        try:
            value = self[key]
            del self._unknown[key]
        except KeyError:
            raise InexistentOptionError(self, key)
        else:
            self.report_deleted_property(key, value)
        
    def move_unknown_property(self, source, dest):
        """
        Moves an option or subsection that has not been declared using the
        `gproperty` method to a new place.
        
        This method may be used to rename options or subsection and/or move
        them to other configuration sections. Please note that that they will
        be removed from the old section.
        
        The option or subsection to be moved can be specified using the first
        argument. This may either be a string representing the name of an 
        option or subsection in the current section or a relative path.
        
        The destination may either be a string, a relative path or another
        configuration section.
        """
        
        if isinstance(source, basestring):
            source_parent = self
            source_key = source
        elif isinstance(source, list):
            source_parent = self.get_by_path(source[:-1])
            source_key = source[-1]
        else:
            raise ValueError("Invalid option source: %s", source)
        
        value = source_parent.get_unknown_property(source_key)
        
        if isinstance(dest, basestring):
            self[source_key] = value
        elif isinstance(dest, list):
            self.set_by_path(dest, value)
        elif isinstance(dest, ConfigSection):
            dest[source_key] = value
        else:
            raise ValueError("Invalid option destination: %s", dest)
        
        del source_parent[source_key]
    
    def _report_property(self, signal_name, key, value):
        section = self
        
        while section is not None:
            section.emit(signal_name, self, key, value)
            section = section._parent
    
    def report_deleted_property(self, key, value):
        """
        Emits the signal `property-deleted` for a certain `key` on this section
        and all of its parents.
        """
        
        self._report_property("property-deleted", key, value)
    
    def report_changed_property(self, key, value):
        """
        Emits the signal `property-changed` for a certain `key` on this section
        and all of its parents.
        """
        
        self._report_property("property-changed", key, value)
    
    def _validate_dict(self, key, value):
        """
        Ensures that plain dictionaries are properly converted to objects of
        ConfigSection or a subclass of it, given that it can be found.
        """
        
        if isinstance(value, dict):
            cls = self.find_section_class(key) or ConfigSection
            value = cls(key, parent=self, options=value)
        
        return value
    
    # pylint: disable-msg=W0613
    def _validate_section_parent(self, key, value):
        """
        Ensures that subsections have the right parent reference.
        """
        
        if isinstance(value, ConfigSection):
            value._parent = self
        
        return value
    
    def merge(self, options):
        """
        Merge the content of a dictionary-like object into the object.
        
        Please note that this method tries to preserve existing ConfigSection
        objects, so that references to them don't break.
        """
        
        for key, value in options.items():
            try:
                assert isinstance(self[key], ConfigSection)
            except (AssertionError, OptionError):
                self[key] = value
            else:
                self[key].merge(value)
    
    def get_sections(self):
        """Returns a dictionary containing all subsections of this section"""
        
        sections = {}
        
        for key in self:
            value = self[key]
            
            if isinstance(value, ConfigSection):
                sections[key] = value
        
        return sections
    
    def get_options(self):
        """Returns a dictionary containing all options in this section"""
        
        options = {}
        
        for key in self:
            value = self[key]
            
            if not isinstance(value, ConfigSection):
                options[key] = value
        
        return options
    
    def add_section(self, section):
        """
        Adds an already created ConfigSection object to this configuration
        section.
        
        The `parent` property of `section`  will be correctly set.
        """
        
        self[section._name] = section
    
    def apply_section_class(self, name, cls):
        """
        Manually apply a custom ConfigSection class to this section.
        
        This method is only meant to be used if this module's mechanism of
        looking up custom ConfigSection classes won't be able to find it
        automatically.
        """
        
        if name in self.get_sections():
            if self[name].__class__ is cls:
                # The subsection is already an instance of this class.
                # We've got nothing to do.
                return
            
            section = cls(name, parent=self, options=self[name])
        else:
            section = cls(name, parent=self)
        
        self[name] = section
    
    def get_by_path(self, path):
        """
        Returns the value of a nested option based on its path.
        
        The following two lines of code return the same:
        config.get_by_path(["modes", "active"])
        config.modes.active
        
        Please note that calling `get_by_path([])` on a `ConfigSection` will
        return the section itself.
        """
        
        if path:
            if len(path) == 1:
                return self[path[0]]
            else:
                return self[path[0]].get_by_path(path[1:])
        else:
            return self
    
    def set_by_path(self, path, value):
        """
        Sets the value of a nested option based on its path.
        
        The following two lines of code do the same:
        config.set_by_path(["modes", "active"], "standalone")
        config.modes.active = "standalone"
        """
        
        self.merge(self.path_to_dict(path, value))
    
    def create_path(self, path):
        """
        Ensure that the passed path exists where the latest name in the path
        will be an empty subsection.
        """
        
        self.set_by_path(path, {})
    
    def __deepcopy__(self, memo):
        """
        Creates a deep copy of this section.
        
        Please note that the section passed to `deepcopy` won't have a parent,
        even if the original does. The parent is not copied.
        The `_parent` property of all subsections will be set properly.
        """
        
        memo.setdefault(id(self._locks), {})
        memo.setdefault(id(self._parent), None)
        
        # Work around Python bug #1515: deepcopy doesn't copy instance methods
        # See http://bugs.python.org/issue1515 for more information
        # Using Python <= 2.6, the process of deepcopying fails when trying to
        # copy the `_validation_functions` list property.
        # That's why the list content is excluded here.
        for function in self._validation_functions:
            memo[id(function)] = function
        
        return GObject.__deepcopy__(self, memo)
    
    def reset(self):
        """
        Changes all options back to their default values and removes any unknown
        options.
        """
        
        self._unknown = {}
        
        for option in self.get_options():
            self[option] = self.get_property_default_value(option)
        
        for section in self.get_sections():
            self[section].reset()
        
        self.reset_object_options()
    
    def reset_object_options(self):
        """
        Changes all object options (only lists at this point of time) to their
        default values.
        
        Subclasses are meant to override this method.
        """
        
        pass
    
    def get_path(self):
        """Returns a list of the names of all parent sections."""
        
        sections = [self._name]
        section = self
        
        while section._parent is not None:
            section = section._parent
            sections.append(section._name)
        
        sections.reverse()
        
        return sections
    
    def get_full_name(self):
        """Returns the complete section name"""
        
        return self.path_to_name(self.get_path())
    
    def get_depth(self):
        """Returns the number of parent sections"""
        
        section = self
        depth = 0
        
        while section._parent is not None:
            section = section._parent
            depth += 1
        
        return depth
    
    def to_dict(self):
        """Returns a simple dictionary version of the config section."""
        
        result = self.get_options()
        
        for key, value in self.get_sections().items():
            result[key] = value.to_dict()
        
        return result
    
    @staticmethod
    def path_to_dict(path, value):
        """
        Turns a path into a nested dictionary.
        
        Example:
        
        Config.path_to_dict(["modes", "active"], "standalone")
         => { 'modes' : { 'active' : 'standalone' } }
        """
        
        path = path[:]
        option = { path.pop(): value }
        
        path.reverse()
        
        for section in path:
            option = { section: option }
        
        return option
    
    @staticmethod
    def path_to_name(path):
        """Build a full section name out of a list of section names"""
        
        return ".".join(map(str, path))
    
    @staticmethod
    def name_to_path(name):
        """Build a path out of a full section name."""
        
        return name.split(".")
    
    def find_section_class(self, section):
        """
        Checks for a custom ConfigSection class based on the section's name.
        
        If the section name is "modes.local_frontend" for example, it tries to
        import the module with the same name and checks if it contains a
        ConfigSection subclass called "Config".
        
        Returns None if no matching class could be found.
        """
        
        name = self.path_to_name(self.get_path() + [section])
        
        try:
            module = __import__(name, globals(), locals(), ["Config"], -1)
            config_cls = module.Config
            
            assert issubclass(config_cls, ConfigSection)
        except (ImportError, AttributeError, AssertionError):
            LOG.debug("Could not find configuration class '%s'.", name)
        else:
            return config_cls

class ListConfigSection(ConfigSection):
    """
    Exposes the methods of a common `list` object and mimics its behaviour.
    
    The most important difference is that clients of this class cannot iterate
    over the list elements using
    
        for element in list_config_section
    
    The desired effect can be achieved using
    
        for element in list_config_section.values()
    
    Besides that it's possible to do anything that can be done with a regular
    list: Access by index using `list_config_section[i]` etc.
    """
    
    def __init__(self, name, parent=None, options=None):
        self._list_items = []
        
        ConfigSection.__init__(self, name, parent, options)
    
    def get_unknown_keys(self):
        """
        Returns a list of all keys of unknown properties and all indices used in
        this section.
        """
        
        indices = range(0, len(self._list_items))
        
        return ConfigSection.get_unknown_keys(self) + indices
    
    def get_unknown_property(self, key):
        """
        Makes sure that the list elements can be accessed using their index, no
        matter if it's a string or an integer.
        """
        
        try:
            return self._list_items[int(key)]
        except ValueError:
            return ConfigSection.get_unknown_property(self, key)
        except IndexError:
            raise InexistentOptionError(self, key)
    
    def set_unknown_property(self, key, value):
        """
        Makes sure that the list elements can be accessed using their index, no
        matter if it's a string or an integer.
        
        Returns True if the new value differs from the old one, False otherwise.
        """
        
        try:
            index = int(key)
            
            # Make sure the name is an integer
            value._name = index
            
            if (len(self._list_items) == index):
                self._list_items.append(value)
            else:
                self._list_items[index] = value
            
            return True
        except ValueError:
            return ConfigSection.set_unknown_property(self, value, key)
        except IndexError:
            raise InvalidOptionError(self, key)
    
    def delete_unknown_property(self, key):
        try:
            self.remove(self.get_unknown_property(key))
        except ValueError:
            ConfigSection.delete_unknown_property(self, key)
    
    def __len__(self):
        """Returns the number of elements in the list."""
        
        return len(self._list_items)
    
    def reset(self):
        """Removes all elements from the list."""
        
        self._list_items = []
        
        ConfigSection.reset(self)
    
    def append(self, value):
        """Add a new subsection to the end of the list."""
        
        self[len(self._list_items)] = value
    
    def extend(self, values):
        """Add an array of subsections to the end of the list."""
        
        for value in values:
            self.append(value)
    
    def insert(self, index, value):
        """Insert a subsection at a certain index of the list."""
        
        if index >= len(self._list_items):
            self.append(value)
        else:
            tmp_value = self[index]
            
            self._list_items.insert(index + 1, tmp_value) 
            self._update_subsection_names(index + 1)
            
            self[index] = value
    
    def remove(self, value):
        """Removes a certain subsection from the list."""
        
        self._list_items.remove(value)
        self._update_subsection_names()
        self.report_deleted_property(value._name, value)
    
    def pop(self, index=0):
        """
        Removes the subsection with a certain index (defaults to 0) from the
        list and returns it.
        """
        
        value = self._list_items.pop(index)
        self._update_subsection_names()
        self.report_deleted_property(index, value)
        
        return value
    
    def index(self, value):
        """Returns the index of a certain subsection."""
        
        return self._list_items.index(value)
    
    def to_list(self):
        """Returns all items as a simple `list`."""
        
        return self._list_items[:]
    
    def _update_subsection_names(self, start_index=0):
        """
        Internal method that is used to update the names of the subsection
        instances when their index in the list has changed.
        
        This is a little hackish an inefficient but it's hard to remove the
        `_name` property from the `ConfigSection` class.
        """
        
        for index in range(start_index, len(self._list_items)):
            self[index]._name = index
