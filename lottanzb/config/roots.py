# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import re
import errno
import logging

LOG = logging.getLogger(__name__)

from os import makedirs
from os.path import dirname, isdir
from copy import deepcopy
from configobj import ConfigObj as _ConfigObj, ConfigObjError

# The `current_thread` property is only available in Python >= 2.6.
from threading import RLock, current_thread

from lottanzb.core.environ import _
from lottanzb.util.gobject_extras import gsignal
from lottanzb.config.section import ConfigSection
from lottanzb.config.errors import (
    OptionError,
    InexistentOptionError,
    ConfigNotFoundError,
    LoadingError,
    SavingError,
    ParsingError
)

__all__ = [
    "ConfigRoot",
    "RemoteConfigRoot",
    "LocalConfigRoot",
    "IniConfigRoot",
    "IniPathConfigRoot",
    "ConfigObjRoot"
]

class ConfigRoot(ConfigSection):
    def __init__(self, target, name="", options=None):
        ConfigSection.__init__(self, name, options=options)
        
        self.target = target
    
    def __getinitargs__(self):
        return [self.target] + ConfigSection.__getinitargs__(self)


class RemoteConfigRoot(ConfigRoot):
    def __init__(self, host, name="", options=None):
        self.host = host
        
        self._clean_config = None
        self._merge_thread = None
        self._merge_lock = RLock()
        
        ConfigRoot.__init__(self, self.host, name, options=options)
    
    def _get_clean_section(self, section):
        return self._clean_config.get_by_path(section.get_path())
    
    def do_property_changed(self, section, key, value):
        """
        Carefully check if it's necessary to write the change in the
        configuration back to the configuration host. If so,
        `write_back_change` will be called.
        
        A change only needs to be written back if it actually originates
        from the user and not if originates from the configuration host
        (as part of a merge operation). This is used to avoid useless queries.
        """
        
        if self._initialized:
            if self._merge_thread:
                if self._merge_thread is not current_thread():
                    try:
                        clean_section = self._get_clean_section(section)
                        clean_section[key] = deepcopy(value)
                    except InexistentOptionError:
                        pass
                    
                    self.write_back_change(section, key, value)
            else:
                self.write_back_change(section, key, value)
    
    def do_property_deleted(self, section, key, value):
        """
        Carefully check if it's necessary to write the deletion in the
        configuration back to the configuration host. If so,
        `write_back_deletion` will be called.
        
        A deletion only needs to be written back if it actually originates
        from the user and not if originates from the configuration host
        (as part of a merge operation). This is used to avoid useless queries.
        """
        
        if self._initialized:
            if self._merge_thread:
                if self._merge_thread is not current_thread():
                    try:
                        clean_section = self._get_clean_section(section)
                        clean_section.delete_unknown_property(key)
                    except InexistentOptionError:
                        pass
                    
                    self.write_back_deletion(section, key, value)
            else:
                self.write_back_deletion(section, key, value)
    
    def load(self):
        raise NotImplementedError
    
    def merge(self, config):
        if isinstance(config, RemoteConfigRoot) and self.host == config.host:
            self._merge_lock.acquire()
            self._merge_thread = current_thread()
            self._clean_config = config
            
            try:
                ConfigRoot.merge(self, config)
            finally:
                self._clean_config = None
                self._merge_thread = None
                self._merge_lock.release()
        else:
            ConfigRoot.merge(self, config)
    
    def write_back_change(self, section, key, value):
        """
        Write a change in the configuration back to the configuration host.
        
        This method needs to be implemented by subclasses.
        """
        
        raise NotImplementedError
    
    def write_back_deletion(self, section, key, value):
        """
        Write a deletion in the configuration back to the configuration host.
        
        This method needs to be implemented by subclasses.
        """
        
        raise NotImplementedError
    
    def __deepcopy__(self, memo):
        """
        Don't start deepcopying the configuration until a merge operation is
        complete. Also, it's not possible to make a deepcopy of `Lock` objects.
        This is taken care of by this method.
        """
        
        with self._merge_lock:
            memo[id(self._merge_lock)] = RLock()
            
            return ConfigRoot.__deepcopy__(self, memo)


class LocalConfigRoot(ConfigRoot):
    """
    Represents a configuration file and its data. This class provides various
    abstract methods that need to be implemented by subclasses.
    
    It takes care of raising standardized exceptions when something bad happens
    while loading or saving the configuration data.
    
    This class supports the usage of a configuration managers, as described
    in the config.manager module.
    """
    
    gsignal("save")
    gsignal("load", str)
    
    def __init__(self, config_file, name=""):
        ConfigRoot.__init__(self, config_file, name)
        
        self.config_file = config_file
        
        # Set to True if the configuration has been changed but not saved yet.
        # It's also set to True before the configuration has been loaded the
        # first time.
        self.is_dirty = True
        
        self.connect("property-changed", self.on_config_changed)
        self.connect("property-deleted", self.on_config_changed)
    
    def load(self, custom_config_file=""):
        """
        Loads the configuration data from the file specified in the
        `config_file` property. Alternatively, it's possible to override it.
        
        Subclasses are required to implement the `_read` method, which is
        called by this method.
        
        Emits the signal "load" and temporarily sets the `loading` property to
        True.
        
        Please note that this method does not reset any options or sections to
        their default values. Users of this class may want to do that by
        invoking the `reset` method on this section or selected subsections.
        """
        
        config_file = custom_config_file or self.config_file
        
        try:
            file_object = open(config_file, "r")
        except IOError as error:
            if error.errno == errno.ENOENT:
                raise ConfigNotFoundError(self)
            else:
                raise LoadingError(self, error.strerror)
        else:
            self.handler_block_by_func(self.on_config_changed)
            self._read(file_object)
            self.handler_unblock_by_func(self.on_config_changed)
            self.is_dirty = False
            self.emit("load", config_file)
    
    def _read(self, file_object):
        """
        Loads the configuration data from the file object passed as an argument.
        
        Subclasses need to implement this method.
        """
        
        raise NotImplementedError
    
    def save(self):
        """
        Saves the configuration to the file specified in the `config_file`
        property.
        
        Subclasses are required to implement the `_write` method, which is
        called by this method.
        
        Emits the signal "save".
        """
        
        try:
            # Make sure that the destination directory exists.
            # Other wise, calling the `open` method will fail.
            if not isdir(self.config_dir):
                makedirs(self.config_dir)
                
                LOG.info(_("Configuration directory %s created."),
                    self.config_dir)
            
            file_object = open(self.config_file, "w")
        except IOError as error:
            raise SavingError(self, error.strerror)
        
        self._write(file_object)
        
        file_object.close()
        LOG.info(_("Configuration file %s saved."), self.config_file)
        
        self.emit("save")
    
    def _write(self, file_object):
        """
        Writes the configuration data to the file object passed as an argument.
        
        Subclasses need to implement this method.
        """
        
        raise NotImplementedError
    
    def on_config_changed(self, *args):
        """Sets the `is_dirty' property."""
        
        self.is_dirty = True
    
    @property
    def config_dir(self):
        """Return the directory in which the configuration file is located."""
        
        return dirname(self.config_file)


class IniConfigRoot(LocalConfigRoot):
    """Abstract root configuration section that loads and saves INI-style
    configuration files.
    """
    
    def __init__(self, config_file, base_module=None):
        name = base_module
        
        if base_module is None:
            name = self.path_to_name(self.__module__.split(".")[:-1])
        elif isinstance(base_module, list):
            name = self.path_to_name(base_module)
        
        LocalConfigRoot.__init__(self, config_file, name)


class IniPathConfigRoot(IniConfigRoot):
    """Root configuration section that loads and saves INI-style
    configuration files.
    
    Section nesting is achieved using paths, as seen in the following example.
    
    [gui]
    [gui.main]
    """
    
    OPTION_PATTERN = re.compile(
        r"(?P<option>[^:=\s][^:=]*?)" # Very permissive!
        r"\s*(?P<separator>[:=])\s*"  # Any number of space/tab, followed by a
                                      # separator (either : or =), followed by
                                      # any space/tab
        r"(?P<value>.*)$")            # Everything up to the end of the line
    
    def get_sections_flat(self, section=None):
        """Return a list of all sections of any depth.
        
        The configuration root section is not included in the list.
        """
        
        if section is None:
            section = self
            result = []
        else:
            result = [section]
        
        for sub_section in section.get_sections().values():
            result.extend(self.get_sections_flat(sub_section))
        
        return result
    
    def _write(self, file_object):
        options = self.get_options()
        
        for section in self.get_sections_flat():
            relative_path = section.get_path()[1:]
            options[self.path_to_name(relative_path)] = section.get_options()
        
        config = ConfigObj()
        config.merge(options)
        
        for section in config.sections:
            config.comments[section] = [""]
        
        config.write(file_object)
    
    def _read(self, file_object):
        lines = self._to_config_obj_format(file_object.readlines())
        
        try:
            config = ConfigObj(lines)
            config.filename = file_object.name
        except ConfigObjError as error:
            error = error.errors[0]
            
            raise ParsingError(self, error.line_number, error.message)
        
        for option_name in config.scalars:
            try:
                self[option_name] = config[option_name]
            except OptionError as option_error:
                LOG.warning(str(option_error))
        
        for section_name in config.sections:
            path = self.name_to_path(section_name)
            options = config[section_name].dict()
            
            try:
                self.set_by_path(path, options)
            except OptionError as option_error:
                LOG.warning(str(option_error))
    
    @classmethod
    def _to_config_obj_format(cls, lines):
        """Transform the lines of a configuration file to the ConfigObj syntax.
        
        Previous versions of this class used multi-line options to store list
        values, such as:
        
        categories = 
            Foo
            Bar
        
        Not being valid ConfigObj syntax, this option needs to be transformed to
        
        categories = Foo, Bar
        """
        
        result = []
        index = 0
        
        while index < len(lines):
            line = lines[index]
            match = cls.OPTION_PATTERN.match(line)
            index += 1
            
            if match and match.group("value") == "":
                option = match.group("option")
                items = []
                
                while index < len(lines) and lines[index].startswith("\t"):
                    items.append(lines[index][1:-1])
                    index += 1
                
                # TODO: ',' is not escaped by putting '"' around such an item.
                result.append("%s = %s\n" % (option, ", ".join(items)))
            else:
                result.append(line)
        
        return result


class ConfigObjRoot(IniConfigRoot):
    """Root configuration section that loads and saves INI-style
    configuration files according to the ConfigObj format.
    
    Section nesting is achieved using multiple opening and closing square
    brackets, as seen in the following example.
    
    [gui]
    [[main]]
    """
    
    # The `ConfigObj' object stored between a reading and writing operation,
    # helping to preserve the appearance of existing configuration files.
    _config_obj = None
    
    def _read(self, file_object):
        try:
            self._config_obj = ConfigObj(file_object)
        except ConfigObjError as error:
            error = error.errors[0]
            
            raise ParsingError(self, error.line_number, error.message)
        
        self.merge(self._config_obj.dict())
    
    def _write(self, file_object):
        if not self._config_obj:
            self._config_obj = ConfigObj()
        
        # ConfigObj represents boolean values as 'True' and 'False' in the
        # configuration file. This format is however not recognized when loading
        # such a configuration file using SABnzbd. All boolean values will
        # default to False. SABnzbd seems to expect 0 for False and 1 for True.
        # This is why the configuration is transformed before being saved.
        def transform_config_dict(section_dict):
            for key, value in section_dict.items():
                if isinstance(value, dict):
                    transform_config_dict(value)
                elif isinstance(value, bool):
                    section_dict[key] = int(value)
        
        config_dict = self.to_dict()
        transform_config_dict(config_dict)
        
        self._config_obj.merge(config_dict)
        self._config_obj.write(file_object)


class ConfigObj(_ConfigObj):
    """Version of `configobj.ConfigObj' with minor modifications.
    
    The method `merge' will cause new scalars and sections to be added
    in alphabetical order, rather than randomly. The order of existing items
    is not affected.
    """
    
    def merge(self, options):
        self._merge_helper(self, options)
    
    def _merge_helper(self, section, options):
        for option in sorted(options.keys()):
            if isinstance(options[option], dict):
                section[option] = {}
                
                # `sub_section' will be of type `configobj.Section'.
                sub_section = section[option]
                
                self._merge_helper(sub_section, options[option])
            else:
                section[option] = options[option]
    
    def __deepcopy__(self, memo):
        result = memo.setdefault(id(self), ConfigObj())
        
        for key in self.__dict__:
            result.__dict__[key] = deepcopy(self.__dict__[key], memo)
        
        return result
