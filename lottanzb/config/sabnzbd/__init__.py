# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import time
import random

from hashlib import md5
from os.path import expanduser

from lottanzb.core.constants import PostProcessing
from lottanzb.util.gobject_extras import gproperty
from lottanzb.config.errors import *

from lottanzb.config import (
    ConfigObjRoot,
    RemoteConfigRoot,
    ListConfigSection as _ListConfigSection,
    ConfigSection
)

__all__ = [
    "ConfigMixin",
    "LocalConfig",
    "AbstractRemoteConfig",
    "ListConfigSection",
    "ListElementConfigSection",
    "create_api_key"
]

DEFAULT_CONFIG_FILE = expanduser("~/.sabnzbd/sabnzbd.ini")

class ConfigMixin(object):
    def import_hellanzb_config(self, hella_config):
        """
        Copies all options part of the intersection of the HellaNZB and SABnzbd
        configuration data and overwrites the current values in the SABnzbd
        configuration.
        """
        
        uses_default_password = hella_config.xmlrpc_password == \
            hella_config.get_property_default_value("xmlrpc_password")
        
        if hella_config.remote_access and not uses_default_password:
            self.misc.username = hella_config.xmlrpc_username
            self.misc.password = hella_config.xmlrpc_password
            self.misc.host = hella_config.xmlrpc_server_bind
        
        self.misc.bandwidth_limit = hella_config.max_rate
        self.misc.complete_dir = hella_config.dest_dir
        
        if hella_config.skip_unrar:
            self.misc.enable_unrar = False
            self.misc.enable_unzip = False
            self.misc.dirscan_opts = PostProcessing.REPAIR
        
        if hella_config.log_file_max_bytes:
            self.logging.max_log_size = hella_config.log_file_max_bytes
        
        self.logging.log_backups = hella_config.log_file_backup_count
        
        if hella_config.newzbin_support:
            self.newzbin.username = hella_config.newzbin_username
            self.newzbin.password = hella_config.newzbin_password
        
        for server in hella_config.servers.values():
            self.servers.append({
                "username": server.username,
                "password": server.password,
                "enable": server.enabled,
                "fillserver": server.fillserver,
                "connections": server.connections,
                "ssl": server.ssl,
                "timeout": server.idleTimeout,
                "host": server.address,
                "port": server.port
            })

class LocalConfig(ConfigMixin, ConfigObjRoot):
    """
    Root `ConfigSection` that loads and saves SABnzbd configuration files.
    """
    
    # All subsections
    misc = gproperty(type=object)
    logging = gproperty(type=object)
    newzbin = gproperty(type=object)
    nzbmatrix = gproperty(type=object)
    categories = gproperty(type=object)
    servers = gproperty(type=object)
    
    def __init__(self, config_file=DEFAULT_CONFIG_FILE):
        ConfigObjRoot.__init__(self, config_file, "lottanzb.config.sabnzbd")
    
    def __getinitargs__(self):
        return [self.config_file]
    
    def _get_section_identifier(self, section):
        """
        Generates the string that will actually be used to identify a
        particular section in the configuration file.
        """
        
        brackets = section.get_depth() - 1
        
        try:
            identifier = section.get_identifier()
        except AttributeError:
            identifier = section._name
        
        if brackets >= 0:
            return "%s%s%s" % ("[" * brackets, identifier, "]" * brackets)
        else:
            return ""
    
    def generate_api_key(self):
        self.misc.api_key = create_api_key()

class AbstractRemoteConfig(ConfigMixin, RemoteConfigRoot):
    """
    Root `ConfigSection` used to hold the configuration data received using
    SABNzbd's API.
    """
    
    # All subsections
    misc = gproperty(type=object)
    logging = gproperty(type=object)
    newzbin = gproperty(type=object)
    nzbmatrix = gproperty(type=object)
    categories = gproperty(type=object)
    servers = gproperty(type=object)
    
    def __init__(self, host, options=None):
        RemoteConfigRoot.__init__(self, host, name="lottanzb.config.sabnzbd",
            options=options)
    
    def __getinitargs__(self):
        return [self.host]
    
    def _get_clean_section(self, section):
        if isinstance(section._parent, ListConfigSection):
            clean_parent_section = RemoteConfigRoot._get_clean_section(self,
                section._parent)
            
            return clean_parent_section[section.get_remote_identifier()]
        else:
            return RemoteConfigRoot._get_clean_section(self, section)


class ListConfigSection(_ListConfigSection):
    def __init__(self, name, parent=None, options=None):
        self._by_identifier = {}
        
        _ListConfigSection.__init__(self, name, parent, options)
    
    def get_unknown_property(self, identifier):
        key = self._get_key_by_identifier(identifier)
        
        return _ListConfigSection.get_unknown_property(self, key)
    
    def set_unknown_property(self, identifier, value):
        key = self._get_key_by_identifier(identifier)
        
        if isinstance(key, basestring):
            self._by_identifier[key] = value
            value.set_identifier(key)
            key = len(self._list_items)
        
        return _ListConfigSection.set_unknown_property(self, key, value)
    
    def forget_identifier(self, identifier):
        try:
            del self._by_identifier[identifier]
        except KeyError:
            pass
    
    def sync_remote_identifier(self, value):
        identifier = value.get_identifier()
        old_remote_identifier = value.get_remote_identifier()
        
        value.sync_remote_identifier()
        
        if old_remote_identifier and identifier != old_remote_identifier:
            self.forget_identifier(old_remote_identifier)
    
    def sync_all_remote_identifiers(self):
        for value in self.values():
            self.sync_remote_identifier(value)
    
    def do_property_changed(self, section, key, value):
        if section is not self:
            value = section
        
        self._by_identifier[value.get_identifier()] = value
        
        if value.get_remote_identifier() is not None:
            self._by_identifier[value.get_remote_identifier()] = value
    
    def do_property_deleted(self, section, key, value):
        if section is self:
            self.forget_identifier(value.get_identifier())
            self.forget_identifier(value.get_remote_identifier())
    
    def _get_key_by_identifier(self, key):
        try:
            return int(key)
        except ValueError:
            try:
                return self._by_identifier[key].get_name()
            except KeyError:
                return key
    
    def merge(self, options):
        if isinstance(options, list):
            for index, option in enumerate(options):
                self[index] = option
        elif isinstance(options, ListConfigSection):
            unknown_merge_values = []
            unknown_local_values = []
            
            for key, value in options.items():
                if isinstance(value, ListElementConfigSection):
                    key = value.get_identifier()
                
                try:
                    self[key].merge(value)
                except InexistentOptionError:
                    unknown_merge_values.append(value)
            
            for key, value in self.items():
                if not value.get_remote_identifier() in options._by_identifier:
                    unknown_local_values.append(value)
            
            if len(unknown_local_values) == len(unknown_merge_values):
                for index, merge_value in enumerate(unknown_merge_values):
                    unknown_local_values[index].merge(merge_value)
            else:
                for local_value in unknown_local_values:
                    self.remove(local_value)
                
                for merge_value in unknown_merge_values:
                    self.append(merge_value)
            
            self.sync_all_remote_identifiers()
        else:
            _ListConfigSection.merge(self, options)
    
    def to_dict(self):
        """Returns a simple dictionary version of the config section.
        
        Instead of numerical keys for items of the list sections, their
        identifiers will be used.
        """
        
        result = self.get_options()
        
        for value in self.get_sections().values():
            result[value.get_identifier()] = value.to_dict()
        
        return result


class ListElementConfigSection(ConfigSection):
    def __init__(self, name, parent=None, options=None):
        self._remote_identifier = ""
        
        ConfigSection.__init__(self, name, parent, options)
    
    def get_identifier(self):
        raise NotImplementedError
    
    def set_identifier(self, identifier):
        raise NotImplementedError
    
    def get_remote_identifier(self):
        return self._remote_identifier
    
    def set_remote_identifier(self, identifier):
        self._remote_identifier = identifier

    def sync_remote_identifier(self):
        self._remote_identifier = self.get_identifier()


def create_api_key():
    """Returns a random 32-byte hex digest.
    
    The return value can serve as the API key used to authenticate clients
    of SABnzbd.
    """
    
    # Create some values to seed md5
    t = str(time.time())
    r = str(random.random())
    
    m = md5(t)
    m.update(r)
    
    # Return a hex digest of the md5, e.g. 49f68a5c8493ec2c0bf489821c21fc3b
    return m.hexdigest()
