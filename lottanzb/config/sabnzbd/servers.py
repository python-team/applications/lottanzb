# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.util.gobject_extras import gproperty
from lottanzb.config.sabnzbd import ListConfigSection, ListElementConfigSection

class Config(ListConfigSection):
    def find_section_class(self, section):
        return ServerConfig

class ServerConfig(ListElementConfigSection):
    username = gproperty(type=str)
    password = gproperty(type=str)
    host = gproperty(type=str)
    port = gproperty(type=int, default=119, minimum=0, maximum=2 ** 16 - 1)
    enable = gproperty(type=bool, default=True)
    fillserver = gproperty(type=bool, default=False)
    connections = gproperty(type=int, default=6, minimum=0, maximum=100)
    ssl = gproperty(type=bool, default=False)
    timeout = gproperty(type=int, default=120, minimum=30, maximum=240)
    optional = gproperty(type=bool, default=False)
    
    def set_identifier(self, identifier):
        # Don't use `str.split' as IPv6 host addresses contain ':'.
        host, separator, port = identifier.rpartition(":") # @UnusedVariable
        
        if port:
            self.host = host
            self.port = port
        else:
            raise ValueError("Expected an identifier of type 'host:port', "
                "got %r" % identifier)
    
    def get_identifier(self):
        return "%s:%s" % (self.host, self.port)
    
    @property
    def needs_authentication(self):
        return bool(self.username and self.password)
