# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gobject
import sys

from copy import deepcopy

from gobject import GObject as OriginalGObject, GObjectMeta # @UnusedImport
from gobject import property as gproperty # @UnusedImport
from gobject import list_properties

# Initializes the use of Python threading in the gobject module
gobject.threads_init()

__all__ = ["GObject", "GObjectMeta", "gproperty", "gsignal"]

def gsignal(name, *args, **kwargs):
    """
    Add a GObject signal to the current object.
    
    It current supports the types: str, int, float, long and object
    
    @param name: name of the signal
    @type name: string
    @param args: types for signal parameters,
        if the first one is a string "override", the signal will be
        overridden and must therefor exists in the parent GObject.
    @note: flags: A combination of;
      - gobject.SIGNAL_RUN_FIRST
      - gobject.SIGNAL_RUN_LAST
      - gobject.SIGNAL_RUN_CLEANUP
      - gobject.SIGNAL_NO_RECURSE
      - gobject.SIGNAL_DETAILED
      - gobject.SIGNAL_ACTION
      - gobject.SIGNAL_NO_HOOKS
    @note: retval: return value in signal callback
    """
    
    frame = sys._getframe(1)
    try:
        locals = frame.f_locals
    finally:
        del frame
    
    dict = locals.setdefault("__gsignals__", {})
    
    if args and args[0] == "override":
        dict[name] = "override"
    else:
        retval = kwargs.get("retval", None)
        if retval is None:
            default_flags = gobject.SIGNAL_RUN_FIRST
        else:
            default_flags = gobject.SIGNAL_RUN_LAST
        
        flags = kwargs.get("flags", default_flags)
        if retval is not None and flags != gobject.SIGNAL_RUN_LAST:
            raise TypeError(
                "You cannot use a return value without setting flags to "
                "gobject.SIGNAL_RUN_LAST.")
        
        dict[name] = (flags, retval, args)

class GObject(OriginalGObject):
    """GObject class which is better at converting values than the original.
    
    When setting the value of a property, this class tries to convert the value
    if necessary to prevent GObject from raising a TypeError. For example,
    "True" is converted to True if the property should be a boolean.
    
    Returns True if the new value differs from the old one, False otherwise.
    """
    
    def set_property(self, key, value):
        """The class documentation says it all"""
        
        value_type = self.get_property_type(key)
        
        if not isinstance(value, value_type):
            if value_type is int:
                if value is None:
                    value = 0
                else:
                    value = int(value)
            
            elif value_type is bool:
                if value in ("False", "0"):
                    value = False
                elif value in ("True", "1"):
                    value = True
                else:
                    value = bool(value)
            
            elif value_type is float:
                if value is None:
                    value = 0
                else:
                    value = float(value)
            
            elif value_type is str:
                if value is None:
                    value = ""
                else:
                    value = str(value)
        
        old_value = self.get_property(key)
        
        if old_value == value:
            return False
        else:
            OriginalGObject.set_property(self, key, value)
            
            return True
    
    def __getitem__(self, key):
        """So that we can use brackets to access the properties"""
        
        return self.get_property(key)
    
    def __setitem__(self, key, value):
        """So that we can use brackets to access the properties"""
        
        self.set_property(key, value)
    
    def __iter__(self):
        """Does the same as the one of dictionaries"""
        
        for key in self.keys():
            yield key
    
    def keys(self):
        return [prop.name.replace("-", "_") for prop in list_properties(self)]
    
    def values(self):
        for key in self.keys():
            yield self[key]
    
    def items(self):
        for key in self.keys():
            yield key, self[key]
    
    def update(self, properties):
        for key in properties:
            self[key] = properties[key]
    
    def deep_copy(self):
        return deepcopy(self)
    
    def __deepcopy__(self, memo):
        args = deepcopy(getattr(self, "__getinitargs__", lambda: [])(), memo)
        result = self.__class__(*args)
        
        for key in self.__dict__:
            result.__dict__[key] = deepcopy(self.__dict__[key], memo)
        
        for key in self.keys():
            result.set_property(key, deepcopy(self.get_property(key), memo))
        
        return result
    
    def connect_async(self, event, handler, *args):
        def new_handler(*new_args):
            gobject.idle_add(handler, *new_args)
        
        return self.connect(event, new_handler, *args)
    
    @classmethod
    def get_property_type(cls, key):
        """Returns the Python type of a certain object property"""
        
        option_type = getattr(cls, key).type
        
        if option_type in (gobject.TYPE_INT, gobject.TYPE_UINT,
            gobject.TYPE_INT64, gobject.TYPE_UINT64):
            return int
        elif option_type is gobject.TYPE_BOOLEAN:
            return bool
        elif option_type in (gobject.TYPE_DOUBLE, gobject.TYPE_FLOAT):
            return float
        elif option_type is gobject.TYPE_STRING:
            return str
        else:
            return object
    
    @classmethod
    def get_property_default_value(cls, key):
        """Returns the default value of a certain object property"""
        
        return getattr(cls, key).default
