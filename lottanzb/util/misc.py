# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import os
import sys

try:
    import gnomevfs
except ImportError:
    gnomevfs = None

__all__ = ["upper_ascii", "html_escape", "open_folder", "get_class_name",
    "unique"]

ASCII_LOWERCASE = "abcdefghijklmnopqrstuvwxyz"
ASCII_UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def upper_ascii(value):
    """
    Returns a copy of value, but with lower case letters converted to
    upper case.
    
    Compared to the built-in string method `upper`, this function
    is locale-independent and only turns ASCII letters to upper case.
    """
    
    result = ""
    
    for letter in value:
        try:
            result += ASCII_UPPERCASE[ASCII_LOWERCASE.index(letter)]
        except ValueError:
            result += letter
    
    return result

def html_escape(string):
    """
    Return a string with escaped HTML characters.
    """
    
    string = string.replace('&', '&amp;')
    string = string.replace('"', '&quot;')
    string = string.replace("'", '&#39;')
    string = string.replace(">", '&gt;')
    string = string.replace("<", '&lt;')
    
    return string

def open_folder(path):
    """Open the given path with the file manager."""
    
    if os.path.isfile(path):
        path = os.path.dirname(path)
    
    try:
        gnomevfs.url_show("file://" + path)
    except:
        pass

def get_class_name(frame=None):
    if frame is None:
        frame = sys._getframe(1)
    
    try:
        class_name = frame.f_locals["self"].__class__.__name__
    except KeyError:
        class_name = None

    return class_name

def unique(sequence):
    """Return copy of `sequence' with all duplicate items removed.
    
    The order of the sequence is preserved. Only the first occurrence of every
    item is kept.
    """
    
    seen = {}
    result = []
    
    for item in sequence:
        if item in seen:
            continue
        
        seen[item] = True
        result.append(item)
    
    return result
