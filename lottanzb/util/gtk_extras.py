# Copyright (C) 2010-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""Facilities making the use of GDK global lock more robust.

GTK+ is "thread aware" but not thread safe - it provides a global lock
controlled by gtk.gdk.threads_enter() and gtk.gdk.threads_leave() which
protects all use of GTK+. That is, only one thread can use GTK+ at any
given time.

However, this lock is not reentrant, meaning that when a thread tries to
acquire the lock twice, it will result in a deadlock. It needs to be taken
into consideration that GTK signal handlers are always executed with the lock
already acquired.

This module helps to ensure that the GDK global lock is acquired whenever
necessary, but no deadlocks occur.

`gtk_lock'
    All code that makes use of the GTK API and may be run from something other
    than a GTK signal handler (e.g. another thread or even a `gobject.idle_add'
    handler in the main thread) must be run in this environment.

`gtk_lock.locked'
    When registering a handler for a GTK signal, whereas the handler may also
    be invoked without the signal being emitted, the handler should be wrapped
    with this method, ensuring that `gtk_lock' is aware that the lock has
    already been acquired.

Example:

>>> def some_signal_handler(*args):
...    with gtk_lock:
...     # Run some GTK code.
>>> my_widget.connect("some-signal", gtk_lock.locked(some_signal_handler))
>>> # ... and in some thread:
>>> some_signal_handler()

The code is based on the PyGTK FAQ entry provided by
Anton Fedorov <datacompboy@mail.ru>:

    http://faq.pygtk.org/index.py?file=faq20.015.htp&req=show
"""

import gtk

from inspect import stack
from threading import RLock, current_thread
from functools import wraps
from logging import getLogger

from lottanzb.util.misc import get_class_name

LOG = getLogger(__name__)

__all__ = ["gtk_lock"]

class GtkLock:
    # If set, a debug message is emitted whenever the lock is acquired or
    # released. Because this introduces a fairly heavy amount of debugging
    # messages, it should not be enabled in production.
    DEBUG = False
    
    def __init__(self):
        self._lock = RLock()
        self._locked = 0
    
    def __enter__(self):
        self._lock.acquire(True)
        self._locked += 1
        
        if self._locked == 1:
            gtk.gdk.threads_enter()
            
            if self.DEBUG:
                LOG.debug("Acquired GTK lock for thread %s in %s." % (
                    self._get_thread_identifier(current_thread()),
                    self._get_code_location(stack()[1])))
    
    def __exit__(self, exc_type, exc_value, traceback):
        if self._locked == 1:
            gtk.gdk.threads_leave()
            
            if self.DEBUG:
                LOG.debug("Released GTK lock for thread %s in %s." % (
                    self._get_thread_identifier(current_thread()),
                    self._get_code_location(stack()[1])))
        
        self._locked -= 1
        self._lock.release()

    @classmethod
    def _get_code_location(cls, stack_element):
        class_name = get_class_name(stack_element[0]) or "<unknown>"
        return "%s.%s:%i" % (class_name, stack_element[3], stack_element[2])

    @classmethod
    def _get_thread_identifier(cls, thread):
        class_name = current_thread().__class__.__name__.strip("_")
        
        try:
            return "%s-%s" % (class_name, current_thread().name.split("-")[1])
        except IndexError:
            return class_name
    
    def locked(self, f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            self._lock.acquire(True)
            self._locked += 1
            
            try:
                return_value = f(*args, **kwargs)
            finally:
                self._locked -= 1
                self._lock.release()
            
            return return_value
        
        return wrapper

gtk_lock = GtkLock()
