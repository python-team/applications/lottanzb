# Copyright (C) 2007-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""
This module is used to communicate and interact with a SABnzbd instance
running on the local or a remote machine. The `sessions' submodule handles
the distinction betweenlocal and remote instances, while the rest of the backend
is indepent from the location of the SABnzbd instance.

Each query is represented by a class in the `interface.queries' submodule and
is a thread that is meant to be started and managed by the `interface' module.

The last important concept are hubs, which are registered with the backend.
Each hubs takes care of synchronizing and storing certain information and
exposing it and API query wrapper methods to the rest of the application.
The default hubs are:

- general: Keeping the list of downloads
- config: Synchronizing the configuration
- log: Providing SABnzbd warnings
- polling: Allowing other hubs to run certain queries regularly.

Plug-ins may add new hubs.
"""

from logging import getLogger
LOG = getLogger(__name__)

from lottanzb.core.environ import _
from lottanzb.core.component import Component, ExtensionPoint
from lottanzb.config.section import ConfigSection
from lottanzb.config.lotta import ConfigurableComponent
from lottanzb.util.gobject_extras import gproperty, gsignal
from lottanzb.backend.hubs import IHub, import_core_hubs
from lottanzb.backend.interface.errors import QueryConnectionError
from lottanzb.backend.sessions.base import ISession
from lottanzb.backend.sessions.errors import (
    SessionError,
    SessionNotSpecifiedError,
    SessionUnknownError,
    SessionConnectionError
)

__all__ = ["Backend"]

class Config(ConfigSection):
    """Represents the configuration section 'backend'."""
    
    sessions = gproperty(
        type    = object,
        nick    = "The active session and options for the various session",
        blurb   = "A subsection that contains information about what session "
                  "is currently being used and all information necessary for "
                  "each session type such as connection information.")
    
    delete_enqueued_nzb_files = gproperty(
        type    = bool,
        default = False,
        nick    = "Delete NZB files successfully added to the download queue",
        blurb   = "Setting this value to True doesn't mean that the NZB "
                  "file is completely removed from the system, but that it "
                  "can only be found in the hidden SABnzbd directories."
                  "Because it's very uncommon for files to disappear as"
                  "soon as a user clicks on them, this option is set to "
                  "False by default.")


class Backend(ConfigurableComponent):
    """
    Set up the right session and establish the connection to the corresponding
    SABnzbd instance. Also manage the hubs and the created session.
    
    Only lives as long as the session is active.
    
    If a query cannot be executed successfully because of a connection error,
    the backend will unload itself and provide observers of the
    `ComponentManager's "component-unloaded" signal with a
    `SessionConnectionError`.
    """
    
    hubs = ExtensionPoint(IHub)
    sessions = ExtensionPoint(ISession)
    
    def __init__(self, component_manager):
        ConfigurableComponent.__init__(self, component_manager)
        
        self.session = None
        
        if not self.config.sessions.active:
            raise SessionNotSpecifiedError()
        
        for session_cls in self.sessions:
            if session_cls.get_name() == self.config.sessions.active:
                self.session = self._component_manager.load(session_cls)
                break
        
        if not self.session:
            raise SessionUnknownError()
        
        try:
            self.interface = self.session.get_interface()
            self.interface.handshake()
        except QueryConnectionError as error:
            raise SessionConnectionError(self.session, error)
        
        # Only listen for failed queries if the initial handshake queries have
        # been successful. Clients will handle failed handshake queries and
        # create proper log messages.
        self.interface.connect("query-failure", self._on_query_failure)
        
        LOG.info(_("Connection to '%s' established."),
            self.interface.connection_info.complete_host_string)
        
        # FIXME: Lazy import.
        # We don't know if all hubs have been imported yet by other parts of the
        # application. Note: The polling hub will be imported anyway.
        import_core_hubs()
        
        for hub_cls in self.hubs:
            self._component_manager.load_async(hub_cls)
    
    def _on_query_failure(self, interface, query):
        """
        If a query fails due to a `QueryConnectionError`, unload the whole
        `Backend` and pass along a proper `SessionConnectionError`.
        """
        
        LOG.warning(str(query.error))
        LOG.debug("Raw Response: " + query.raw_response)
        
        if isinstance(query.error, QueryConnectionError):
            self._component_manager.unload(self, SessionConnectionError(
                self.session, query.error, previous_success=True))
    
    @classmethod
    def get_known_exceptions(cls):
        """Loading the `Backend' may fail due to various reasons. In this case
        a `SessionError' is raised that is meant to be handled by clients.
        """
        
        return (SessionError,)
