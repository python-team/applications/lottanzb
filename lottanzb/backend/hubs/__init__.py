# Copyright (C) 2007-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import re
import locale

from datetime import datetime, timedelta
from gettext import ngettext

from lottanzb.core.environ import _
from lottanzb.core.component import Interface
from lottanzb.util.gobject_extras import gsignal

__all__ = ["DataSize", "DataSpeed", "TimeDelta", "DateTime", "IHub",
    "import_core_hubs"]

class DataSize(object):
    UNITS = ["bytes", "KiB", "MiB", "GiB", "TiB", "PiB"]
    PATTERN = re.compile(r"""
    ^
     (?P<value>-?[0-9]+(\.[0-9]+)?)\s*(?P<unit>(B|KB|MB|GB|TB|PB)?)
    $""", re.VERBOSE)
    
    def __init__(self, bytes=0, kibibytes=0, mebibytes=0, gibibytes=0,
        tebibytes=0, pebibytes=0):
        self._data = sum([
            bytes,
            kibibytes * 2 ** 10,
            mebibytes * 2 ** 20,
            gibibytes * 2 ** 30,
            tebibytes * 2 ** 40,
            pebibytes * 2 ** 50,
        ])
    
    @property
    def bytes(self):
        return self._data
    
    @property
    def kibibytes(self):
        return self._data / float(2 ** 10)
    
    @property
    def mebibytes(self):
        return self._data / float(2 ** 20)
    
    @property
    def gibibytes(self):
        return self._data / float(2 ** 30)
    
    def __str__(self):
        value = self._data
        n = 0
        
        # Use 1000 as the lower boundary for switching the the next-higher
        # scale. 1024 would be more accurate, but may cause the returned number
        # to consist of 5 numbers instead of 4.
        # Thus, even though this change introduces a slight mathematical
        # inaccuracy ('1.0 MiB/s' is returned instead of '1023.9 KiB/s'),
        # it will save a few pixels here and there and is also more readable.
        while value >= 1000.0 and n < len(self.UNITS) - 1:
            value = value / float(2 ** 10)
            n = n + 1
        
        info = { "data_size": value, "data_size_unit": self.UNITS[n]}
        
        if n == 0:
            info["data_size_unit"] = ngettext("byte", "bytes", value)
            
            # Don't show any decimal places when displaying a number of bytes.
            return _("%(data_size).0f %(data_size_unit)s") % info
        else:
            return _("%(data_size).1f %(data_size_unit)s") % info
    
    def __nonzero__(self):
        return self._data != 0
    
    def __eq__(self, other):
        if isinstance(other, DataSize):
            return self._data == other._data
        else:
            return id(self) == id(other)
    
    def __neq__(self, other):
        if isinstance(other, DataSize):
            return self._data != other._data
        else:
            return id(self) != id(other)
    
    def __cmp__(self, other):
        return cmp(self._data, other._data)
    
    @classmethod
    def from_string(cls, value):
        match = cls.PATTERN.match(value)
        
        if match:
            unit_map = {
                "": "bytes",
                "B": "bytes",
                "KB": "kibibytes",
                "MB": "mebibytes",
                "GB": "gibibytes",
                "TB": "tebibytes",
                "PB": "pedibytes"
            }
            
            try:
                value = float(match.group("value"))
                unit = unit_map[match.group("unit")]
            except (KeyError, ValueError):
                pass
            else:
                return DataSize(**{ unit: value })
        elif not value:
            return DataSize(0)
        
        raise ValueError("Invalid data size.")


class DataSpeed(DataSize):
    @property
    def bytes_per_second(self):
        return self.bytes
    
    @property
    def kibibytes_per_second(self):
        return self.kibibytes
    
    @property
    def mebibytes_per_second(self):
        return self.mebibytes
    
    @property
    def gibibytes_per_second(self):
        return self.gibibytes
    
    def __str__(self):
        return _("%s/s") % DataSize.__str__(self)


class TimeDelta(timedelta):
    PATTERN = re.compile(r"""
    ^
     (?P<hours>\d+) :
     (?P<minutes>\d{2}) :
     (?P<seconds>\d{2})
    $""", re.VERBOSE)
    
    PATTERN_2 = re.compile(r"""
    ^
     (?P<value>\d+)
     (?P<unit>[mhd])
    $""", re.VERBOSE)
    
    SCALES = ["days", "hours", "minutes", "seconds"]
    
    @classmethod
    def from_string(cls, value):
        match = cls.PATTERN.search(value)
        match_2 = cls.PATTERN_2.search(value)
        
        if match:
            return cls(
                hours=int(match.group("hours")),
                minutes=int(match.group("minutes")),
                seconds=int(match.group("seconds")))
        elif match_2:
            value = int(match_2.group("value"))
            unit = {
                "m": "minutes",
                "h": "hours",
                "d": "days"
            }.get(match_2.group("unit"))
            
            return cls(**{ unit: value })
        else:
            raise ValueError("Invalid time format.")
    
    @property
    def short(self):
        """
        Return a pretty, localized string representation of the `TimeDelta` like
        '12 hours' or '1 second'.
        
        Because a string like '1 hour' is not precise enough, it must also
        include information about the value of the next smaller time scale, in
        this case minutes.
        
        Because such values will often be imprecise (especially if they're
        estimations), the value of the next smaller time scale will be rounded
        down.
        
        This means that instead of '1 hour and 43 minutes', the returned string
        will be '1 hour and 40 minutes'. For estimations that are likely to
        change over time, this causes the return values to be changed less
        frequently.
        
        Consider the following example return values:
        
        >>> TimeDelta(seconds=46).short
        '46 seconds'
        >>> TimeDelta(minutes=2, seconds=3).short
        '2 minutes'
        >>> TimeDelta(minutes=2, seconds=33).short
        '2 minutes'.
        >>> TimeDelta(hours=2, minutes=3).short
        '2 hours'
        >>> TimeDelta(hours=2, minutes=33).short
        '2 hours and 30 minutes'.
        >>> TimeDelta(days=2, minutes=57)
        '2 days'
        >>> TimeDelta(days=1, hours=18)
        '1 day and 18 hours'
        """
        
        # Use 'seconds' as the default major time scale so that if the
        # `TimeDelta` is actually 0 seconds, the method doesn't return '0 days'.
        major_scale = self.SCALES[-1]
        minor_scale = None
        
        for index, scale in enumerate(self.SCALES):
            value = self._get_partitioned_value(scale)
            
            if value >= 1:
                major_scale = scale
                
                if value < 10 and index != len(self.SCALES) - 1:
                    minor_scale = self.SCALES[index + 1]
                
                break
        
        if minor_scale is None or major_scale in ("seconds", "minutes"):
            return self._get_simple_string(major_scale)
        else:
            minor_scale_value = self._get_partitioned_value(minor_scale)
            
            if minor_scale in ("minutes", "seconds"):
                # Only round minutes and seconds.
                minor_scale_value = minor_scale_value / 10 * 10
            
            if not minor_scale_value:
                # Don't return something like '1 minute and 0 seconds', but
                # simply '1 minute'.
                return self._get_simple_string(major_scale)
            
            rounded_time_delta = TimeDelta(**{
                major_scale: self._get_partitioned_value(major_scale),
                minor_scale: minor_scale_value
            })
            
            return _("%(major_time_scale)s and %(minor_time_scale)s") % {
                "major_time_scale":
                    rounded_time_delta._get_simple_string(major_scale),
                "minor_time_scale":
                    rounded_time_delta._get_simple_string(minor_scale)
            }
    
    def _get_partitioned_value(self, scale):
        """
        Extract the integer number of seconds, minutes, hours or days calculated
        using modulo 24 or 60, respectively.
        
        As an example, if `TimeDelta` represents a duration of 2 hours,
        32 minutes and 10 seconds, this method will do the following:
        
        >>> _get_partitioned_value("days")
        0
        >>> _get_partitioned_value("hours")
        2
        >>> _get_partitioned_value("minutes")
        32
        >>> _get_partitioned_value("seconds")
        10
        """
        
        if not scale in self.SCALES:
            raise ValueError("Unknown time scale.")
        
        if scale == "days":
            return self.days
        elif scale == "hours":
            return int(self.seconds / 3600) % 24
        elif scale == "minutes":
            return int(self.seconds / 60) % 60
        elif scale == "seconds":
            return self.seconds % 60
    
    def _get_simple_string(self, scale):
        """
        Return a string representation of the `TimeDelta` for a certain time
        scale.
        
        As an example, if `TimeDelta` represents a duration of 2 hours,
        32 minutes and 10 seconds, this method will do the following:
        
        >>> _get_simple_string("days")
        '0 days'
        >>> _get_simple_string("hours")
        '2 hours'
        >>> _get_simple_string("minutes")
        '32 minutes'
        >>> _get_simple_string("seconds")
        '10 seconds'
        """
        
        if not scale in self.SCALES:
            raise ValueError("Unknown time scale.")
        
        value = int(self._get_partitioned_value(scale))
        
        if scale == "days":
            return ngettext("%i day", "%i days", value) % value
        elif scale == "hours":
            return ngettext("%i hour", "%i hours", value) % value
        elif scale == "minutes":
            return ngettext("%i minute", "%i minutes", value) % value
        elif scale == "seconds":
            return ngettext("%i second", "%i seconds", value) % value


class DateTime(datetime):
    PATTERN = re.compile(r"""
    ^
     (?P<year>\d{4})-
     (?P<month>\d{2})-
     (?P<day>\d{2}).*?
     (?P<hour>\d{2}):
     (?P<minute>\d{2}):
     (?P<second>\d{2}),
     .*?
    $""", re.VERBOSE)
    
    @classmethod
    def from_string(cls, value):
        match = cls.PATTERN.search(value)
        
        if match:
            info = {
                "year": int(match.group("year")),
                "month": int(match.group("month")),
                "day": int(match.group("day")),
                "hour": int(match.group("hour")),
                "minute": int(match.group("minute")),
                "second": int(match.group("second"))
            }
            
            return cls(**info)
        
        raise ValueError("Invalid date format.")
    
    @classmethod
    def from_datetime(cls, value):
        """Return the equivalent `DateTime' object given a `datetime' object"""
        return cls.combine(value.date(), value.time())
    
    @property
    def full(self):
        """Localized string containing both the date and time"""
        return self.strftime(locale.nl_langinfo(locale.D_T_FMT))
    
    @property
    def short_time(self):
        """Localized string containing only the time"""
        return self.strftime(locale.nl_langinfo(locale.T_FMT))
    
    @property
    def short_date(self):
        """Localized string containing only the date"""
        return self.strftime(locale.nl_langinfo(locale.D_FMT))


class IHub(Interface):
    pass

def import_core_hubs():
    from lottanzb.backend.hubs.config import ConfigHub
    from lottanzb.backend.hubs.capabilities import CapabilitiesHub
    from lottanzb.backend.hubs.general import GeneralHub
    from lottanzb.backend.hubs.log import LoggingHub
    from lottanzb.backend.hubs.polling import PollingHub
    from lottanzb.backend.hubs.statistics import StatisticsHub
