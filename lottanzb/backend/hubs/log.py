# Copyright (C) 2007-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import re
import hashlib
import os
import logging
import time

from os.path import isfile
from threading import Lock

LOG = logging.getLogger(__name__)

from lottanzb import resources
from lottanzb.core.component import Component, implements, depends_on
from lottanzb.util.gobject_extras import gsignal
from lottanzb.backend import Backend
from lottanzb.backend.hubs.polling import PollingHub
from lottanzb.backend.hubs import IHub, DateTime
from lottanzb.backend.interface.queries import WarningsQuery, ClearWarningsQuery

class LoggingHub(Component):
    implements(IHub)
    depends_on(Backend)
    depends_on(PollingHub)
    
    gsignal("records-added", object)
    gsignal("records-cleared")
    
    LOG_FILE = resources.get_config("sabnzbd_log")
    LOG_FILE_LOCK = Lock()
    SECONDS_BETWEEN_REQUESTS = 10
    MESSAGE_PATTERN = re.compile("""
    ^
     (?P<created>.*?)\\n
     (?P<level>.*?)\\n
     (?P<message>.*?)
    $""", re.VERBOSE | re.MULTILINE)
    
    def __init__(self, component_manager):
        Component.__init__(self, component_manager)
        
        self.maybe_delete_log_file()
        
        self._message_hashes = []
        self._records = []
        
        self._interface = self._component_manager.get(Backend).interface
        self._interface.connect_query_started(ClearWarningsQuery,
            self.on_clear_warnings_query)
        
        polling_hub = self._component_manager.get(PollingHub)
        polling_hub.schedule(1000 * self.SECONDS_BETWEEN_REQUESTS,
            WarningsQuery, self.on_warnings_query)
    
    def clear(self):
        return self._interface.run_query(ClearWarningsQuery())
    
    def maybe_delete_log_file(self):
        if isfile(self.LOG_FILE):
            os.unlink(self.LOG_FILE)
    
    def on_clear_warnings_query(self, interface, query):
        with self.LOG_FILE_LOCK:
            self.maybe_delete_log_file()
            self._records = []
            self.emit("records-cleared")
    
    def on_warnings_query(self, query):
        if not query.response:
            if self._records:
                self._records = []
                self.emit("records-cleared")
            
            return
        
        new_log_records = []
        
        for raw_message in query.response:
            message_hash = self.get_message_hash(raw_message)
            
            if message_hash in self._message_hashes:
                continue
            
            match = self.MESSAGE_PATTERN.match(raw_message)
            
            if match:
                self._message_hashes.append(message_hash)
                
                message = match.group("message")
                level = {
                    "WARNING": logging.WARNING,
                    "ERROR": logging.ERROR
                }.get(match.group("level"))
                
                datetime = DateTime.from_string(match.group("created"))
                created = time.mktime(datetime.timetuple())
                new_log_records.append(
                    SABnzbdLogRecord(message, level, created))
        
        self._records.extend(new_log_records)
        self.emit("records-added", new_log_records)
        
        with self.LOG_FILE_LOCK:
            with open(self.LOG_FILE, "a") as log_file:
                for log_record in new_log_records:
                    log_file.write("{created} {level:8} {message}\n".format(
                        created=log_record.created,
                        level=logging.getLevelName(log_record.levelno),
                        message=log_record.getMessage()))
    
    @property
    def records(self):
        """Returns a shallow copy of the array holding all `SABnzbdLogRecord`s.
        """
        
        return self._records[:]
    
    @staticmethod
    def get_message_hash(message):
        m = hashlib.sha224()
        m.update(message)
        
        return m.hexdigest()


class SABnzbdLogRecord(object):
    """Stores a SABnzbd log message.
    
    This class has an interface that is similar to `logging.LogRecord's.
    """
    
    def __init__(self, message, level, created):
        self.msg = message
        self.levelno = level
        self.created = created
        self.name = "sabnzbd"
        self.args = []
    
    def getMessage(self):
        return self.msg
