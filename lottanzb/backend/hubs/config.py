# Copyright (C) 2007-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import copy

from threading import Timer
from copy import deepcopy

from lottanzb.util.gobject_extras import gsignal
from lottanzb.config.section import ConfigSection
from lottanzb.config.sabnzbd import AbstractRemoteConfig, ListConfigSection
from lottanzb.core.component import Component, implements, depends_on
from lottanzb.backend import Backend
from lottanzb.backend.hubs import IHub
from lottanzb.backend.hubs.polling import PollingHub
from lottanzb.backend.interface import QueryCache
from lottanzb.backend.interface.queries import (GetConfigQuery,
    SimpleSetConfigQuery, ComplexSetConfigQuery, DeleteConfigQuery,
    RestartQuery)

__all__ = ["ConfigHub"]

class SilentRemoteConfig(AbstractRemoteConfig):
    """
    Configuration that is not synchronized with a remote host.
    
    Useful for temporary copies of a synchronized configuration.
    """
    
    def load(self):
        pass
    
    def write_back(self, section, key):
        pass


class GroupedSectionChange(set):
    WRITEBACK_DELAY = 0.1
    
    def __init__(self, section, write_back_method):
        delay = self.WRITEBACK_DELAY
        
        self.section = section
        self.timer = Timer(delay, write_back_method, [self.section])
        self.timer.start()
        
        set.__init__(self)


class RemoteConfig(AbstractRemoteConfig):
    """
    Holds the configuration of a SABnzbd instance.
    
    This class offers the possibility to seamlessly merge an updated version of
    the configuration received by a `GetConfigQuery' into this configuration.
    Also, any changes that aren't the result of such a merge operation
    are detected and prepared to be written back to the SABnzbd instance using
    `SimpleSetConfigQuery', `ComplexSetConfigQuery' and `DeleteConfigQuery'.
    
    Any communication is not initiated directly by this class (as it doesn't
    know anything about an `Interface', but requested using the two signals
    'request-write-back' and 'request-load'.
    """
    
    # Emitted when a certain change in the configuration needs to be written
    # back to the configuration host. The only argument is a
    # `SimpleSetConfigQuery', `ComplexSetConfigQuery' or a `DeleteConfigQuery'.
    gsignal("request-write-back", object)
    
    # Emitted when the `load` method is called. The observer is meant to
    # initiate a `GetConfigQuery` and pass its response to
    # `process_config_query`.
    # If the only argument is True, the `GetConfigQuery' should be invoked
    # synchronously, asynchronously otherwise.
    gsignal("request-load", bool)
    
    def __init__(self, host):
        self._grouped_changes = {}
        
        AbstractRemoteConfig.__init__(self, host)
    
    def load(self, synchronous=True):
        self.emit("request-load", synchronous)
    
    def process_config_query(self, query):
        config = deepcopy(query.response)
        
        # In a configuration file created by an instance of SABnzbd that has
        # not been configured yet, the 'servers' and 'categories' configuration
        # sections may be missing.
        servers = config.get("servers", [])
        categories = config.get("categories", [])
        
        config["servers"] = {}
        config["categories"] = {}
        
        for server in servers:
            config["servers"][server.pop("name")] = server
        
        for category in categories:
            config["categories"][category["name"]] = category
        
        self.merge(SilentRemoteConfig(self.host, options=config))
    
    def write_back_change(self, section, key, value):
        if isinstance(value, ConfigSection):
            if value.get_depth() != 2:
                raise ValueError("%r does not have a depth of 2" % value)
            
            if not isinstance(section, ListConfigSection):
                raise ValueError(("%r is not an instance of type "
                    "`ListConfigSection'") % value)
            
            keyword = value.get_remote_identifier()
            values = value.to_dict()
            query = ComplexSetConfigQuery(section._name, keyword, values)
            
            self.emit("request-write-back", query)
        else:
            depth = section.get_depth()
            
            if depth == 1:
                query = SimpleSetConfigQuery(section._name, key, section[key])
                
                self.emit("request-write-back", query)
            elif depth == 2:
                if section in self._grouped_changes:
                    grouped_change = self._grouped_changes[section]
                else:
                    grouped_change = GroupedSectionChange(section,
                        self.write_back_group_change)
                    
                    self._grouped_changes[section] = grouped_change
                
                grouped_change.add(key)
            else:
                raise ValueError(("Could not write back key '%s' in section "
                    "'%s'") % (key, section.get_full_name()))
    
    def write_back_group_change(self, section):
        values = {}
        
        for key in self._grouped_changes[section]:
            values[key] = section[key]
        
        section_name = section._parent._name
        keyword = section.get_remote_identifier()
        query = ComplexSetConfigQuery(section_name, keyword, values)
        section._parent.sync_remote_identifier(section)
        
        del self._grouped_changes[section]
        
        self.emit("request-write-back", query)
    
    def write_back_deletion(self, section, key, value):
        # We cannot use the `key` because in the case of a `ListConfigSection`
        # this will simply be an integer.
        keyword = value.get_remote_identifier()
        query = DeleteConfigQuery(section._name, keyword)
        
        self.emit("request-write-back", query)
    
    def __deepcopy__(self, memo):
        """
        Objects of type `RemoteConfig` are not meant to be copied deeply.
        
        Any changes would still be written back to the remote host, which is
        most probably not desired.
        
        Create a `SilentRemoteConfig` using `silent_deep_copy` instead.
        """
        
        raise copy.Error("un(deep)copyable object of type %r" % type(self))
    
    def silent_deep_copy(self):
        """
        Create a deep copy of the configuration that won't be synchronized
        with the remote host in any way.
        
        It's an instance of `SilentRemoteConfig`.
        """
        
        options = {}
        
        for key, value in self.items():
            options[key] = deepcopy(value)
        
        return SilentRemoteConfig(self.host, options)


class ConfigHub(Component):
    implements(IHub)
    depends_on(Backend)
    
    def __init__(self, component_manager):
        Component.__init__(self, component_manager)
        
        self.interface = self._component_manager.load(Backend).interface
        
        # Cache the query responses.
        self.config_cache = QueryCache()
        
        self.config = RemoteConfig(
            self.interface.connection_info.complete_host_string)
        self.config.connect("request-load", self.on_request_load)
        self.config.connect("request-write-back",
            self.on_request_write_back)
        self.config.load()
        
        polling_hub = self._component_manager.load(PollingHub)
        polling_hub.schedule_minutely(GetConfigQuery, self.on_get_config_query)
        
        self.interface.connect_query_success(SimpleSetConfigQuery,
            self.on_simple_set_config_query)
        self.interface.connect_query_success(RestartQuery,
            self.on_restart_query)
    
    def on_request_load(self, config, synchronous):
        query = GetConfigQuery()
        query.connect("success", self.on_get_config_query)
        
        if synchronous:
            self.interface.get_response(query)
        else:
            self.interface.run_query(query)
    
    def on_request_write_back(self, config, query):
        self.interface.run_query(query)
    
    def on_get_config_query(self, query):
        """Pass response of a `GetConfigQuery' to `config' for merging.
        
        Only responses containing the whole configuration are considered.
        """
        
        if not query.arguments["section"] and not query.arguments["keyword"]:
            # Avoid unnecessary work.
            if not self.config_cache.has_changed(query):
                return
            
            self.config.process_config_query(query)
    
    def on_simple_set_config_query(self, interface, query):
        """
        Update the connection information after the user has updated the
        configuration using a `SimpleSetConfigQuery` (most probably using the
        preferences UI).
        
        This method ensures that queries will work properly even after the
        username, password, or the API key have changed.
        
        The port doesn't need to be updated because this change doesn't take
        effect immediately.
        """
        
        if query.changes_connection_info_option():
            self.interface.connection_info.set_property(query.key, query.value)
    
    def on_restart_query(self, interface, query):
        """
        Using a `RestartQuery`, a change to the port SABnzbd listens on can be
        applied. This method ensures that the connection information is updated
        after the `RestartQuery` has been completed.
        """
        
        self.interface.connection_info.port = self.config.misc.port
    
    def set_bandwidth_limit(self, limit=0):
        self.config.misc.bandwidth_limit = limit
    
    def remove_bandwidth_limit(self):
        self.set_bandwidth_limit(0)
    
    def get_bandwidth_limit(self):
        return self.config.misc.bandwidth_limit
