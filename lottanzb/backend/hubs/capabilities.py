# Copyright (C) 2010-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from threading import Lock

from lottanzb.util.gobject_extras import gproperty
from lottanzb.core.constants import PostProcessing
from lottanzb.backend import Backend
from lottanzb.core.component import Component, implements, depends_on
from lottanzb.backend.interface.queries import (
    CapabilitiesQuery, RestartQuery, QueueQuery)
from lottanzb.backend.hubs import IHub

class CapabilitiesHub(Component):
    implements(IHub)
    depends_on(Backend)
    
    supports_yenc = gproperty(type=bool, default=False)
    supports_ssl = gproperty(type=bool, default=False)
    
    par_path = gproperty(type=str)
    rar_path = gproperty(type=str)
    zip_path = gproperty(type=str)
    nice_path = gproperty(type=str)
    ionice_path = gproperty(type=str)
    
    power_management_support = gproperty(type=bool, default=False,
        nick="SABnzbd host supports power management",
        blurb="Indicates whether SABnzbd has the possibility of shutting down, "
            "suspending or hibernating the machine it's running on when all "
            "downloads are complete.")
    
    def __init__(self, component_manager):
        Component.__init__(self, component_manager)
        
        self._refresh_lock = Lock()
        
        self.interface = self._component_manager.load(Backend).interface
        self.interface.connect_query_success(RestartQuery,
            self.on_restart_query)
        self.interface.connect_query_success(QueueQuery,
            self.on_queue_query)
    
    def on_restart_query(self, interface, query):
        self.refresh()
    
    def on_queue_query(self, interface, query):
        self.power_management_support = query.response["power_options"]
    
    def on_capabilities_query(self, query):
        response = query.response
        
        self.yenc_support = response.get("yenc", False)
        self.ssl_support = response.get("ssl", False)
        
        self.par_path = response.get("par2c", "") or response.get("par2", "")
        self.rar_path = response.get("rar", "")
        self.zip_path = response.get("zip", "")
        self.nice_path = response.get("nice", "")
        self.ionice_path = response.get("ionice", "")
        
        self.emit("updated")
        
        self._refresh_lock.release()
    
    def refresh(self):
        if self._refresh_lock.acquire(False):
            query = CapabilitiesQuery()
            query.connect("success", self.on_capabilities_query)
            
            self.interface.run_query(query)
    
    def supports_software(self, software):
        return bool(getattr(self, software + "_path", None))
    
    def supports_post_processing(self, post_processing):
        """
        If the query has been successful, check if a certain post-processing
        level in `lottanzb.core.constants.PostProcessing` is supported by
        SABnzbd.
        """
        
        if post_processing == PostProcessing.REPAIR:
            return self.supports_software("par")
        elif post_processing in (PostProcessing.UNPACK, PostProcessing.DELETE):
            return self.supports_software("rar") \
                and self.supports_software("zip") \
                and self.supports_post_processing(PostProcessing.REPAIR)
        
        return True
