# Copyright (C) 2007-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.util.gtk_extras import gtk_lock
from lottanzb.util.misc import unique
from lottanzb.backend.hubs import TimeDelta
from lottanzb.backend.hubs.general.download import Download, Status, StatusGroup
from lottanzb.backend.hubs.general.download_list import DownloadListStore

__all__ = ["DownloadListUpdater", "DownloadListQueueUpdater"]

class DownloadListUpdater(object):
    STATUS_ORDER = (
        StatusGroup.COMPLETE,
        StatusGroup.PROCESSING,
        StatusGroup.NOT_FULLY_LOADED)
    
    def __init__(self, model, status):
        self.paused = False
        
        for some_status in self.STATUS_ORDER:
            if not (some_status & status == some_status or \
                some_status & status == 0):
                raise ValueError("'status' must be one or a combination of "
                    "`StatusGroup.COMPLETE', `StatusGroup.PROCESSING' and "
                    "`StatusGroup.NOT_FULLY_LOADED'.")
        
        self.model = model
        self.status = status
        
        with gtk_lock:
            self.model_filter = self.model.get_filter_by_status(self.status)
        
        # Mapping of download IDs to the last corresponding slot processed by
        # `apply_slots'.
        self._cache = {}
    
    def apply_slots(self, slots, paused=False):
        self.paused = paused
        
        # Determine downloads that are not part of the slots anymore.
        # When downloading recovery data, the download appears twice in the list
        # of slots included in the response to the `QueueQuery'. Thus, filter
        # duplicate IDs in order to prevent a `KeyError' from being raised when
        # calling `old_positions.pop' below.
        new_ids = unique([slot["nzo_id"] for slot in slots])
        
        # List of iters found in `model_filter' for all downloads that are not
        # part of `slots' anymore.
        # Such downloads may have changed their status or may have been deleted
        # by the user.
        disappeared_iters = []
        
        with gtk_lock:
            for row in self.model_filter:
                download = row[DownloadListStore.COLUMN]
                
                if not download.id in new_ids:
                    # Defer calling `handle_disappearance' in order to avoid
                    # messing up the traversal of `model_filter'.
                    iter = self.model_filter.convert_iter_to_child_iter(
                        row.iter)
                    disappeared_iters.append(iter)
            
            # Update download information
            updated_downloads = set()
            
            for iter in disappeared_iters:
                download = self.model[iter][DownloadListStore.COLUMN]
                updated_downloads.add(download)
                
                self.handle_disappearance(iter, download)
            
            for slot in slots:
                download_id = slot["nzo_id"]
                
                # Only update the download if no corresponding slot is in the
                # cache or if the new slot is different from the one in the
                # cache.
                if download_id not in self._cache or \
                    self._cache[download_id] != slot:
                    if download_id in self.model._by_id:
                        download = self.model._by_id[download_id]
                        download.update(slot, paused)
                    else:
                        download = Download(download_id)
                        download.update(slot, paused)
                        
                        self.model.append((download,))
                    
                    self._cache[download_id] = slot
                    updated_downloads.add(download)
            
            # Emit 'row-changed' for all updated downloads
            for row in self.model:
                if row[DownloadListStore.COLUMN] in updated_downloads:
                    self.model.row_changed(row.path, row.iter)
            
            # Update order of downloads
            new_order = []
            
            for status in self.STATUS_ORDER:
                # Determine the new order of downloads with status `status'.
                
                model_filter = self.model.get_filter_by_status(status)
                
                if self.status & status == status:
                    # The new order needs to be determined using `slots'.
                    
                    # Maps downloads IDs of downloads with `status' to their
                    # absolute position in the download list.
                    old_positions = {}
                    
                    # The new order of downloads that don't appear in `slots'.
                    # They have disappeared from `slots', but are expected to
                    # reappear soon in the output of another API method
                    # (i.e. the history query).
                    disappeared_order = []
                    
                    received_order = []
                    
                    for row in model_filter:
                        download = row[DownloadListStore.COLUMN]
                        path = model_filter.convert_path_to_child_path(
                            row.path)
                        old_positions[download.id] = path[0]
                    
                    for new_id in new_ids:
                        download = self.model._by_id[new_id]
                        
                        if bool(download.status & status):
                            received_order.append(old_positions.pop(new_id))
                    
                    disappeared_order = old_positions.values()
                    disappeared_order.sort()
                    
                    new_order.extend(disappeared_order)
                    new_order.extend(received_order)
                else:
                    for row in model_filter:
                        path = model_filter.convert_path_to_child_path(
                            row.path)
                        new_order.append(path[0])
            
            self.model.reorder(new_order)
    
    def handle_disappearance(self, iter, download):
        """Remove `download' from the model."""
        
        with gtk_lock:
            self.model.remove(iter)
        
        if download.id in self._cache:
            del self._cache[download.id]


class DownloadListQueueUpdater(DownloadListUpdater):
    def handle_disappearance(self, iter, download):
        """
        Remove `download' from the model only if the download is likely to have
        been deleted by the user through the SABnzbd web interface.
        
        Otherwise, such a download might just have been completed and will thus
        show up in the next 'history' query.
        
        Also, downloads with the status `Status.GRABBING' will be replaced by
        another one as soon as SABnzbd is done downloading the NZB file.
        Thus, in this case, the temporary download entry should always be
        removed from LottaNZB's download list.
        """
        
        deleted = False
        
        if download.has_status(StatusGroup.NOT_FULLY_LOADED):
            deleted = \
                download.status == Status.GRABBING or \
                self.download_is_paused(download) or \
                self.download_not_nearly_finished(download) 
        
        if deleted:
            DownloadListUpdater.handle_disappearance(self, iter, download)
    
    def download_is_paused(self, download):
        return self.paused or download.has_status(Status.PAUSED)
    
    def download_not_nearly_finished(self, download):
        return download.time_left and download.time_left > TimeDelta(seconds=5)
