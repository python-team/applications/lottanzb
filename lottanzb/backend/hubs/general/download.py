# Copyright (C) 2007-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import re

from os.path import basename

from lottanzb.core.environ import _
from lottanzb.util.gobject_extras import GObject, gproperty
from lottanzb.core.constants import Priority, PostProcessing
from lottanzb.backend.hubs import DataSize, TimeDelta, DateTime

__all__ = ["Status", "Download"]

class Status:
    GRABBING, QUEUED, DOWNLOADING, PAUSED, QUICKCHECK, \
    DOWNLOADING_RECOVERY_DATA, VERIFYING, REPAIRING, EXTRACTING, JOINING, \
    MOVING, SCRIPT, PROCESSING, FAILED, SUCCEEDED = [2 ** i for i in range(15)]
    
    @classmethod
    def to_pretty_string(cls, status):
        """Return a printable string given a numerical download status."""
        
        MAP = {
            cls.GRABBING: _("Downloading NZB file"),
            cls.QUEUED: _("Queued"),
            cls.DOWNLOADING: _("Downloading"),
            cls.PAUSED: _("Paused"),
            cls.QUICKCHECK: _("Quick check"),
            cls.DOWNLOADING_RECOVERY_DATA: _("Downloading recovery data"),
            cls.VERIFYING: _("Verifying"),
            cls.REPAIRING: _("Repairing"),
            cls.EXTRACTING: _("Extracting"),
            cls.JOINING: _("Joining"),
            cls.MOVING: _("Moving to download directory"),
            cls.SCRIPT: _("Executing script"),
            cls.FAILED: _("Failed"),
            cls.SUCCEEDED: _("Completed")
        }
        
        try:
            return MAP[status]
        except KeyError:
            raise ValueError("Unknown status: %r", status)


class StatusGroup:
    NOT_FULLY_LOADED = \
        Status.GRABBING | \
        Status.QUEUED | \
        Status.DOWNLOADING | \
        Status.PAUSED | \
        Status.DOWNLOADING_RECOVERY_DATA
    
    PROCESSING = \
        Status.QUICKCHECK | \
        Status.VERIFYING | \
        Status.REPAIRING | \
        Status.EXTRACTING | \
        Status.JOINING | \
        Status.MOVING | \
        Status.SCRIPT
    
    MOVABLE = \
        Status.QUEUED | \
        Status.DOWNLOADING | \
        Status.PAUSED
    
    INCOMPLETE = NOT_FULLY_LOADED | PROCESSING
    COMPLETE = Status.SUCCEEDED | Status.FAILED
    FULLY_LOADED = PROCESSING | COMPLETE
    ANY_STATUS = NOT_FULLY_LOADED | FULLY_LOADED


class Download(GObject):
    PERCENTAGE_PATTERN = re.compile(r"(\d{1,2})%")
    PIECES_PROGRESS_PATTERN = re.compile(r"(\d+)/(\d+)")
    RECOVERY_BLOCKS_PATTERN = re.compile(r"(\d+)")
    URL_PATTERN = re.compile(r"https?\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}"
        "(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*")
    HTML_LINK_PATTERN = re.compile(r"<a .*>.*</a>")
    
    status = gproperty(type=int, default=Status.QUEUED)
    priority = gproperty(type=int, default=Priority.NORMAL)
    id = gproperty(type=str)
    file_name = gproperty(type=str)
    name = gproperty(type=str)
    average_age = gproperty(type=object)
    time_left = gproperty(type=object)
    size = gproperty(type=object)
    size_left = gproperty(type=object)
    eta = gproperty(type=object)
    percentage = gproperty(type=int, minimum=0, maximum=100)
    script = gproperty(type=str)
    category = gproperty(type=str)
    postprocessing = gproperty(type=int, default=PostProcessing.DELETE)
    message_id = gproperty(type=int)
    
    postprocessing_time = gproperty(type=object)
    download_time = gproperty(type=object)
    completed = gproperty(type=object)
    storage_path = gproperty(type=str)
    error_message = gproperty(type=str)
    verification_percentage = gproperty(type=int, minimum=0, maximum=100)
    repair_percentage = gproperty(type=int, minimum=0, maximum=100)
    unpack_percentage = gproperty(type=int, minimum=0, maximum=100)
    recovery_block_count = gproperty(type=int, minimum=0)
    
    def __init__(self, download_id):
        GObject.__init__(self)
        
        self.id = download_id
    
    @property
    def size_downloaded(self):
        if self.size is not None:
            if self.size_left is None:
                return DataSize(bytes=self.size.bytes)
            else:
                return DataSize(bytes=self.size.bytes - self.size_left.bytes)
    
    def has_status(self, status):
        return bool(self.status & status)
    
    def update(self, data, paused):
        if "avg_age" in data:
            self.average_age = TimeDelta.from_string(data["avg_age"])
        
        # data["filename"] is returned by `QueueQuery` and data["nzb_name"]
        # is returned by `HistoryQuery`.
        if "nzb_name" in data:
            self.file_name = data["nzb_name"]
            
            if not self.name:
                self.name = self.get_clean_name(self.file_name)
        elif "filename" in data:
            self.file_name = data["filename"]
            self.name = data["filename"]
        
        if "name" in data:
            self.name = data["name"]
        
        # If a download is active, but the download speed is 0 Bytes/s
        # (e.g. because the internet connection is not available), 'timeleft'
        # will be '0:00:00', which is of course not correct.
        if paused or data.get("eta", None) == "unknown":
            self.time_left = None
            self.eta = None
        elif "timeleft" in data:
            self.time_left = TimeDelta.from_string(data["timeleft"])
            self.eta = DateTime.from_datetime(DateTime.now() + self.time_left)
        
        if "mbleft" in data:
            self.size_left = DataSize(mebibytes=float(data["mbleft"]))
        
        if "mb" in data:
            self.size = DataSize(mebibytes=float(data["mb"]))
        elif "bytes" in data:
            self.size = DataSize(bytes=int(data["bytes"]))
        
        if "script" in data:
            self.script = data["script"]
        
        if "msgid" in data and data["msgid"]:
            self.message_id = int(data["msgid"])
        
        if "cat" in data:
            self.category = data["cat"]
        
        if "unpackopts" in data:
            try:
                self.postprocessing = int(data["unpackopts"])
            except ValueError:
                pass
        
        if "priority" in data:
            self.priority = {
                "Force": Priority.TOP,
                "High": Priority.HIGH,
                "Low": Priority.LOW,
                "Normal": Priority.NORMAL
            }[data["priority"]]
        
        if "status" in data:
            # TODO: Not sure whether downloads in the post-processing queue in
            # general have the status 'Queued' or whether only downloads
            # requiring further transmissions are affected.
            if data["status"] == "Queued" and data.get("loaded", False):
                self.status = Status.DOWNLOADING_RECOVERY_DATA
            else:
                self.status = {
                    "Grabbing": Status.GRABBING,
                    "Queued": Status.QUEUED,
                    "Downloading": Status.DOWNLOADING,
                    "Paused": Status.PAUSED,
                    "QuickCheck": Status.QUICKCHECK,
                    "Fetching": Status.DOWNLOADING_RECOVERY_DATA,
                    "Verifying": Status.VERIFYING,
                    "Repairing": Status.REPAIRING,
                    "Extracting": Status.EXTRACTING,
                    "Moving": Status.MOVING,
                    "Running": Status.SCRIPT,
                    "Failed": Status.FAILED,
                    "Completed": Status.SUCCEEDED
                }[data["status"]]
        
        # Check whether this download represents the download of an NZB file
        # given an URL.
        # Unfortunately, SABnzbd doesn't seem to assign such downloads a
        # separate status, so LottaNZB needs to make an educated guess.
        # Also, the name and file name of the download is replaced by the
        # where the NZB file is being downloaded from.
        if self.status == Status.DOWNLOADING and not self.size \
            and not self.size_left and not self.percentage:
            match = self.URL_PATTERN.search(self.name)
            
            if match:
                self.status = Status.GRABBING
                self.name = match.group()
                self.file_name = match.group()
        
        if self.status == Status.DOWNLOADING and paused:
            self.status = Status.QUEUED
        
        if "percentage" in data:
            self.percentage = data["percentage"]
        elif self.has_status(StatusGroup.FULLY_LOADED):
            self.percentage = 100
        
        if "postproc_time" in data:
            self.postprocessing_time = TimeDelta(seconds=data["postproc_time"])
        
        if "download_time" in data:
            self.download_time = TimeDelta(seconds=data["download_time"])
        
        if "completed" in data:
            self.completed = DateTime.fromtimestamp(data["completed"])
        
        if "storage" in data:
            self.storage_path = data["storage"]
        
        if "fail_message" in data:
            # Some error messages contain a HTML link allowing the user to retry
            # the download.
            self.error_message = self.HTML_LINK_PATTERN.sub("",
                data["fail_message"]).strip(" ,")
        
        if self.has_status(Status.VERIFYING) and "action_line" in data:
            match = self.PIECES_PROGRESS_PATTERN.search(data["action_line"])
            
            if match:
                verified_pieces = float(match.group(1))
                total_pieces = float(match.group(2))
                
                self.verification_percentage = verified_pieces / \
                    total_pieces * 100
        
        if self.has_status(Status.REPAIRING) and "action_line" in data:
            match = self.PERCENTAGE_PATTERN.search(data["action_line"])
            
            if match:
                self.repair_percentage = float(match.group(1))
        
        if self.has_status(Status.EXTRACTING) and "action_line" in data:
            match = self.PIECES_PROGRESS_PATTERN.search(data["action_line"])
            
            if match:
                extracted_pieces = float(match.group(1))
                total_pieces = float(match.group(2))
                
                self.unpack_percentage = extracted_pieces / total_pieces * 100
        
        if self.has_status(Status.DOWNLOADING_RECOVERY_DATA) and \
            "action_line" in data:
            match = self.RECOVERY_BLOCKS_PATTERN.search(data["action_line"])
            
            if match:
                self.recovery_block_count = int(match.group(1))
    
    @staticmethod
    def get_clean_name(file_name):
        """
        Remove the file extensions, as well as symbols like "." or "_" from
        an NZB file name.
        """
        
        name = basename(file_name)
        name = re.compile(r"\.nzb", re.I).sub("", name)
        name = re.compile(r"[\._]").sub(" ", name)
        
        return name
