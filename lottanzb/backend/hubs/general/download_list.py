# Copyright (C) 2007-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk

from lottanzb.backend.hubs.general.download import StatusGroup

class DownloadListStore(gtk.ListStore):
    COLUMN = 0
    
    def __init__(self):
        self._model_filters = {};
        self._by_id = {};
        
        gtk.ListStore.__init__(self, object)
        
        self.connect("row-changed", self.on_row_changed)
    
    def get_filter_by_status(self, status):
        if status in self._model_filters:
            return self._model_filters[status]
        
        def func(model, listiter):
            download = model[listiter][self.COLUMN]
            
            return download and download.has_status(status)
        
        self._model_filters[status] = self.filter_new()
        self._model_filters[status].set_visible_func(func)
        
        return self._model_filters[status]
    
    def get_filter_complete(self):
        return self.get_filter_by_status(StatusGroup.COMPLETE)
    
    def get_filter_incomplete(self):
        return self.get_filter_by_status(StatusGroup.INCOMPLETE)
    
    def get_filter_processing(self):
        return self.get_filter_by_status(StatusGroup.PROCESSING)
    
    def get_filter_not_fully_loaded(self):
        return self.get_filter_by_status(StatusGroup.NOT_FULLY_LOADED)
    
    def get_filter_movable(self):
    	return self.get_filter_by_status(StatusGroup.MOVABLE)
    
    def remove(self, iter):
        """Remove download specified by `iter' and also drop it from `_by_id'"""
        
        download = self[iter][self.COLUMN]
        
        if download.id in self._by_id:
            del self._by_id[download.id]
        
        gtk.ListStore.remove(self, iter)
    
    def on_row_changed(self, model, path, iter):
        """Add download at `path' to `_by_id'"""
        
        download = self[iter][self.COLUMN]
        
        if download:
            self._by_id[download.id] = download
    
    def download_changed(self, download):
        """Call `row_changed' for a given `download'."""
        
        for row in self:
            if row and row[self.COLUMN] is download:
                self.row_changed(row.path, row.iter)
