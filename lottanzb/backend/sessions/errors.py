# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.core.environ import _

__all__ = [
    "SessionError",
    "SessionStartError",
    "SessionNotSpecifiedError",
    "SessionUnknownError",
    "SessionConnectionError",
    "SessionNoExecutableError",
    "SessionNoServerError",
    "SessionPermissionError",
    "SessionImportError"
]

class SessionError(Exception):
    message = ""
    
    def __init__(self, session=None, message=""):
        self.session = session
        
        if message:
            self.message = message
        
        Exception.__init__(self)
    
    def __str__(self):
        if self.message:
            return self.message
        elif self.session:
            return _("Unknown error in session '%s'.") % self.session.get_name()
        else:
            return _("Unknown error in session.")


class SessionStartError(SessionError):
    def __str__(self):
        prefix = _("Could not start session")
        
        if self.session:
            prefix = _("Could not start session '%s'") % self.session.get_name()
        
        if self.message:
            return "%s: %s" % (prefix, self.message)
        else:
            return prefix

class SessionNotSpecifiedError(SessionStartError):
    message = _("No session has been selected.")


class SessionUnknownError(SessionStartError):
    message = _("Unknown session type.")


class SessionConnectionError(SessionStartError):
    def __init__(self, session, connection_error, previous_success=False):
        self.connection_error = connection_error
        self.previous_success = previous_success
        
        SessionStartError.__init__(self, session,
            str(self.connection_error))


class SessionNoExecutableError(SessionStartError):
    message = _("Could not find the SABnzbd executable.")


class SessionNoServerError(SessionStartError):
    message = _("No news server has been specified.")


class SessionPermissionError(SessionStartError):
    message = _("Not allowed to launch SABnzbd.")
    
    def __init__(self, session=None, executable=None):
        SessionStartError.__init__(self, session)
        
        self.executable = executable
        
        if self.executable:
            extra_info = _("%s is probably not executable.") % self.executable
            
            self.message = "%s %s" % (self.message, extra_info)


class SessionImportError(SessionStartError):
    def __init__(self, session=None, module=""):
        SessionStartError.__init__(self, session)
        
        self.module = module
        self.message= _("Could not find the Python module '%s'.") % self.module
