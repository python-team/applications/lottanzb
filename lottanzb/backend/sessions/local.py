# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import re
import errno

import logging
LOG = logging.getLogger(__name__)

from time import time, sleep
from subprocess import Popen, PIPE
from distutils.spawn import find_executable
from os.path import isfile

from lottanzb import resources
from lottanzb.core.environ import _
from lottanzb.util.gobject_extras import gproperty
from lottanzb.config import hellanzb, sabnzbd
from lottanzb.config.section import ConfigSection
from lottanzb.config.errors import ConfigNotFoundError, LoadingError
from lottanzb.backend.sessions.base import BaseSession
from lottanzb.backend.sessions.errors import SessionNoServerError
from lottanzb.backend.interface import ConnectionInformation
from lottanzb.backend.interface.queries import ShutdownQuery, RestartQuery
from lottanzb.backend.interface.errors import (
    QueryError,
    QueryConnectionRefusedError,
    QueryUnsupportedAPIVersionError
)
from lottanzb.backend.sessions.errors import (
    SessionStartError,
    SessionNoExecutableError,
    SessionPermissionError,
    SessionImportError
)

__all__ = ["HELLANZB_CONFIG_FILE", "LocalSession"]

# Path to the HellaNZB configuration managed by LottaNZB <= 0.5.x.
HELLANZB_CONFIG_FILE = resources.get_config("hellanzb.conf")

class Config(ConfigSection):
    command = gproperty(type=str)


class SmartLocalConfig(sabnzbd.LocalConfig):
    def load(self, custom_config_file=""):
        try:
            sabnzbd.LocalConfig.load(self, custom_config_file)
        except ConfigNotFoundError:
            hella_config = hellanzb.Config(HELLANZB_CONFIG_FILE)
            
            try:
                hella_config.load()
            except LoadingError:
                pass
            else:
                self.import_hellanzb_config(hella_config)
                
                LOG.info(_("HellaNZB configuration file %s imported."),
                    hella_config.config_file)
            
            self.generate_api_key()
            self.save()


class LocalSession(BaseSession):
    abstract = False
    
    IMPORT_ERROR_PATTERN = re.compile("ImportError: No module named (.+)$")
    
    def __init__(self, component_manager):
        BaseSession.__init__(self, component_manager)
        
        self.sabnzbd_config = SmartLocalConfig()
        self.sabnzbd_config.load()
        
        self.interface = None
        
        if not self.config.command:
            self.config.command = self.get_sabnzbd_command()
    
    def get_connection_info(self):
        info = ConnectionInformation()
        info.update({
            "host": "localhost",
            "port": self.sabnzbd_config.misc.port,
            "username": self.sabnzbd_config.misc.username,
            "password": self.sabnzbd_config.misc.password,
            "api_key": self.sabnzbd_config.misc.api_key
        })
        
        return info
    
    def get_interface(self):
        """
        Get the SABnzbd daemon up and running, if this is not already the case
        and return the `Interface`.
        
        If something goes wrong, a `SessionError` will be raised.
        
        If the SABnzbd configuration stored in the `sabnzbd_config' property
        has been changed before the session is started (e.g. to add a server),
        the configuration is saved before attempting to launch SABnzbd.
        If SABnzbd was already running in such a case, it will be restarted
        automatically.
        """
        
        if not self.config.command or not isfile(self.config.command):
            raise SessionNoExecutableError(self)
        
        # Don't allow the local session to be loaded if no news server has been
        # specified. This might be the case if the user has launched SABnzbd
        # once but did not complete its setup wizard.
        # Even though one could later on provide the user with a warning message
        # indicating that no server is available, it's probably less disruptive
        # to just ask for the server information directly.
        if not len(self.sabnzbd_config.servers):
            raise SessionNoServerError(self)
        
        self.interface = BaseSession.get_interface(self)
        
        try:
            self.interface.handshake()
        except QueryConnectionRefusedError:
            if self.sabnzbd_config.is_dirty:
                self.sabnzbd_config.save()
            
            LOG.info(_("Launching SABnzbd (%s)..."), self.config.command)
            
            connection_established = False
            version_error = None
            
            args = (
                self.config.command,
                "--daemon",
                "--config-file=%s" % self.sabnzbd_config.config_file
            )
            
            try:
                process = Popen(args, stdout=PIPE, stderr=PIPE)
            except OSError as error:
                if error.errno == errno.ENOENT:
                    raise SessionNoExecutableError(self)
                elif error.errno == errno.EACCES:
                    raise SessionPermissionError(self, self.config.command)
                else:
                    raise SessionStartError(self, str(error))
            
            start_time = time()
            timeout = self.interface.START_TIMEOUT
            
            # Use `process.poll()` instead of `process.returncode`. Otherwise,
            # we might end up with a zombie SABnzbd process and an infinite
            # loop in LottaNZB.
            # Stop trying to connect to SABnzbd, when
            # - the SABnzbd version is not supported by LottaNZB
            # - a certain time has passed
            # - the process that forks the daemon terminates with a return code
            #   other than 0.
            while not process.poll() and not connection_established and \
                not version_error and time() - start_time < timeout:
                sleep(self.interface.HANDSHAKE_ATTEMPT_INTERVAL)
                
                try:
                    self.interface.handshake()
                except QueryConnectionRefusedError:
                    # SABnzbd is not fully started yet.
                    pass
                except QueryUnsupportedAPIVersionError as error:
                    version_error = error
                else:
                    connection_established = True
            
            if version_error:
                # The connection could be established, but the version is
                # not supported. Shut SABnzbd down again.
                self.stop_sabnzbd()
                raise version_error
            elif time() - start_time >= timeout:
                raise SessionStartError(self,
                    _("Could not start SABnzbd in %d seconds.") % timeout)
            elif process.returncode:
                output = process.stderr.read() or process.stdout.read()
                match = self.IMPORT_ERROR_PATTERN.search(output)
                
                if match:
                    raise SessionImportError(self, match.group(1))
                else:
                    raise SessionStartError(self, output)
            
            LOG.info(_("SABnzbd has been successfully launched."))
        else:
            if self.sabnzbd_config.is_dirty:
                self.sabnzbd_config.save()
                
                # It would not be necessary to restart SABnzbd if it has already
                # been configured once. If SABnzbd is however in its
                # wizard-state, simply adding the server using the configuration
                # API won't cause the first-start wizard to be completed.
                self.interface.run_query(RestartQuery())
        
        return self.interface
    
    def on_unload(self):
        self.stop_sabnzbd()
    
    def stop_sabnzbd(self):
        """Shut down SABnzbd by sending a `ShutdownQuery'.
        
        Note that for this method to work, `interface' must have been set.
        """
        
        if self.interface:
            LOG.info(_("Shutting down SABnzbd..."))
            
            try:
                self.interface.run_query(ShutdownQuery())
            except QueryError:
                pass
    
    @staticmethod
    def get_sabnzbd_command():
        names = ["sabnzbdplus", "sabnzbd", "SABnzbd", "SABnzbd.py"]
        
        for name in names:
            command = find_executable(name)
            
            if command:
                return command
