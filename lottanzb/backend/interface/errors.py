# Copyright (C) 2007-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.core.environ import _

__all__ = [
    "QueryError",
    "QueryConnectionError",
    "QueryUnknownHostError",
    "QueryConnectionRefusedError",
    "QueryHTTPError",
    "QueryUnsupportedAPIVersionError",
    "QueryResponseError",
    "QueryAuthenticationError",
    "QueryAPIKeyError",
    "QueryUsernamePasswordError",
    "QueryConnectionTimeout",
    "QueryRestartTimeout",
    "QueryArgumentsError",
    "QueryNotImplementedError"
]

class QueryError(Exception):
    def __init__(self, query, message=""):
        self.query = query
        self.message = message or _("Unknown error.")
    
    def __str__(self):
        return _("Query '%s' to %s failed: %s") % (
            self.query.arguments.get("mode", ""),
            self.query.connection_info.complete_host_string,
            self.message)


class QueryConnectionError(QueryError):
    def __init__(self, query, message=""):
        QueryError.__init__(self, query, message or _("IO error."))


class QueryUnknownHostError(QueryConnectionError):
    def __init__(self, query, message=""):
        QueryConnectionError.__init__(self, query,
            message or _("Unknown host."))


class QueryConnectionRefusedError(QueryConnectionError):
    def __init__(self, query, message=""):
        QueryConnectionError.__init__(self, query,
            message or _("Connection refused."))


class QueryHTTPError(QueryConnectionError):
    def __init__(self, query, code):
        self.code = code
        
        QueryConnectionError.__init__(self, query,
            _("HTTP error code %i.") % code)


class QueryUnsupportedAPIVersionError(QueryConnectionError):
    def __init__(self, query, version, required_version):
        host = query.connection_info.host
        
        if query.connection_info.is_local:
            message = _("SABnzbd version %s is installed, but %s is "
                "required." % (version, required_version))
        else:
            message = _("Host '%s' runs SABnzbd version %s, but %s is "
                "required.") % (host, version, required_version)
        
        QueryConnectionError.__init__(self, query, message)


class QueryResponseError(QueryConnectionError):
    def __init__(self, query, message=""):
        QueryConnectionError.__init__(self, query,
            message or _("Invalid response."))


class QueryAuthenticationError(QueryConnectionError):
    pass


class QueryAPIKeyError(QueryAuthenticationError):
    def __init__(self, query, message=""):
        QueryAuthenticationError.__init__(self, query,
            message or _("Invalid API key."))


class QueryUsernamePasswordError(QueryAuthenticationError):
    def __init__(self, query, message=""):
        QueryAuthenticationError.__init__(self, query,
            message or _("Invalid username or password."))


class QueryConnectionTimeout(QueryConnectionError):
    def __init__(self, query, message=""):
        QueryConnectionError.__init__(self, query,
            message or _("Query took too long to execute."))


class QueryRestartTimeout(QueryConnectionError):
    def __init__(self, query, timeout, message=""):
        QueryConnectionError.__init__(self, query,
            message or _("Restart didn't finish in %d seconds.") % timeout)


class QueryArgumentsError(QueryError):
    def __init__(self, query, message=""):
        QueryError.__init__(self, query, message or _("Invalid arguments."))


class QueryNotImplementedError(QueryArgumentsError):
    def __init__(self, query, message=""):
        QueryArgumentsError.__init__(self, query,
            message or _("Not implemented."))
