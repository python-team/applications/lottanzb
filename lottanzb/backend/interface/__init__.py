# Copyright (C) 2007-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import re

from threading import Semaphore, Condition, current_thread, Thread
from distutils.version import LooseVersion

from lottanzb.util.gobject_extras import GObject, gproperty, gsignal
from lottanzb.util.regex import IPv6_ADDRESS_PATTERN
from lottanzb.core.constants import DefaultPortNumber
from lottanzb.backend.interface.errors import QueryUnsupportedAPIVersionError
from lottanzb.backend.interface.queries import (
    Query,
    VersionQuery,
    AuthenticationQuery
)

__all__ = ["Interface", "ConnectionInformaton"]

class Interface(GObject):
    MINIMUM_VERSION = LooseVersion("0.5.2")
    
    # The time to wait between two attempts to establish a connection while
    # starting the SABnzbd daemon.
    HANDSHAKE_ATTEMPT_INTERVAL = 0.25
    
    # The time to wait until to give up the attempt of starting the SABnzbd
    # daemon. The default is an arbitrary choice of 15 seconds.
    # TODO: Probably not the best place to put this constant.
    START_TIMEOUT = 15
    
    gsignal("query-started", object)
    gsignal("query-completed", object)
    gsignal("query-success", object)
    gsignal("query-failure", object)
    
    def __init__(self, connection_info):
        GObject.__init__(self)
        
        self.version = None
        self.connection_info = connection_info
        
        # Used to synchronize the overridden methods.
        self._lock = Condition()
        
        # Holds all `Query' objects that are currently being run.
        self._active_queries = []
        
        # Holds all `Query' objects that are waiting to be run.
        self._waiting_queries = []
        
        # For each query in `_waiting_queries', this property provides the
        # corresponding `QueryWaitingThread'.
        # A mapping is removed as soon as a query is completed.
        self._waiting_threads = {}
    
    def run_query(self, query):
        """
        Run `query' asynchronously and emit the 'query-started' signal.
        
        If the query requires a newer version than `version', a
        `QueryUnsupportedAPIVersionError' will be raised.
        
        If it's not possible to start the query right away, an additional
        `QueryWaitingThread' is created so that the method doesn't block if
        the query needs to wait for other queries to be completed before it
        can even be started.
        
        Returns a thread, which is either `query' or the corresponding
        `QueryWaitingThread'. Either way, joining the thread will cause the
        caller to wait until the whole query has been completed.
        """
        
        if query.required_version and self.version < query.required_version:
            raise QueryUnsupportedAPIVersionError(query, self.version,
                query.required_version)
        
        with self._lock:
            self._lock.notify_all()
            
            if self.is_query_allowed_to_run(query):
                self._active_queries.append(query)
                self._run_query_immediately(query)
                
                return query
            else:
                self._waiting_queries.append(query)
                
                self._waiting_threads[query] = QueryWaitingThread(query, \
                    Interface._wait_in_queue, (self, query))
                self._waiting_threads[query].start()
                
                return self._waiting_threads[query]
    
    def _wait_in_queue(self, query):
        """Run `query' as soon as its allowed to according to
        `is_query_allowed_to_run'.
        """
        
        with self._lock:
            while True:
                self._lock.wait()
                
                if self.is_query_allowed_to_run(query):
                    self._waiting_queries.remove(query)
                    break
            
            # Let the next thread in the waiting queue proceed.
            self._lock.notify_all()
            self._active_queries.append(query)
        
        self._run_query_immediately(query)
    
    def _run_query_immediately(self, query):
        """Asynchronously runs `query'.
        
        It also connects to the signals of interest and emits 'query-started'.
        """
        
        query.connect("completed", self.on_query_completed)
        query.connect("success", self.on_query_success)
        query.connect("failure", self.on_query_failure)
        
        query.start(self.connection_info)
        
        self.emit("query-started", query)
    
    def get_response(self, query, *args, **kwargs):
        thread = self.run_query(query, *args, **kwargs)
        thread.join()
        
        if query.error:
            raise query.error
        
        return query.response
    
    def is_query_allowed_to_run(self, query):
        """Whether `query' is allowed to be run.
        
        This is not the case if:
        
        - `query' and all queries before `query' in the waiting queue don't
          affect each other.
        - `query' and all active queries don't affect each other.
        """
        
        # It's much more likely that the queries in `_waiting_queries' are
        # blocked because of an active query like `RestartQuery'. Thus, check
        # the active queries first.
        for active_query in self._active_queries:
            if active_query.affects_query(query) or \
                query.affects_query(active_query):
                return False
        
        for waiting_query in self._waiting_queries:
            if waiting_query is query:
                break
            
            if waiting_query.affects_query(query) or \
                query.affects_query(waiting_query):
                return False
        
        return True
    
    def handshake(self):
        """
        Perform two initial queries to the SABnzbd instance in order to check
        if the connection can be established.
        
        Also check if version of the SABnzbd instance the interface is
        connected to is a supported one and if the username and password as
        well as the API key is valid.
        
        May raise `QueryConnectionError`s of various types.
        """
        
        query = VersionQuery()
        self.version = self.get_response(query)
        
        if self.version != VersionQuery.SpecialVersion.TRUNK:
            if self.MINIMUM_VERSION > self.version:
                raise QueryUnsupportedAPIVersionError(query, self.version,
                    self.MINIMUM_VERSION)
        
        self.get_response(AuthenticationQuery())
    
    @property
    def waiting_queries(self):
        """The list of queries that have not been started yet."""
        
        return self._waiting_queries[:]
    
    @property
    def active_queries(self):
        """The list of queries that are currently being run."""
        return self._active_queries[:]
    
    def connect_filtered(self, query_class, signal_name, handler, *args):
        def internal_handler(interface, query, *args):
            if isinstance(query, query_class):
                handler(interface, query, *args)
        
        return self.connect(signal_name, internal_handler, *args)
    
    def connect_query_started(self, query_class, handler, *args):
        return self.connect_filtered(query_class, "query-started", handler,
            *args)
    
    def connect_query_completed(self, query_class, handler, *args):
        return self.connect_filtered(query_class, "query-completed", handler,
            *args)
    
    def connect_query_success(self, query_class, handler, *args):
        return self.connect_filtered(query_class, "query-success", handler,
            *args)
    
    def connect_query_failure(self, query_class, handler, *args):
        return self.connect_filtered(query_class, "query-failure", handler,
            *args)
    
    def on_query_completed(self, query):
        self.emit("query-completed", query)
        
        with self._lock:
            self._active_queries.remove(query)
            self._lock.notify_all()
    
    def on_query_success(self, query):
        self.emit("query-success", query)
    
    def on_query_failure(self, query, error):
        self.emit("query-failure", query)


class QueryWaitingThread(Thread):
    def __init__(self, query, waiting_target, args=()):
        self.query = query
        
        Thread.__init__(self, target=waiting_target, args=args)
    
    def join(self):
        Thread.join(self)
        
        self.query.join()


class QueryCache:
    """
    Cache query responses and allow clients to check if the response to a
    query differs from the one received before that.
    
    How to use this class:
    
     1. Create a new `QueryCache` object using
        my_cache = QueryCache()
     2. Add this snippet of code to the top of the method that handles successful
        queries:
        if my_chache.has_changed(query):
            return
    """
    
    def __init__(self):
        self.last_raw_response = ""
    
    def has_changed(self, query):
        """
        Check if the query passed to the method has the same response as the
        previous query.
        """
        
        if query.raw_response == self.last_raw_response:
            return False
        else:
            self.last_raw_response = query.raw_response
            return True


class ConnectionInformation(GObject):
    API_KEY_PATTERN = re.compile(r"^(|[a-z0-9]{32}$)")
    
    host = gproperty(type=str, default="localhost")
    port = gproperty(type=int, default=DefaultPortNumber.HTTP, minimum=1,
        maximum=2 ** 16 - 1)
    username = gproperty(type=str)
    password = gproperty(type=str)
    api_key = gproperty(type=str)
    https = gproperty(type=bool, default=False)
    
    # Indicates whether `host' is an IPv6 address or not.
    _host_is_ipv6_address = False
    
    def set_property(self, key, value):
        if key == "api_key" and not self.API_KEY_PATTERN.match(value):
            raise ValueError("The API key must consist of 32 alphanumeric "
                "characters.")
        if key == "host":
            self._host_is_ipv6_address = bool(IPv6_ADDRESS_PATTERN.match(value))
        
        GObject.set_property(self, key, value)
    
    @property
    def needs_authentication(self):
        return bool(self.username and self.password)
    
    @property
    def complete_host_string(self):
        """The host and the port, separated by ":"."""
        return "{0.host}:{0.port}".format(self)
    
    @property
    def protocol(self):
        """The protocol to be used. Will be "http" or "https" depending on the
        `https' property.
        """
        
        if self.https:
            return "https"
        else:
            return "http"
    
    @property
    def url(self):
        """The URL of the SABnzbd web interface.
        
        Note that IPv6 addresses are enclosed in square brackets because
        some browsers won't accept the URL otherwise.
        """
        
        host_format = "{0.host}"
        
        if self._host_is_ipv6_address:
            host_format = "[%s]" % host_format
        
        return ("{0.protocol}://" + host_format + ":{0.port}/").format(self)
    
    @property
    def api_url(self):
        """The URL of the SABnzbd API."""
        return self.url + "api"
    
    @property
    def is_local(self):
        # The IPv6 loopback address '::1' could be written in many other forms,
        # but this should be enough for most cases.
        return self.host in ("localhost", "127.0.0.1", "::1")
    
    @property
    def is_remote(self):
        return not self.is_local
