# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk

from distutils.version import LooseVersion

from lottanzb.core.environ import _
from lottanzb.config.sabnzbd.servers import ServerConfig
from lottanzb.backend import Backend
from lottanzb.backend.interface.queries import VersionQuery
from lottanzb.gui.prefs.base import Tab
from lottanzb.gui.prefs.widgets import WarningNotice
from lottanzb.gui.help import open_help
from lottanzb.gui.framework import MainDelegate, SlaveDelegate
from lottanzb.gui.framework.models import ListConfigSectionTreeModel
from lottanzb.gui.framework.proxy import (
    GObjectEndPoint, ToggleEndPoint, SpinButtonEndPoint, EntryEndPoint,
    PasswordEntryEndPoint, Conduit, ValidatableConduit, HostConduit,
    ValidatableConduitCollection)

__all__ = ["ServerTab", "ServerEditorPane", "AddServerDialog",
    "EditServerDialog"]

class ServerTab(Tab):
    abstract = False
    builder_file = "prefs_tab_servers"
    label = _("Servers")
    
    help_topic = "news-servers"
    
    def __init__(self, component_manager):
        Tab.__init__(self, component_manager)
        
        self._servers = None
        self._model = None
        
        self._server_toggle_column = ServerToggleColumn()
        self._server_toggle_column.renderer_toggle.connect("toggled",
            self.on_renderer_toggled)
        
        self._server_info_column = ServerInfoColumn()
        self._server_ssl_column = ServerSSLColumn()
        
        self.tree_view.append_column(self._server_toggle_column)
        self.tree_view.append_column(self._server_info_column)
        self.tree_view.append_column(self._server_ssl_column)
        
        self._selection = self.tree_view.get_selection()
        self._selection.set_mode(gtk.SELECTION_BROWSE)
        self._selection.connect("changed", self.on_selection_changed)
        
        interface = self._component_manager.get(Backend).interface
        
        # Make the whole tab insensitive if LottaNZB is connected to SABnzbd 0.6
        # or newer, because this version of LottaNZB is not compatible with the
        # new server configuration API.
        if interface.version == VersionQuery.SpecialVersion.TRUNK or \
            interface.version >= LooseVersion("0.6"):
            text = _("SABnzbd {version} is not supported yet.\n"
                "Use the <a href='{url}'>web interface</a> instead.").format(
                version=interface.version,
                url=interface.connection_info.url + "config/server")
            
            notice = WarningNotice.build_for_widget(self.container, text)
            notice.show()
            
            self.container.set_sensitive(False)
    
    def set_config(self, lottanzb_config, sabnzbd_config):
        self._servers = sabnzbd_config.servers
        self._model = ListConfigSectionTreeModel(self._servers)
        
        for signal in ("row-inserted", "row-deleted", "row-changed"):
            self._model.connect(signal,
                self.update_server_column_visibility)
        
        self.update_server_column_visibility()
        self.tree_view.set_model(self._model)
        
        if len(self._servers):
            self.select_server(self._servers[0])
    
    def update_server_column_visibility(self, *args):
        """Adjust the visibility of the tree view columns.
        
        Only display the check-boxes to enable or disable servers if
        
        - the list consists of more than one server
        - the only server in the list is disabled
        
        Only display the SSL column if at least one of the server has SSL
        enabled.
        """
        
        self._server_toggle_column.set_visible(len(self._model) > 1 or \
            (len(self._model) == 1 and not self._model[0][0].enable))
        
        self._server_ssl_column.set_visible(
            any([server.ssl for server in self._servers.values()]))
    
    def remove_selected_server(self):
        server = self.get_selected_server()
        
        if server:
            from lottanzb.gui.prefs import Window
            
            prefs_window = self._component_manager.get(Window)
            dialog = gtk.MessageDialog(parent=None, flags=0,
                type=gtk.MESSAGE_WARNING, buttons=gtk.BUTTONS_OK_CANCEL)
            dialog.set_property("text", _("Delete server?"))
            dialog.format_secondary_text(_("This cannot be undone."))
            
            def on_response(dialog, response):
                if response == gtk.RESPONSE_OK:
                    self._servers.remove(server)
                
                dialog.destroy()
            
            dialog.connect("response", on_response)
            dialog.set_transient_for(prefs_window.get_toplevel())
            dialog.show()

    def edit_selected_server(self):
        from lottanzb.gui.prefs import Window
        
        server = self.get_selected_server()
        
        if server:
            dialog = EditServerDialog(self._servers, server)
            dialog.show(self._component_manager.get(Window).get_toplevel())
    
    def get_selected_server(self):
        treeiter = self._selection.get_selected()[1]
        
        if treeiter:
            return self._model.get_value(treeiter, 0)
    
    def select_server(self, server):
        self._selection.select_path(self._model.get_index(server))
    
    def on_selection_changed(self, selection):
        server = self.get_selected_server()
        
        self.edit.set_sensitive(bool(server))
        self.remove.set_sensitive(bool(server))
    
    def on_add__clicked(self, widget):
        from lottanzb.gui.prefs import Window
        
        dialog = AddServerDialog(self._servers)
        dialog.show(self._component_manager.get(Window).get_toplevel())
    
    def on_edit__clicked(self, widget, *args):
        self.edit_selected_server()
    
    def on_tree_view__key_press_event(self, widget, event):
        if event.keyval == gtk.keysyms.Delete:
            self.remove_selected_server()
    
    def on_remove__clicked(self, widget):
        self.remove_selected_server()
    
    def on_tree_view__row_activated(self, treeview, path, column):
        self.edit_selected_server()
    
    def on_renderer_toggled(self, renderer, path):
        server = self._model.get_value(self._model.get_iter(path), 0)
        server.enable = not server.enable


class ServerToggleColumn(gtk.TreeViewColumn):
    def __init__(self):
        gtk.TreeViewColumn.__init__(self, "")
        
        self.renderer_toggle = gtk.CellRendererToggle()
        self.renderer_toggle.set_property("activatable", True)
        
        self.pack_start(self.renderer_toggle)
        self.set_cell_data_func(self.renderer_toggle, self.renderer_toggle_func)
        self.set_expand(False)
    
    def renderer_toggle_func(self, column, renderer, tree_model, rowref):
        server = tree_model.get_value(rowref, 0)
        if not server: return
        renderer.set_active(server.enable)


class ServerInfoColumn(gtk.TreeViewColumn):
    def __init__(self):
        gtk.TreeViewColumn.__init__(self, "")
        
        self.renderer_text = gtk.CellRendererText()
        self.pack_start(self.renderer_text)
        self.set_cell_data_func(self.renderer_text, self.renderer_text_func)
        self.set_expand(True)
    
    def renderer_text_func(self, column, renderer, tree_model, rowref):
        server = tree_model.get_value(rowref, 0)
        if not server: return
        if server.needs_authentication:
            username = server.username
        else:
            username = "<i>%s</i>" % _("No authentication required")
        
        if server.fillserver:
            # LottaNZB users are much more likely to be used to the term
            # 'Backup server' compared to 'Fillserver'.
            first_line = _("%s (Backup server)") % server.host
        else:
            first_line = server.host
        
        text = "<b>%s</b>\n<small>%s</small>" % \
            (first_line, username)
        
        renderer.set_property("markup", text)
        renderer.set_property("sensitive", server.enable)


class ServerSSLColumn(gtk.TreeViewColumn):
    def __init__(self):
        gtk.TreeViewColumn.__init__(self, "")
        
        self.renderer_ssl = gtk.CellRendererPixbuf()
        self.renderer_ssl.set_property("stock-size", gtk.ICON_SIZE_SMALL_TOOLBAR)
        self.pack_start(self.renderer_ssl)
        self.set_cell_data_func(self.renderer_ssl, self.renderer_ssl_func)
        self.set_expand(False)
    
    def renderer_ssl_func(self, column, renderer, tree_model, rowref):
        server = tree_model.get_value(rowref, 0)
        icon_name = ""
        
        if not server:
            return
        
        if server.ssl:
            icon_name = "stock_lock"
        
        renderer.set_property("icon-name", icon_name)
        renderer.set_property("sensitive", server.enable)


class ServerEditorPane(SlaveDelegate):
    """Slave delegate allowing the user to edit a server (object of type
    `config.sabnzbd.servers.ServerConfig').
    
    Any changes are instantaneously applied to the object.
    """
    
    builder_file = "server_editor_pane"
    
    def __init__(self, server_section, server):
        """Specify the server to be edited.
        
        The list of servers (an object of type `config.sabnzbd.servers.Config'.
        that the server is or will be part of needs to be passed to the
        constructor as well.
        
        It will be required to decide whether to display the 'Use as backup
        server' checkbox or not.
        """
        
        SlaveDelegate.__init__(self)
        
        self.server_section = server_section
        self.server = server
        
        # The collection of conduits used in the editor pane.
        # Can be used by clients to react to changes of its 'is-valid' property,
        # e.g. to make a button sensitive or insensitive.
        self.conduits = ValidatableConduitCollection()
        
        self.host_conduit = HostConduit(
            GObjectEndPoint(self.server, "host"),
            EntryEndPoint(self.host))
        self.host_conduit.mandatory = True
        
        self.conduits.add(self.host_conduit)
        
        self.conduits.add(ValidatableConduit(
            GObjectEndPoint(self.server, "username"),
            EntryEndPoint(self.username)))
        
        self.conduits.add(ValidatableConduit(
            GObjectEndPoint(self.server, "password"),
            PasswordEntryEndPoint(self.password)))
        
        self.conduits.add(Conduit(
            GObjectEndPoint(self.server, "port"),
            SpinButtonEndPoint(self.port)))
        
        self.conduits.add(Conduit(
            GObjectEndPoint(self.server, "ssl"),
            ToggleEndPoint(self.ssl)))
        
        self.conduits.add(Conduit(
            GObjectEndPoint(self.server, "fillserver"),
            ToggleEndPoint(self.fillserver)))
        
        self.conduits.add(Conduit(
            GObjectEndPoint(self.server, "connections"),
            SpinButtonEndPoint(self.connections)))
        
        unknown_server = True
        known_server_count = len(self.server_section)
        
        # For comparing servers, their properties are compared, instead of their
        # object IDs. The server object may have been copied in order to
        # implement a 'Save' button.
        for a_server in self.server_section.values():
            if a_server == server:
                unknown_server = False
        
        # Don't display the 'Use as backup server' checkbox in the list of
        # servers is empty or if the only server in it is the server currently
        # being edited.
        # However, always display the 'Use as backup server' checkbox if it
        # has already been activated by the user.
        self.fillserver.set_property("visible", any([
            known_server_count >= 2,
            known_server_count and unknown_server,
            server.fillserver]))
        
        # Automatically expand the 'Advanced Settings' expander if the server is
        # used as backup.
        self.advanced_settings.set_expanded(server.fillserver)
    
    def on_ssl__toggled(self, widget):
        """Automatically adjust the port number when SSL is enabled or disabled.
        
        When SSL is enabled and the port is still set to 119, the port will most
        likely need to be changed to 563.
        
        When SSL is disabled and the port is still set to 563, the port will
        most likely need to be changed to 119.
        """
        
        enabled = widget.get_active()
        
        if enabled and self.server.port == 119:
            self.server.port = 563
        
        if not enabled and self.server.port == 563:
            self.server.port = 119


class ServerDialog(MainDelegate):
    builder_file = "server_dialog"
    
    def __init__(self, server_section, server):
        MainDelegate.__init__(self)
        
        self.server = server
        self.server_section = server_section
        self.editor_pane = ServerEditorPane(server_section, server)
        self.attach_slave(self.editor_pane_parent, self.editor_pane)
        
        self.editor_pane.conduits.connect("notify::is-valid",
            self.on_validity_changed)
        self.on_validity_changed(self.editor_pane.conduits)
    
    def on_validity_changed(self, conduits, *args):
        self.save.set_property("sensitive", conduits.is_valid)
    
    def on_server_dialog__response(self, dialog, response):
        if response == gtk.RESPONSE_HELP:
            open_help("news-servers")
            dialog.stop_emission("response")
        else:
            dialog.hide()


class AddServerDialog(ServerDialog):
    def __init__(self, server_section):
        ServerDialog.__init__(self, server_section, ServerConfig(-1))
        
        self.get_toplevel().set_title(_("Add Server"))
    
    def on_server_dialog__response(self, dialog, response):
        """Add the newly created server to the list of servers in SABnzbd's
        configuration and hide the dialog.
        """
        
        if response == gtk.RESPONSE_OK:
            self.server_section.append(self.server)
        
        ServerDialog.on_server_dialog__response(self, dialog, response)


class EditServerDialog(ServerDialog):
    def __init__(self, server_section, server):
        self.original_server = server
        
        ServerDialog.__init__(self, server_section, server.deep_copy())
        
        self.get_toplevel().set_title(_("Edit Server"))
    
    def on_server_dialog__response(self, dialog, response):
        """Apply all changes made to the server to the server in SABnzbd's
        configuration and hide the dialog.
        """
        
        if response == gtk.RESPONSE_OK:
            self.original_server.merge(self.server)
        
        ServerDialog.on_server_dialog__response(self, dialog, response)
