# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import urllib
import gtk
import pango

from gettext import ngettext

from lottanzb.core.environ import _
from lottanzb.core.component import depends_on
from lottanzb.util.gobject_extras import gsignal
from lottanzb.util.misc import html_escape, open_folder
from lottanzb.backend import Backend
from lottanzb.backend.hubs.general import GeneralHub
from lottanzb.backend.hubs.general.download import Status, StatusGroup
from lottanzb.backend.hubs.general.download_list import DownloadListStore
from lottanzb.gui import download_properties
from lottanzb.gui.framework import SlaveDelegateComponent
from lottanzb.gui.framework.proxy import (GObjectEndPoint, Conduit,
    ToggleActionEndPoint)
from lottanzb.gui.framework.messages import warning

class DownloadList(SlaveDelegateComponent):
    depends_on(Backend)
    depends_on(GeneralHub)
    
    gsignal("selection-changed", object)
    
    builder_file = "download_list"
    
    # The download statuses that will represented by a higher row in the
    # download list, having a second line with additional status information.
    EXPANDED_STATUSES = StatusGroup.PROCESSING | Status.FAILED | \
        Status.DOWNLOADING | Status.GRABBING | Status.DOWNLOADING_RECOVERY_DATA
    
    def __init__(self, component_manager):
        SlaveDelegateComponent.__init__(self, component_manager)
        
        # Maps all actions to a download status or status group.
        # When a download is selected, an action must be sensitive if and only
        # if the download has such a status.
        self.action_status_sensitivity = {
            self.show_properties_dialog: StatusGroup.ANY_STATUS,
            self.open_folder: StatusGroup.COMPLETE
        }
        
        for action in (self.remove, self.move_up, self.move_up_to_top,
            self.move_down, self.move_down_to_bottom, self.pause):
            self.action_status_sensitivity[action] = \
                StatusGroup.MOVABLE
        
        self._general_hub = self._component_manager.get(GeneralHub)
        self._general_hub.downloads.connect("rows-reordered",
            self.update_action_sensitivity)
        
        backend = self._component_manager.get(Backend)
        self.open_folder.set_visible(backend.interface.connection_info.is_local)
        
        # When a download is selected, the `_pause_action_end_point' is
        # connected to a dedicated `DownloadPauseHubEndPoint' using
        # `_pause_conduit' such that the 'Pause' toggle action is kept in sync
        # with the status of the download.
        self._pause_action_end_point = ToggleActionEndPoint(self.pause)
        self._pause_conduit = None
        
        self.treeview.append_column(DownloadPrimaryColumn(self))
        self.treeview.append_column(DownloadSizeColumn())
        self.treeview.append_column(DownloadProgressColumn())
        
        self.treeview.set_search_equal_func(self.search_equal_function)
        
        self.treeview.get_selection().set_mode(gtk.SELECTION_BROWSE)
        self.treeview.get_selection().connect("changed",
            self.on_selection_changed)
        
        drag_targets = [("DOWNLOAD_ROW", gtk.TARGET_SAME_WIDGET, 0)]
        drop_targets = drag_targets + [("text/plain", 0, 1)]
        
        self.treeview.enable_model_drag_source(gtk.gdk.BUTTON1_MASK,
            drag_targets, gtk.gdk.ACTION_MOVE)
        self.treeview.enable_model_drag_dest(drop_targets, gtk.gdk.ACTION_MOVE)
        
        self.treeview.set_model(self._general_hub.downloads)
    
    def search_equal_function(self, model, column, key, iter):
        """
        `False' if the name of the download specified by `iter' contains `key',
        `True' otherwise.
        """
        
        download = model[iter][DownloadListStore.COLUMN]
        
        return key.lower() not in download.name.lower()
    
    def get_selected(self):
        selection = self.treeview.get_selection()
        
        if selection:
            model, treeiter = selection.get_selected()
            
            if treeiter:
                return model[treeiter][DownloadListStore.COLUMN]
    
    def on_selection_changed(self, selection):
        download = self.get_selected()
        
        if self._pause_conduit:
            self._pause_conduit.destroy()
        
        if download:
            self._pause_conduit = Conduit(
                DownloadPauseHubEndPoint(download, self._general_hub),
                self._pause_action_end_point)
        
        self.update_action_sensitivity(self)
        self.emit("selection-changed", download)
    
    def update_action_sensitivity(self, *args):
        download = self.get_selected()
        action_sensitivity = {}
        
        for action, status in self.action_status_sensitivity.items():
            action_sensitivity[action] = bool(download) and \
                download.has_status(status)
        
        if download:
            queue = self._general_hub.downloads.get_filter_movable()
            
            if len(queue):
                if queue[0][DownloadListStore.COLUMN] is download:
                    action_sensitivity[self.move_up] = False
                    action_sensitivity[self.move_up_to_top] = False
                if queue[-1][DownloadListStore.COLUMN] is download:
                    action_sensitivity[self.move_down] = False
                    action_sensitivity[self.move_down_to_bottom] = False
        
        for action, state in action_sensitivity.items():
            action.set_property("sensitive", state)
    
    def remove_download(self, download):
        # Lazy import
        from lottanzb.gui.main import MainWindow
        
        main_window = self._component_manager.load(MainWindow).get_toplevel()
        
        if download and download.has_status(StatusGroup.MOVABLE):
            title = _("Cancel download?")
            message = _("This cannot be undone.")
            response = warning(title, message=message, parent=main_window,
                buttons=gtk.BUTTONS_OK_CANCEL)
            
            if response == gtk.RESPONSE_OK:
                self._general_hub.delete_downloads(download)
    
    def on_treeview__key_press_event(self, treeview, event):
        download = self.get_selected()
        
        if download and event.keyval == gtk.keysyms.Delete:
            self.remove_download(download)
    
    def on_treeview__button_press_event(self, treeview, event):
        if event.type == gtk.gdk.BUTTON_PRESS and event.button == 3:
            self.context_menu.popup(None, None, None, event.button, event.time)
    
    def on_treeview__drag_data_get(self, treeview, context, selection, info,
        timestamp):
        download = self.get_selected()
        
        if download.has_status(StatusGroup.MOVABLE):
            selection.set("DOWNLOAD_ROW", 8, download.id)
    
    def on_treeview__drag_data_received(self, treeview, context, x, y,
        selection, info, timestamp):
        drop_info = treeview.get_dest_row_at_pos(x, y)
        
        if not selection.data:
            return
        
        if info == 0:
            self.handle_download_move(selection.data, drop_info)
        elif info == 1:
            for a_file in selection.data.replace("file://", "").split():
                if a_file.lower().endswith(".nzb"):
                    self._general_hub.add_file(urllib.unquote(a_file).strip())
    
    def handle_download_move(self, download_id, drop_info):
        # TODO: Is this the right place for this piece of code?
        downloads = self.treeview.get_model()
        download = downloads._by_id[download_id]
        filter_movable = downloads.get_filter_movable()
        source_index = 0
        
        for row in filter_movable:
            if row[DownloadListStore.COLUMN].id == download_id:
                source_index = row.path[0]
        
        not_movable_count = len(downloads.get_filter_processing())
        not_movable_count += len(downloads.get_filter_complete())
        
        target_index = None
        
        if drop_info:
            path, position = drop_info
            target_index = path[0] - not_movable_count
            
            if target_index - source_index == 1 and \
                position == gtk.TREE_VIEW_DROP_BEFORE:
                target_index += -1
            elif source_index - target_index == 1 and \
                position == gtk.TREE_VIEW_DROP_AFTER:
                target_index += 1
            elif source_index < target_index and \
                position == gtk.TREE_VIEW_DROP_BEFORE:
                target_index += -1
            elif source_index > target_index and \
                position == gtk.TREE_VIEW_DROP_AFTER:
                target_index += 1
        else:
            # Move the download to the bottom of the queue.
            target_index = len(filter_movable) - 1
        
        # When a download is dropped on the list of completed downloads or
        # downloads being processed, move it to the top of the queue.
        if target_index < 0:
            target_index = 0
        
        self._general_hub.move_download(download, target_index)
    
    def show_properties(self, *args):
        # Lazy import
        from lottanzb.gui.main import MainWindow
        
        download = self.get_selected()
        main_window = self._component_manager.load(MainWindow)
        
        dialog = self._component_manager.load(download_properties.Dialog)
        dialog.set_download(download)
        dialog.show(main_window.get_toplevel())
    
    def on_move_up_to_top__activate(self, widget):
        download = self.get_selected()
        
        if download:
            self._general_hub.force_download(download)
    
    def on_move_up__activate(self, widget):
        download = self.get_selected()
        
        if download:
            self._general_hub.move_download_up(download)
    
    def on_move_down__activate(self, widget):
        download = self.get_selected()
        
        if download:
            self._general_hub.move_download_down(download)
    
    def on_move_down_to_bottom__activate(self, widget):
        download = self.get_selected()
        
        if download:
            self._general_hub.move_download_down_to_bottom(download)
    
    def on_treeview__row_activated(self, treeview, path, column):
        """Directly open the folder containing the downloaded files for the
        selected download if and only if:
        
        - the download has been completed successfully
        - LottaNZB is connected to a local instance of SABnzbd
        - the download folder actually exists
        
        Otherwise, open the download properties dialog.
        
        TODO: Not sure if it's such a good idea to behave differently depending
        on the context.
        """
        
        self.show_properties()
    
    def on_show_properties_dialog__activate(self, widget):
        self.show_properties()
    
    def on_open_folder__activate(self, widget):
        download = self.get_selected()
        
        if download:
            open_folder(download.storage_path)
    
    def on_remove__activate(self, *args):
        self.remove_download(self.get_selected())


class DownloadPauseHubEndPoint(GObjectEndPoint):
    """Pauses or resumes `download' using `general_hub' given a boolean value
    received through the conduit.
    """
    
    PROPERTY = "status"
    
    def __init__(self, download, general_hub):
        self._general_hub = general_hub
        
        GObjectEndPoint.__init__(self, download)
    
    def get_value(self):
        return self._object.status == Status.PAUSED
    
    def set_value(self, paused):
        # The actual download status will be set in `backend.hubs.general'
        # in response to the newly started query.
        # It's not done twice in order to avoid race conditions with queue
        # queries.
        if paused:
            self._general_hub.pause_downloads(self._object)
        else:
            self._general_hub.resume_downloads(self._object)


class DownloadPrimaryColumn(gtk.TreeViewColumn):
    def __init__(self, download_list):
        gtk.TreeViewColumn.__init__(self, "")
        
        # TODO: Access to the download list is required because for certain
        # rows, it needs to know whether it represents the download currently
        # being selected.
        self.download_list = download_list
        self.renderer_text = gtk.CellRendererText()
        self.renderer_text.set_property("ellipsize", pango.ELLIPSIZE_MIDDLE)
        self.pack_start(self.renderer_text)
        self.set_cell_data_func(self.renderer_text, self.renderer_text_func)
        self.set_expand(True)
    
    def renderer_text_func(self, column, renderer, tree_model, rowref):
        download = tree_model.get_value(rowref, DownloadListStore.COLUMN)
        content = "<b>%s</b>" % html_escape(download.name)
        
        if download.has_status(DownloadList.EXPANDED_STATUSES):
            content += "\n"
            
            if download.status == Status.DOWNLOADING_RECOVERY_DATA:
                content += ngettext(
                    "Downloading {0.recovery_block_count} recovery block...",
                    "Downloading {0.recovery_block_count} recovery blocks...",
                    download.recovery_block_count)
            elif download.status == Status.VERIFYING:
                content += _("Verifying... ({0.verification_percentage}%)")
            elif download.status == Status.REPAIRING:
                content += _("Repairing... ({0.repair_percentage}%)")
            elif download.status == Status.EXTRACTING:
                content += _("Extracting... ({0.unpack_percentage}%)")
            elif download.status == Status.JOINING:
                content += _("Joining...")
            elif download.status == Status.MOVING:
                content += _("Moving to download folder...")
            elif download.status == Status.SCRIPT:
                content += _("Executing user script...")
            elif download.status == Status.FAILED:
                text = html_escape(download.error_message)
                
                if not self.download_list.get_selected() is download:
                    text = "<span foreground='red'>" + text + "</span>"
                
                content += text
            elif download.status == Status.DOWNLOADING:
                # `Download.time_left' may be None if its not known.
                if download.time_left is not None:
                    content += _("Downloading - {0.time_left.short} left")
                else:
                    content += _("Downloading - Remaining time unknown")
            elif download.status == Status.GRABBING:
                content += _("Downloading NZB file...")
        
        renderer.set_property("markup", content.format(download))
        renderer.set_property("sensitive", download.status != Status.PAUSED)


class DownloadSizeColumn(gtk.TreeViewColumn):
    def __init__(self):
        gtk.TreeViewColumn.__init__(self)
        
        self.renderer = gtk.CellRendererText()
        self.renderer.set_property("xalign", 1.0)
        self.pack_start(self.renderer)
        self.set_cell_data_func(self.renderer, self.renderer_func)
    
    def renderer_func(self, column, renderer, tree_model, rowref):
        download = tree_model[rowref][DownloadListStore.COLUMN]
        
        if download.size:
            size = str(download.size)
        else:
            size = ""

        renderer.set_property("text", size)
        renderer.set_property("sensitive", download.status != Status.PAUSED)


class DownloadProgressColumn(gtk.TreeViewColumn):
    def __init__(self):
        gtk.TreeViewColumn.__init__(self)
        
        self.renderer = gtk.CellRendererProgress()
        self.pack_start(self.renderer)
        self.set_cell_data_func(self.renderer, self.renderer_func)
        self.set_min_width(150)
    
    def renderer_func(self, column, renderer, tree_model, rowref):
        download = tree_model[rowref][DownloadListStore.COLUMN]
        
        if download.has_status(DownloadList.EXPANDED_STATUSES):
            renderer.set_property("ypad", 8)
        else:
            renderer.set_property("ypad", 0)
        
        renderer.set_property("value", download.percentage)
        renderer.set_property("sensitive", download.status != Status.PAUSED)
