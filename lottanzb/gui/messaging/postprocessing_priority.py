# Copyright (C) 2010-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""Allows the user to set the parameters used by SABnzbd for 'nice' and 'ionice'
to more conservatives values (the default values used by LottaNZB) such that
post-processing has the least influence on the user's system. By default,
SABnzbd doesn't adjust the post-processing priority using (io)nice.

It's meant for users who migrate to LottaNZB after having already set up
SABnzbd on their local machine. Changing the (io)nice configuration options for
a remote instance doesn't make sense.

The question asked to the user aims to avoid technical terms. It is only shown
if LottaNZB is connected to a local instance of SABnzb.
"""

import gtk

from lottanzb.core.environ import _
from lottanzb.core.component import depends_on
from lottanzb.core.constants import PostProcessing
from lottanzb.config.section import ConfigSection
from lottanzb.util.gobject_extras import GObject, gproperty, gsignal
from lottanzb.gui.messaging import base
from lottanzb.backend import Backend
from lottanzb.backend.hubs.config import ConfigHub

__all__ = ["MessageProvider", "Message"]

OPTIONS = ("nice", "ionice")

class Action:
    """Enumerations of actions that can be performed by the user.
    
    - `NONE': The user hasn't taken any action.
    - `CANCELED': The user has canceled a proposed configuration change.
    - `CHANGED': The user has performed a proposed configuration change.
    """
    
    NONE, CANCELED, CHANGED = range(0, 3)


class Config(ConfigSection):
    """Store information used by this module across sessions."""
    
    last_action = gproperty(
        nick="The last action performed by the user",
        type=int,
        default=Action.NONE)


class MessageProvider(base.MessageProvider):
    """When loading the component, an instance of `Message' is published only if
    
    - LottaNZB is connected to a local instance of SABnzbd.
    - The configuration options for (io)nice is set to to non-default values.
    - The user hasn't discarded such a message in the past.
    - SABnzbd is actually set up to perform post-processing.
    """
    
    depends_on(Backend)
    depends_on(ConfigHub)
    
    abstract = False
    
    def __init__(self, component_manager):
        base.MessageProvider.__init__(self, component_manager)
        
        if self.config.last_action == Action.CANCELED:
            # A previously proposed configuration change was canceled.
            return
        
        backend = self._component_manager.get(Backend)
        is_local = backend.interface.connection_info.is_local
        
        if not is_local:
            # LottaNZB is not connected to a local instance of SABnzbd.
            return
        
        misc_section = self._component_manager.get(ConfigHub).config.misc
        
        if misc_section.dirscan_opts == PostProcessing.NOTHING:
            # SABnzbd doesn't perform any post-processing.
            return
        
        uses_default_values = True
        
        for option in OPTIONS:
            default = misc_section.get_property_default_value(option)
            actual = misc_section.get_property(option)
            
            if default != actual:
                uses_default_values = False
        
        if uses_default_values:
            return
        
        message = Message(misc_section)
        message.connect("action", self.on_message_action)
        
        self.publish_message(message)
    
    def on_message_action(self, message, action):
        """Remember the action performed by the user."""
        self.config.last_action = action


class Message(base.Message, GObject):
    """The message letting the user change the (io)nice configuration options.
    
    The 'action' signal is emitted when the user has clicked one of the two
    buttons 'Cancel' or 'Decrease Priority'. Its only argument is a constant of
    `Action'.
    """
    
    gsignal("action", int)
    
    def __init__(self, misc_section):
        self._misc_section = misc_section
        
        base.Message.__init__(self, gtk.MESSAGE_QUESTION)
        GObject.__init__(self)
    
    def create_ui(self):
        icon = self.create_stock_icon(gtk.STOCK_DIALOG_QUESTION)
        label = self.create_multiline_label(
            _("Post-processing may slow down system"),
            _("It's recommended to decrease the priority of download "
                "post-processing. Post-processing may take a bit longer "
                "afterwards."))
        
        self._content_area.pack_start(icon, expand=False)
        self._content_area.pack_start(label)
        
        self._change_button = self.add_button(_("_Decrease Priority"),
            gtk.RESPONSE_OK)
        self._cancel_button = self.add_button(_("_Cancel"),
            gtk.RESPONSE_CANCEL)
    
    def on_widget__response(self, widget, response):
        if response == gtk.RESPONSE_OK:
            self._cancel_button.set_property("sensitive", False)
            
            for option in OPTIONS:
                default = self._misc_section.get_property_default_value(option)
                self._misc_section[option] = default
            
            self.emit("action", Action.CHANGED)
        elif response == gtk.RESPONSE_CANCEL:
            self.emit("action", Action.CANCELED)
        
        self.hide()
