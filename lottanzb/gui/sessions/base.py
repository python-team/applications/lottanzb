# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.gui.framework import SlaveDelegate
from lottanzb.core.component import implements, Component
from lottanzb.util.gobject_extras import gproperty
from lottanzb.backend.sessions.errors import SessionConnectionError

class IView:
    # True if the view is only capable of asking for input from the user if
    # a previous attempt to launch the session resulted in a `SessionError'.
    session_error_required = False
    
    # Help topic to be opened when clicking 'Help'.
    help_topic = ""
    
    def get_session_type(self):
        pass
    
    def get_radio_button_text(self):
        pass
    
    def set_session_error(self, error):
        pass
    
    def restore(self):
        """Revert all changes made by the user while the view was displayed."""
        
        pass


class View(SlaveDelegate, Component):
    implements(IView)
    
    abstract = True
    session_error_required = False
    help_topic = "setting-up"
    
    error_message = gproperty(type=str)
    is_valid = gproperty(type=bool, default=True)
    
    def __init__(self, component_manager):
        Component.__init__(self, component_manager)
        SlaveDelegate.__init__(self)
        
        self._session = self._component_manager.load(self.get_session_type())
        self._session_config_copy = self._session.config.deep_copy()
    
    def get_session_type(self):
        raise NotImplementedError
    
    def get_radio_button_text(self):
        raise NotImplementedError
    
    def set_session_error(self, error):
        if isinstance(error, SessionConnectionError):
            self.set_connection_error(error.connection_error)
        elif error.message:
            self.error_message = error.message
        else:
            self.error_message = str(error)
    
    def set_connection_error(self, error):
        if error.message:
            self.error_message = error.message
        else:
            self.error_message = str(error)
    
    def restore(self):
        """Revert all changes made by the user while the view was displayed.
        
        By default, the configuration of the individual session is restored.
        """
        
        self._session.config.merge(self._session_config_copy)
