# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import sys
import gtk

import logging
LOG = logging.getLogger(__name__)

from lottanzb.core.component import ExtensionPoint
from lottanzb.util.gtk_extras import gtk_lock
from lottanzb.config.lotta import ConfigComponent
from lottanzb.backend import Backend
from lottanzb.backend.sessions.errors import (
    SessionError, SessionNotSpecifiedError, SessionUnknownError)

from lottanzb.gui.framework import MainDelegateComponent
from lottanzb.gui.help import open_help
from lottanzb.gui.sessions.base import IView
from lottanzb.gui.sessions.local import LocalView
from lottanzb.gui.sessions.remote import RemoteView

class SelectionDialog(MainDelegateComponent):
    builder_file = "session_selection_dialog"
    views = ExtensionPoint(IView)
    
    def __init__(self, component_manager):
        config = component_manager.load(ConfigComponent).root
        
        self.sessions_config = config.backend.sessions
        
        # Holds the value of the configuration option 'backend.sessions.active'.
        # When the user decides to quit the selection dialog, the option will
        # be restored to this value.
        self.original_active_session = self.sessions_config.active
        
        # Holds a `SessionError' if the session selection dialog was launched
        # because it occurred. Further failed attempts to start a new session
        # won't cause the property to be changed and launching the session
        # selection dialog even though no `SessionError' caused the backend to
        # be unloaded, the property will remain None.
        self.original_session_error = None
        self.view = None
        
        MainDelegateComponent.__init__(self, component_manager)
        
        self._component_manager.connect("component-loaded",
            self.on_component_loaded)
    
    def create_ui(self):
        first_radio_button = None
        
        for view_cls in self.views:
            view = self._component_manager.load(view_cls)
            view.connect("notify::is-valid", self.on_view_validation_changed)
            view.connect("notify::error-message", self.on_view_error_message)
            
            radio_button = gtk.RadioButton()
            
            if first_radio_button is None:
                first_radio_button = radio_button
            else:
                radio_button.set_group(first_radio_button)
            
            radio_button.set_use_underline(True)
            radio_button.set_label(view.get_radio_button_text())
            radio_button.connect("toggled",
                gtk_lock.locked(self.on_radio_button_toggled), view)
            radio_button.show()
            
            view.set_data("radio-button", radio_button)
            
            self.radio_buttons.pack_start(radio_button, expand=False)
        
        self.get_toplevel().set_default_response(gtk.RESPONSE_OK)
    
    def on_radio_button_toggled(self, widget, view):
        if widget.get_active():
            self.set_view(view)
    
    def on_view_validation_changed(self, view, *args):
        if self.view is view:
            self.update_save_button_sensitivity()
    
    def on_view_error_message(self, view, *args):
        if self.view is view:
            self.set_error_message(view.error_message)
    
    def preselect_session_type(self, session_type, parent_window=None):
        """Automatically select `session_type' as the new active session.
        
        The radio buttons will not be displayed and if no user input is required
        for the user to change to the new session, the session selection dialog
        will not be shown at all. Otherwise, its `show' method is called
        with the argument `parent_window'.
        """
        
        for view_cls in self.views:
            view = self._component_manager.get(view_cls)
            
            if session_type is view.get_session_type():
                with gtk_lock:
                    self.set_view(view)
                    self.radio_buttons.set_property("visible", False)
                    
                    if view.session_error_required:
                        self.get_toplevel().response(gtk.RESPONSE_OK)
                    else:
                        self.show(parent_window)
                
                return
        
        raise ValueError("Unknown session type %r." % session_type)
    
    def set_view(self, view):
        if not self.get_slave(self.view_container) is view:
            with gtk_lock:
                self.detach_slave(self.view_container)
                self.view = view
                
                if view:
                    self.attach_slave(self.view_container, view)
                    self.view_container.show()
                    active = view.get_session_type().get_name()
                else:
                    self.view_container.hide()
                    active = ""
                
                self.sessions_config.active = active
                view.get_data("radio-button").set_active(True)
                
                self.set_error_message(view.error_message)
                self.update_save_button_sensitivity()
    
    def set_error_message(self, error_message):
        with gtk_lock:
            self.error_container.set_property("visible", bool(error_message))
            self.error.set_label(error_message)
    
    def get_view_for_session(self, session):
        for view_cls in self.views:
            view = self._component_manager.get(view_cls)
            
            if isinstance(session, view.get_session_type()):
                return view
    
    def set_session_error(self, error):
        if not isinstance(error, SessionError):
            raise ValueError("gui.sessions.SelectionDialog.set_session_error "
                "expects an error of type "
                "backend.sessions.error.SessionError.")
        
        if self.view is None:
            self.original_session_error = error
        
        if isinstance(error, (SessionNotSpecifiedError, SessionUnknownError)):
            self.set_view(self._component_manager.get(LocalView))
            return
        
        view = self.get_view_for_session(error.session)
        
        if view:
            view.set_session_error(error)
            
            self.set_view(view)
    
    def update_save_button_sensitivity(self):
        sensitive = self.view and self.view.is_valid
        
        with gtk_lock:
            self.save.set_property("sensitive", sensitive)
    
    def restore(self):
        """Restore the value of the configuration option
        'backend.sessions.active' to its original value.
        """
        
        self.sessions_config.active = self.original_active_session
    
    def on_unload(self):
        for view_cls in self.views:
            self._component_manager.unload(view_cls)
        
        MainDelegateComponent.on_unload(self)
    
    def on_component_loaded(self, component_manager, component):
        if isinstance(component, Backend):
            self._component_manager.unload(self)
    
    def on_widget__response(self, dialog, response):
        if response in (gtk.RESPONSE_CANCEL, gtk.RESPONSE_DELETE_EVENT):
            # Revert all changes made by the user.
            self.restore()
            
            for view_cls in self.views:
                view = self._component_manager.get(view_cls)
                view.restore()
            
            # If the session selection dialog was opened because of a
            # `SessionError', the application it shut down. Otherwise, the
            # previous session is restarted again with its original
            # configuration.
            if self.original_session_error:
                sys.exit()
            else:
                self._component_manager.load_async(Backend)
                self.hide()
        elif response == gtk.RESPONSE_OK:
            self._component_manager.load_async(Backend)
            self.hide()
        elif response == gtk.RESPONSE_HELP and self.view:
            open_help(self.view.help_topic)
        
        dialog.stop_emission("response")
