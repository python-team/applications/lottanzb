# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import logging
LOG = logging.getLogger(__name__)

from lottanzb.core.environ import _
from lottanzb.core.constants import DefaultPortNumber
from lottanzb.util.signalmanager import SignalSynchronicityProvider
from lottanzb.util.gtk_extras import gtk_lock
from lottanzb.gui.sessions.base import View
from lottanzb.backend.sessions.remote import RemoteSession
from lottanzb.gui.framework.proxy import (
    EntryEndPoint, ValidatableConduit, GObjectEndPoint, SpinButtonEndPoint,
    ValidatableConduitCollection, HostConduit)
from lottanzb.backend.interface.queries import (AuthenticationType,
    AuthenticationTypeQuery)
from lottanzb.backend.interface.errors import QueryConnectionRefusedError


class RemoteView(View):
    abstract = False
    builder_file = "session_selection_remote"
    help_topic = "remote-access"
    
    def __init__(self, component_manager):
        View.__init__(self, component_manager)
        
        self._query_synchronicity = SignalSynchronicityProvider("completed",
            self.on_authentication_type_query)
        
        self._conduits = ValidatableConduitCollection()
        self._conduits_by_key = {
            "host": HostConduit(
                GObjectEndPoint(self._session.config, "host"),
                EntryEndPoint(self.host)),
            "port": ValidatableConduit(
                GObjectEndPoint(self._session.config, "port"),
                SpinButtonEndPoint(self.port))
        }
        
        for key in ("api_key", "username", "password"):
            self._conduits_by_key[key] = ValidatableConduit(
                GObjectEndPoint(self._session.config, key),
                EntryEndPoint(getattr(self, key)))
        
        self._conduits_by_auth_type = {
            self._conduits_by_key["api_key"]: \
                AuthenticationType.API_KEY,
            self._conduits_by_key["username"]: \
                AuthenticationType.USERNAME_PASSWORD,
            self._conduits_by_key["password"]: \
                AuthenticationType.USERNAME_PASSWORD
        }
        
        for key in ("host", "port"):
            self._conduits_by_key[key].mandatory = True
        
        for conduit in self._conduits_by_key.values():
            self._conduits.add(conduit)
        
        self._session.config.connect("property-changed",
            self.on_session_config_changed)
        
        self.refresh_authentication_type()
        
        self._conduits.connect("notify::is-valid", self.on_validation_changed)
        self.on_validation_changed()
    
    def on_validation_changed(self, *args):
        self.is_valid = self._conduits.is_valid
    
    def on_session_config_changed(self, section, parent, key, value):
        """Perform actions when some of the configuration values are changed.
        
        If the port is changed to the default HTTPS port, HTTPS is automatically
        enabled. If the port is changed from the default HTTPS port to something
        else, HTTPS is automatically disabled.
        
        TODO: In the future, it might be possible to manually specify this using
        LottaNZB's UI.
        
        If either the port or the host is changed, the SABnzbd listening at it
        (if any) is asked what types of authentication it requries.
        """
        
        if key == "port":
            connection_info = self._session.get_connection_info()
            https = value == DefaultPortNumber.HTTPS
            changed = self._session.config.set_property("https", https)
            
            if changed:
                if https:
                    message = _("HTTPS is automatically enabled for the "
                        "connection to '%s'.")
                else:
                    message = _("HTTPS is automatically disabled for the "
                        "connection to '%s'.")
                
                LOG.info(message % connection_info.complete_host_string)
        
        if key in ("host", "port"):
            self.refresh_authentication_type()
    
    def refresh_authentication_type(self):
        if self._session.config.host and self._session.config.port:
            query = AuthenticationTypeQuery()
            
            self._query_synchronicity.connect(query)
            
            interface = self._session.get_interface()
            interface.run_query(query)
        else:
            self.error_message = ""
            self.set_authentication_type(AuthenticationType.NOTHING)
    
    def on_authentication_type_query(self, query):
        if query.error:
            self.set_connection_error(query.error)
            self.set_authentication_type(AuthenticationType.NOTHING)
        else:
            self.set_authentication_type(query.response)
    
    def set_authentication_type(self, authentication_type):
        with gtk_lock:
            self.api_key_container.set_visible(
                authentication_type == AuthenticationType.API_KEY)
            self.username_password_container.set_visible(
                authentication_type == AuthenticationType.USERNAME_PASSWORD)
        
        for conduit, value in self._conduits_by_auth_type.items():
            conduit.mandatory = value == authentication_type
    
    def set_connection_error(self, error):
        if isinstance(error, QueryConnectionRefusedError):
            connection_info = error.query.connection_info
            
            # When connecting to 'localhost', SABnzbd doesn't need to allow
            # access from other computers.
            if connection_info.is_local:
                info = _("Ensure that SABnzbd is running.")
            else:
                info = _("Ensure that SABnzbd is running on '%s' and allows "
                    "access from other computers.") % connection_info.host
            
            self.error_message = error.message + " " + info
        else:
            View.set_connection_error(self, error)
    
    def get_session_type(self):
        return RemoteSession
    
    def get_radio_button_text(self):
        return _("_Manage the downloads of another computer")
