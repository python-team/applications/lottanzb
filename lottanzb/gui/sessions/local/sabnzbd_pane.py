# Copyright (C) 2010-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""
UI and application logic aiming to help the user get a supported version of
SABnzbd in place.
"""

import gtk
import webbrowser

try:
    import apt
except ImportError:
    apt = None

from os.path import isfile
from distutils.version import LooseVersion
from distutils.spawn import find_executable
from subprocess import Popen, PIPE

from lottanzb.core.environ import _
from lottanzb.util.timer import Timer
from lottanzb.util.threading_extras import Thread
from lottanzb.gui.framework import SlaveDelegate
from lottanzb.gui.framework.proxy import (FileChooserButtonEndPoint,
    GObjectEndPoint, ValidatableConduit, ValidationInvalidError)
from lottanzb.backend.interface import Interface
from lottanzb.backend.interface.errors import QueryUnsupportedAPIVersionError
from lottanzb.backend.sessions.local import LocalSession
from lottanzb.backend.sessions.errors import (SessionConnectionError,
    SessionNoExecutableError, SessionPermissionError, SessionImportError)

# TODO: It would theoretically be possible to pass parameters to the help page
# indicating whether what distribution is in use and what version of SABnzbd
# is installed, if any.
HELP_URL = "http://www.lottanzb.org/help/installing-and-upgrading-sabnzbd/"

class FileConduit(ValidatableConduit):
    """Validatable conduit for handling file names.
    
    The conduit is marked as valid only if the file exists. It's possible for
    the model endpoint to refer to a file that does not exist. However, the
    conduit will regularly check whether the file has been created.
    """
    
    def __init__(self, model_endpoint, *endpoints):
        ValidatableConduit.__init__(self, model_endpoint, *endpoints)
        
        self._timer = Timer(250, self.on_timer_tick)
        self._timer.start()
    
    def validate_endpoint_value(self, endpoint, value):
        ValidatableConduit.validate_endpoint_value(self, endpoint, value)
        
        if not isfile(value):
            raise ValidationInvalidError(self, endpoint, value)
    
    def on_timer_tick(self, timer):
        if not self.is_valid:
            self.publish_model_endpoint()
    
    def destroy(self):
        self._timer.stop()
        
        ValidatableConduit.destroy(self)


class APTPackageVersionThread(Thread):
    """Determines status of a package on a dpkg-based distribution.
    
    After the execution of the thread, the properties `installed_version'
    and `candidate_version' will be set or None if the package isn't known.
    
    - `installed_version' holds the version of the installed package
      or None if it's not installed.
    - `candidate_version' holds the version of the package that can be
      installed, which might be greater than `installed_version' if an update
      is available that has not yet been installed by the user.
    
    Please note that the thread will only produce a result if the Python module
    `apt' is available.
    """
    
    def __init__(self, package):
        if not isinstance(package, str) or not package:
            raise ValueError("%r is not a valid package name" % package)
        
        self.installed_version = None
        self.candidate_version = None
        self.package = package
        
        Thread.__init__(self)
    
    def run(self):
        try:
            package = apt.Cache()[self.package]
        except (AttributeError, KeyError):
            pass
        else:
            installed_version = None
            candidate_version = None
            
            # The attribute `installedVersion' was renamed to
            # `installed.version' starting with python-apt 0.7.10.
            try:
                if package.installed:
                    installed_version = package.installed.version
            except AttributeError:
                installed_version = package.installedVersion
            
            # The attribute `candidateVersion' was renamed to
            # `candidate.version' starting with python-apt 0.7.10.
            try:
                if package.candidate:
                    candidate_version = package.candidate.version
            except AttributeError:
                candidate_version = package.candidateVersion
            
            if installed_version:
                self.installed_version = LooseVersion(installed_version)
            
            if candidate_version:
                self.candidate_version = LooseVersion(candidate_version)
        
        self.emit("completed")


class QuickInstallThread(Thread):
    """Provides a graphical way of installing one or more packages on a
    dpkg-based distribution.
    
    The packages to be installed can be passed to the constructor.
    The thread invokes the application 'apturl' if available.
    After the execution of the thread, the 'completed' signal will be emitted
    and the property `result' will hold a value of the enumeration
    `QuickInstallResult'.
    """
    
    def __init__(self, *packages):
        self.packages = packages
        self.result = None
        
        Thread.__init__(self)
    
    def run(self):
        try:
            args = ("nohup", "apturl", "apt:" + ",".join(self.packages))
            process = Popen(args, stdout=PIPE, stderr=PIPE)
            process.communicate()
        except OSError:
            # 'apturl' most probably could not be found.
            self.result = QuickInstallResult.ERROR
        else:
            self.result = int(process.returncode)
        
        self.emit("completed")


class QuickInstallResult:
    """Enumeration of various results of the execution of a `QuickInstallThread'
    
    - `INSTALLED': All requested packages have been succesfully installed or
      were already installed.
    - `ABORTED': The user aborted the installation of one or more packages.
    - `ERROR': There was error while installing the packages, or the utility
      'apturl' could not be found.
    - `INVALID_ARGUMENTS': The arguments passed to 'apturl' were invalid.
    """
    
    INSTALLED, ABORTED, ERROR, INVALID_ARGUMENTS = range(0, 4)


class QuickUpgradeThread(Thread):
    """Runs the application 'update-manager', if available.
    
    It will wait for 'update-manager' to be completed, but doesn't give any
    information on whether some updates were actually installed or not.
    """
    
    COMMAND = "update-manager"
    
    def run(self):
        try:
            args = ("nohup", self.COMMAND)
            process = Popen(args)
            process.communicate()
        except OSError:
            pass
        
        self.emit("completed")


class State(SlaveDelegate):
    """Abstract class representing a view to be displayed within `SABnzbdPane'.
    
    It may contain information about certain actions to be performed by the user
    or itself provide actions to the user in the form of links or buttons.
    
    Subclasses can override the methods `can_handle_session_error' and
    `allows_manual_input' to indicate the properties and capabilities of the
    view.
    
    The class also provides some helper methods such as `create_label' and
    `run_apt_version_thread'.
    """
    
    def __init__(self, sabnzbd_pane):
        self.sabnzbd_pane = sabnzbd_pane
        
        SlaveDelegate.__init__(self)
    
    @staticmethod
    def create_label(markup=""):
        """Creates a new object of type `gtk.Label'.
        
        Some of its properties are set to sane default values and it's also
        possible to pass a markup as a method argument.
        
        The label is returned and not automatically added to the view.
        """
        
        label = gtk.Label()
        label.set_alignment(0.0, 0.5)
        label.set_line_wrap(True)
        label.set_use_underline(True)
        
        if markup:
            label.set_markup(markup)
        
        label.show()
        
        return label
    
    @staticmethod
    def can_handle_session_error(error):
        """Whether the state is capable of handling `error` of type
        `SessionError', i.e. presenting the user with helpful information
        for dealing with this particular error.
        
        Can be overridden by subclasses and returns False by default.
        """
        
        return False
    
    def allows_manual_input(self):
        """Whether the `FileChooserButton' for manually selecting the SABnzbd
        executable should be visible or not.
        
        Can be overridden by subclasses and returns False by default.
        """
        
        return False
    
    def run_apt_version_thread(self, callback):
        """Runs a new `APTPackageVersionThread' for the package 'sabnzbdplus'.
        `callback' will be invoked when the thread is complete.
        """
        
        thread = APTPackageVersionThread("sabnzbdplus")
        thread.connect_async("completed", callback)
        thread.start()


class SearchingState(State):
    """Determines the status of the 'sabnzbdplus' package on the user's machine.
    
    Depending on this status, the user is routed to a certain other state.
    
    This view is activated if a `SessionNoExecutableError' or
    `QueryUnsupportedAPIVersionError' was raised.
    """
    
    builder_file = "session_selection_local_sabnzbd_pane_searching"
    
    def create_ui(self):
        # GTK >= 2.20 supports `gtk.Spinner'.
        if hasattr(gtk, "Spinner"):
            self.spinner = gtk.Spinner()
            self.spinner.start()
            
            self.spinner_container.add(self.spinner)
            self.spinner_container.show_all()
    
    def handle_attached(self, sabnzbd_pane):
        """Run the `APTPackageVersionThread' as soon as the view is visible."""
        
        self.run_apt_version_thread(self.on_apt_version_thread_completed)
    
    def on_apt_version_thread_completed(self, thread):
        """React to the result of the `APTPackageVersionThread'."""
        
        installed_version = thread.installed_version
        candidate_version = thread.candidate_version
        minimum_version = Interface.MINIMUM_VERSION
        
        if candidate_version and candidate_version >= minimum_version:
            if installed_version:
                if installed_version < minimum_version:
                    self.sabnzbd_pane.set_state(QuickUpgradeState)
                else:
                    # In this situation, the SABnzbd package was found to
                    # be installed, but the executable itself could not be
                    # found. There might be a problem with the installation or
                    # a unofficial package is being used. In either case, it's
                    # certainly better to display a 'Not found' message rather
                    # than leaving the `gtk.Spinner' spinning.
                    self.sabnzbd_pane.set_state(NotFoundState)
            else:
                self.sabnzbd_pane.set_state(QuickInstallState)
        elif self.sabnzbd_pane.command_conduit.is_valid:
            self.sabnzbd_pane.set_state(UpgradeState)
        else:
            self.sabnzbd_pane.set_state(NotFoundState)
    
    @staticmethod
    def can_handle_session_error(error):
        """Returns True if `error' is of type `SessionNoExecutableError' or
        `SessionConnectionError' with an embedded `ConnectionError' of type
        `QueryUnsupportedAPIVersionError'.
        """
        
        if isinstance(error, SessionNoExecutableError):
            return True
        
        if isinstance(error, SessionConnectionError):
            connection_error = error.connection_error
            
            if isinstance(connection_error, QueryUnsupportedAPIVersionError):
                return True
        
        return False


class QuickInstallState(State):
    builder_file = "session_selection_local_sabnzbd_pane_quick_install"
    
    def handle_attached(self, sabnzbd_pane):
        """Set the SABnzbd executable in advance to '/usr/bin/sabnzbdplus'.
        
        Thus the user is also allowed to manually install SABnzbd, causing the
        'OK' button to be made sensitive.
        
        TODO: The 'OK' button is made sensitive way to early, before all
        files that are part of the 'sabnzbdplus' package are installed.
        Let's hope that the user is wise enough not to hit 'OK' too early.
        """
        
        self.sabnzbd_pane.config.command = "/usr/bin/sabnzbdplus"
    
    def on_install_button__clicked(self, button):
        """Initiate the installation of the 'sabnzbdplus' package.
        
        Until `QuickInstallThread' is completed, the `install_button' will be
        made insensitive, preventing the user from clicking it twice.
        """
        
        self.install_button.set_label(_("Installing..."))
        self.install_button.set_property("sensitive", False)
        
        thread = QuickInstallThread("sabnzbdplus")
        thread.connect_async("completed",
            self.on_quick_install_thread_completed)
        thread.start()
    
    def on_quick_install_thread_completed(self, thread):
        """React to the result of `QuickInstallThread'."""
        
        if thread.result == QuickInstallResult.INSTALLED:
            self.install_button.set_label(_("Installed"))
        elif thread.result == QuickInstallResult.ABORTED:
            self.install_button.set_label(_("_Install..."))
            self.install_button.set_property("sensitive", True)
        else:
            # TODO: It should probably say 'could not be installed'.
            self.sabnzbd_pane.set_state(NotFoundState)


class QuickUpgradeState(State):
    """
    If the package 'sabnzbdplus' is installed, but it's version is too low
    and a supported version of the package is available, ask the user to
    install the updates available for the system.
    """
    
    builder_file = "session_selection_local_sabnzbd_pane_quick_upgrade"
    _timer = None
    
    def create_ui(self):
        """Only show the 'Upgrade' button if `QuickUpgradeThread.COMMAND'
        is available. Provide additional instructions otherwise.
        """
        
        if find_executable(QuickUpgradeThread.COMMAND):
            self.upgrade_button_container.show()
        else:
            self.upgrade_label.set_text(self.upgrade_label.get_text() + " " + \
                _("Install the updates available for your system."))
    
    def handle_attached(self, sabnzbd_pane):
        """Regularly check whether the updates have been installed."""
        
        self._timer = Timer(1000, self.on_timer_tick)
        self._timer.start()
    
    def handle_detached(self, sabnzbd_pane):
        """Stop checking whether the updates have been installed."""
        
        self._timer.stop()
    
    def on_upgrade_button__clicked(self, button):
        """Initiate the installation of the update.
        
        Until `QuickUpgradeState' is completed, the `upgrade_button' will be
        made insensitive, preventing the user from clicking it twice.
        """
        
        self.upgrade_button.set_label(_("Updating..."))
        self.upgrade_button.set_property("sensitive", False)
        
        thread = QuickUpgradeThread()
        thread.start()
    
    def on_timer_tick(self, timer):
        """Run an `APTPackageVersionThread' to check whether the updates have
        been installed."""
        
        self.run_apt_version_thread(self.on_apt_version_thread_completed)
    
    def on_apt_version_thread_completed(self, thread):
        """Update the view if the updates have been installed."""
        
        installed_version = thread.installed_version
        
        if installed_version and installed_version >= Interface.MINIMUM_VERSION:
            self._timer.stop()
            self.upgrade_button.set_label(_("Updated"))


class UpgradeState(State):
    """View that asks the user to manually update SABnzbd.
    
    This is the case when the the installed version of SABnzbd is out-of-date,
    but no up-to-date package is available through the package manager.
    """
    
    def create_ui(self):
        markup = _("The program required for downloading is out-of-date.")
        
        self.label = self.create_label()
        self.get_toplevel().add(self.label)
        
        # The signal 'activate-link' is available in GTK >= 2.18.
        try:
            self.label.connect("activate-link", self.on_activate_upgrade_link)
        except TypeError:
            pass
        else:
            markup += " " + _("Learn <a href=''>how to upgrade</a> it.")
        
        self.label.set_markup(markup)
    
    def allows_manual_input(self):
        return True
    
    def on_activate_upgrade_link(self, label, uri):
        webbrowser.open(HELP_URL)


class NotFoundState(State):
    """View that is displayed when the SABnzbd executbale could not be found.
    
    It regularly checks again so that after the installation of SABnzbd, the
    user is not required to manually navigate to '/usr/bin'.
    """
    
    _timer = None
    
    def create_ui(self):
        format = ("", "")
        markup = _("The program required for downloading could not be found. "
            "{0}Install it{1} and specify where it is.")
        
        self.label = self.create_label()
        self.get_toplevel().add(self.label)
        
        # The signal 'activate-link' is available in GTK >= 2.18.
        try:
            self.label.connect("activate-link", self.on_activate_install_link)
        except TypeError:
            pass
        else:
            format = ("<a href=''>", "</a>")
        
        self.label.set_markup(markup.format(*format))
    
    def handle_attached(self, sabnzbd_pane):
        self._timer = Timer(1000, self.on_timer_tick)
        self._timer.start()
    
    def handle_detached(self, sabnzbd_pane):
        self._timer.stop()
    
    def on_timer_tick(self, timer):
        if not self.sabnzbd_pane.config.command:
            command = LocalSession.get_sabnzbd_command()
            
            self.sabnzbd_pane.config.command = command
            self._timer.stop()
    
    def on_activate_install_link(self, label, uri):
        """Open the associated help content in a webbrowser."""
        
        webbrowser.open(HELP_URL)
    
    def allows_manual_input(self):
        return True


class PermissionState(State):
    """View that asks the user to make the SABnzbd program file executable.
    
    This state is activated when a `SessionPermissionError' is raised.
    """
    
    def create_ui(self):
        self.label = self.create_label(_("The program required for downloading "
            "cannot be started. Make sure that it is executable."))
        self.get_toplevel().add(self.label)
    
    def allows_manual_input(self):
        return True
    
    @staticmethod
    def can_handle_session_error(error):
        return isinstance(error, SessionPermissionError)


class ImportState(State):
    """View that informs the user that SABnzbd could not be started because
    a Python module is missing.
    
    This state is activated when a `SessionPermissionError' is raised.
    
    There is some extra logic for the Python module 'cherrypy' as SABnzbd ships
    with it and the 'cherrypy.zip' file in the SABnzbd source tarball needs to
    be extracted before it can be used. Of course, this doesn't apply to
    installations using package managers.
    """
    
    def create_ui(self):
        self.label = self.create_label()
        self.get_toplevel().add(self.label)
    
    def handle_attached(self, sabnzbd_pane):
        markup = _("The program required for downloading could not be started.")
        
        if isinstance(sabnzbd_pane.session_error, SessionImportError):
            if sabnzbd_pane.session_error.module == "cherrypy":
                markup += " " + _("Ensure that cherrypy.zip is extracted.")
            else:
                markup += " " + sabnzbd_pane.session_error.message
        
        self.label.set_markup(markup)
    
    def allows_manual_input(self):
        return True
    
    @staticmethod
    def can_handle_session_error(error):
        return isinstance(error, SessionImportError)


class SABnzbdPane(SlaveDelegate):
    """Takes care of managing all possible states related to the selection
    of the SABnzbd executable.
    
    If no command has yet been specified, it automatically initiates the state
    `SearchingState'.
    
    It provides a conduit `command_conduit' for the SABnzbd executable.
    
    It also takes care of changing to the right state given a `SessionError',
    that can be handled according to `can_handle_session_error'.
    """
    
    builder_file = "session_selection_local_sabnzbd_pane"
    
    STATE_CLASSES = (
        SearchingState,
        QuickInstallState,
        QuickUpgradeState,
        UpgradeState,
        NotFoundState,
        PermissionState,
        ImportState)
    
    def __init__(self, config):
        SlaveDelegate.__init__(self)
        
        self.config = config
        self.states = {}
        self.state = None
        self.session_error = None
        
        self.command_conduit = FileConduit(
            GObjectEndPoint(self.config, "command"),
            FileChooserButtonEndPoint(self.executable))
        
        self.command_conduit.mandatory = True
        
        if not self.config.command:
            self.set_state(SearchingState)
    
    def can_handle_session_error(self, error):
        """Return True if any of the states is capable of handing the
        `SessionError' `error'.
        """
        
        return bool(self.get_session_error_handling_state(error))
    
    def handle_session_error(self, error):
        self.session_error = error
        self.set_state(self.get_session_error_handling_state(error))
    
    def get_session_error_handling_state(self, error):
        """Given a `SessionError' `error', determine the state (class) that can
        handle the error based on the return value of the corresponding
        `can_handle_session_error' method.
        
        Returns None if no state can handle the error.
        """
        
        for state_class in self.STATE_CLASSES:
            if state_class.can_handle_session_error(error):
                return state_class
    
    def set_state(self, state_class):
        """Changes the state to the state of class `state_class'.
        
        If the `allows_manual_input' method provided by the state returns True
        the `FileChooserButton' for selecting the SABnzbd executable is
        made visible or invisible otherwise."""
        
        if not state_class is self.state:
            # Don't instantate the `State's in advance for the reason of
            # efficiency.
            if not state_class in self.states:
                self.states[state_class] = state_class(self)
            
            state = self.states[state_class]
            
            self.detach_slave(self.state_parent)
            self.attach_slave(self.state_parent, state)
            self.executable_container.set_property("visible",
                state.allows_manual_input())
            
            self.state = state_class
