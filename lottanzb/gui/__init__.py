# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import logging
LOG = logging.getLogger(__name__)

import warnings
import gtk

from lottanzb.config import ConfigSection
from lottanzb.config.lotta import ConfigurableComponent
from lottanzb.core.environ import _
from lottanzb.util.gobject_extras import GObject, gproperty
from lottanzb.gui import main, sessions, restart
from lottanzb.backend import Backend
from lottanzb.backend.interface.queries import RestartQuery

# Ignore GTK warnings
warnings.simplefilter("ignore", gtk.Warning)

__all__ = ["GUIComponent"]

class Config(ConfigSection):
    main = gproperty(type=object)
    add_file = gproperty(type=object)


class GUIComponent(ConfigurableComponent):
    def __init__(self, component_manager):
        ConfigurableComponent.__init__(self, component_manager)
        
        self._component_manager.connect_async("component-loading-failure",
            self.on_component_loading_failure)
        self._component_manager.connect_async("component-loaded",
            self.on_component_loaded)
        self._component_manager.connect_async("component-unloaded",
            self.on_component_unloaded)
        
        LOG.debug("Launching GUI...")
        
        gtk.window_set_default_icon_name("lottanzb")
        
        self.main_window = self._component_manager.load(main.MainWindow)
        self.main_window.show()
        
        self._component_manager.load_async(Backend)
    
    def on_component_loading_failure(self, component_manager, component_class,
        error):
        if issubclass(component_class, Backend):
            self.show_session_selection_dialog(error)
    
    def on_component_loaded(self, component_manager, component):
        if isinstance(component, Backend):
            interface = component.interface
            
            self.main_window.attach_to_backend(component)
            
            interface.connect_query_started(RestartQuery, \
                self.on_restart_query_started)
            interface.connect_query_completed(RestartQuery, \
                self.on_restart_query_completed)
            
            # A `RestartQuery' might already have been started even before this
            # signal handler is run. Therefore, check the list of active
            # queries.
            # FIXME: Possible race condition as the access to `active_queries'
            # is not synchronized.
            for query in interface.active_queries:
                if isinstance(query, RestartQuery):
                    self.on_restart_query_started(interface, query)
    
    def on_component_unloaded(self, component_manager, component_class, error):
        if component_class is Backend:
            self.main_window.detach_from_backend()
            
            if error:
                self.show_session_selection_dialog(error)
    
    def show_session_selection_dialog(self, error):
        main_window = self._component_manager.get(main.MainWindow)
        dialog = self._component_manager.load(sessions.SelectionDialog)
        dialog.set_session_error(error)
        dialog.show(main_window.get_toplevel())
    
    def on_restart_query_started(self, interface, query):
        restart_dialog = self._component_manager.load(restart.Dialog)
        
        if self.main_window:
            restart_dialog.show(self.main_window.get_toplevel())
        else:
            restart_dialog.show()
    
    def on_restart_query_completed(self, interface, query):
        self._component_manager.unload(restart.Dialog)
