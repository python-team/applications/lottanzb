# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk
import logging

from logging import getLogger
LOG = getLogger(__name__)

from time import strftime, localtime

from lottanzb.core.environ import _
from lottanzb.core.component import depends_on
from lottanzb.core.log import LoggingComponent
from lottanzb.util.gobject_extras import GObject, gsignal
from lottanzb.util.signalmanager import SignalManager
from lottanzb.backend.hubs.log import LoggingHub
from lottanzb.gui.framework import MainDelegateComponent

__all__ = ["Window"]

# The rows that will be added to the `Window.level_store' `ListStore'.
LEVELS = [
    (logging.DEBUG, _("Debugging")),
    (logging.INFO, _("Default")),
    (logging.WARNING, _("Warnings")),
    (logging.ERROR, _("Errors"))
]

class LevelColumn:
    """Indices of the columns in the `Window.level_store' `ListStore'."""
    CONSTANT = 0
    NAME = 1


class Window(MainDelegateComponent):
    """
    A window displaying log messages by both LottaNZB and SABnzbd.
    
    The source of a message is not displayed anymore and it's not possible to
    filter the messages by source as the user shouldn't need to care about it
    and as LottaNZB only displays SABnzbd warnings anyway, which hopefully is
    rarely the case.
    
    The messages are fetched from the `core.log.LoggingComponent` as well as the
    `backend.hubs.log.LoggingHub', which are wrapped by the classes
    `LottaNZBLogRecordSource' and `SABnzbdLogRecordSource', which share the same
    ancestor `LogRecordSource' such that the access is uniform.
    """
    
    depends_on(LoggingComponent)
    
    builder_file = "log_window"
    textbuffer = None
    adjust = None
    end_mark = None
    
    # The newest record that is currently being displayed.
    last_record = None
    
    # A list of objects of type `LogRecordSource', whose records are meant to
    # be displayed.
    sources = None
    
    def create_ui(self):
        self.textbuffer = self.content.get_buffer()
        self.textbuffer.get_tag_table().add(self.error_tag)
        self.textbuffer.get_tag_table().add(self.debug_tag)
        
        self.adjust = self.scrolled_window.get_vadjustment()
        self.end_mark = self.textbuffer.create_mark("end",
            self.textbuffer.get_end_iter(), False)
        
        logging_component = self._component_manager.get(LoggingComponent)
        
        # Populate the 'Level' combobox.
        for row in LEVELS:
            row_iter = self.level_store.append(row)
            
            # Only show debug messages by default if LottaNZB is actually
            # executed using the --debug argument.
            if logging_component.console_level == row[LevelColumn.CONSTANT]:
                self.level.set_active_iter(row_iter)
        
        # Setup all sources and connect to their signals.
        self.sources = []
        
        for source_class in (LottaNZBLogRecordSource, SABnzbdLogRecordSource):
            source = source_class(self._component_manager)
            source.connect("records-added", self.on_records_added)
            source.connect("records-cleared", self.on_records_cleared)
            
            self.sources.append(source)
        
        # The default behaviour. The user is always free to scroll upwards,
        # which will cause the log window to stay there even if new records are
        # added.
        self.scroll_to_bottom()
        
        # Fill the content pane.
        self.refresh()
    
    def display_record(self, record):
        """Append `record' to the bottom of the `textbuffer'.
        
        The record doesn't need to be of time `logging.LogRecord', but should at
        least have a similar interface, including the properties
        `levelno', `created' and `getMessage().
        
        Records with a level that is lower than the one returned by `get_level()
        will be discarded.
        """
        
        if record.levelno < self.get_level():
            return
        
        line = "{time}\t{message}\n".format(
            time=strftime("%H:%M:%S", localtime(record.created)),
            message=record.getMessage())
        
        if record.levelno >= logging.WARNING:
            tag = "error_tag"
        elif record.levelno == logging.DEBUG:
            tag = "debug_tag"
        else:
            tag = ""
        
        end_iter = self.textbuffer.get_end_iter()
        
        if tag:
            self.textbuffer.insert_with_tags_by_name(end_iter, line, tag)
        else:
            self.textbuffer.insert(end_iter, line)
        
        # Auto-scroll the log message `TextView` if the user has manually moved
        # the scrollbar to the bottom.
        if self.adjust.value + self.adjust.page_size == self.adjust.upper:
            self.scroll_to_bottom()
        
        self.last_record = record
    
    def get_level(self):
        """The current level constant selected by the user."""
        active_iter = self.level.get_active_iter()
        return self.level_store[active_iter][LevelColumn.CONSTANT]
    
    def refresh(self):
        """Clear `textbuffer' and display all records provided by `sources'."""
        self.textbuffer.set_text("")
        self.adjust.set_value(0)
        self.last_record = None
        
        # Holds source with records that have not been displayed yet
        active_sources = [source for source in self.sources if source.records]
        
        # Mapping from sources to indices that indicate the next record to
        # be displaced for that particular source
        indices = dict()
        
        # Start with the first record for each source
        for source in active_sources:
            indices[source] = 0
        
        while active_sources:
            # Determine the source whose next record is the oldest
            next_source = active_sources[0]
            next_record = next_source.records[indices[next_source]]
            
            for source in active_sources:
                if source.records[indices[source]].created < next_record.created:
                    next_source = source
                    next_record = source.records[indices[source]]
            
            self.display_record(next_record)
            indices[next_source] += 1
            
            # Remove the source from `active_sources' if all of its records
            # have been displayed
            if len(next_source.records) == indices[next_source]:
                active_sources.remove(next_source)
                del indices[next_source]
    
    def scroll_to_bottom(self):
        self.content.scroll_to_mark(self.end_mark, 0.05, True, 0.0, 1.0)
    
    def on_records_added(self, record_source, records):
        """Display all `records'.
        
        It might be necessary to refresh the whole `textbuffer' if the records
        are out-of-order.
        """
        
        if not records:
            return
        
        # Check if the records are out-of-order, meaning that the newest
        # displayed record is older than the oldest record to be displayed.
        if self.last_record and records[0].created < self.last_record.created:
            self.refresh()
            return
        
        for record in records:
            self.display_record(record)
    
    def on_records_cleared(self, record_source):
        """Refresh the whole content pane."""
        self.refresh()
    
    def on_save__clicked(self, widget):
        """Let the user save the log messages to a file."""
        def handle_response(dialog, response):
            if response == gtk.RESPONSE_OK:
                filename = dialog.get_filename()
                start_iter = self.textbuffer.get_start_iter()
                end_iter = self.textbuffer.get_end_iter()
                content = self.textbuffer.get_text(start_iter, end_iter)
                
                try:
                    logfile = open(filename, "w")
                    logfile.write(content)
                    logfile.close()
                except IOError as error:
                    LOG.error(_("Could not write message log to %s: %s") % 
                        (filename, error.strerror))
                else:
                    LOG.info(_("Message log saved to %s.") % (filename))
            
            dialog.destroy()
        
        dialog = gtk.FileChooserDialog(_("Save Message Log"),
            self.get_toplevel(), gtk.FILE_CHOOSER_ACTION_SAVE,
            (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_SAVE,
            gtk.RESPONSE_OK))
        
        dialog.connect("response", handle_response)
        dialog.set_default_response(gtk.RESPONSE_OK)
        dialog.show()
    
    def on_clear__clicked(self, widget):
        """Clear all `sources'."""
        for source in self.sources:
            source.clear()
    
    def on_level__changed(self, widget):
        """Refresh the whole content pane."""
        self.refresh()


class LogRecordSource(GObject):
    gsignal("records-added", object)
    gsignal("records-cleared")
    
    def __init__(self, component_manager):
        self._component_manager = component_manager
        self.records = []
        
        GObject.__init__(self)
    
    def clear(self):
        self.records = []
        self.emit("records-cleared")


class LottaNZBLogRecordSource(LogRecordSource):
    def __init__(self, component_manager):
        LogRecordSource.__init__(self, component_manager)
        
        logging_component = self._component_manager.load(LoggingComponent)
        logging_component.connect("record-added", self.on_log_record_added)
        
        self.records = logging_component.records
    
    def on_log_record_added(self, logging_component, log_record):
        self.records.append(log_record)
        self.emit("records-added", [log_record])


class SABnzbdLogRecordSource(LogRecordSource):
    def __init__(self, component_manager):
        LogRecordSource.__init__(self, component_manager)
        
        self._signal_manager = SignalManager()
        self._component_manager.connect_async("component-loaded", \
            self.on_component_loaded)
        self._component_manager.connect_async("component-unloaded", \
            self.on_component_unloaded)
        
        logging_hub = self._component_manager.get(LoggingHub)
        
        if logging_hub:
            self.on_component_loaded(component_manager, logging_hub)
    
    def on_component_loaded(self, component_manager, component):
        if isinstance(component, LoggingHub):
            self._signal_manager.connect(component, "records-added", \
                self.on_log_records_added)
            self._signal_manager.connect(component, "records-cleared", \
                self.on_records_cleared)
            
            self.on_log_records_added(component, component.records)
    
    def on_component_unloaded(self, component_manager, component_class, error):
        if component_class is LoggingHub:
            self._signal_manager.disconnect_all()
            self.clear()
    
    def on_log_records_added(self, logging_hub, log_records):
        self.records.extend(log_records)
        self.emit("records-added", log_records)
    
    def on_records_cleared(self, logging_hub):
        self.records = []
        self.emit("records-cleared")
    
    def clear(self):
        logging_hub = self._component_manager.get(LoggingHub)
        
        if logging_hub:
            logging_hub.clear()
        else:
            LogRecordSource.clear(self)
