# Copyright (C) 2010-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gobject

from lottanzb.util.gtk_extras import gtk_lock
from lottanzb.gui.framework import MainDelegateComponent

class Dialog(MainDelegateComponent):
    builder_file = "restart_dialog"
    unloaded = False
    
    def create_ui(self):
        gobject.timeout_add(40, self.on_progress_timeout)
    
    def on_progress_timeout(self):
        with gtk_lock:
            self.progress.pulse()
        
        return not self.unloaded
        
    def on_unload(self):
        self.unloaded = True
        
        MainDelegateComponent.on_unload(self)
