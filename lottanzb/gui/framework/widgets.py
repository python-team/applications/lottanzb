# Copyright (C) 2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk

from lottanzb.util.gobject_extras import gproperty, gsignal

__all__ = ["MenuButton"]

class MenuButton(gtk.ToggleButton):
    gsignal("show-menu")
    
    _menu_deactivate_id = -1
    _menu = None
    
    def set_menu(self, menu):
        if not menu and not isinstance(menu, gtk.Menu):
            raise ValueError("expected gtk.Menu, got %s", type(menu))
        
        if self._menu is not menu:
            if self._menu and self._menu.get_visible():
                self._menu.deactivate()
            
            if self._menu:
                self._menu.disconnect(self._menu_deactivate_id)
                self._menu.detach()
            
            self._menu = menu
            
            if self._menu:
                self.set_sensitive(True)
                self._menu.attach_to_widget(self, self._menu_detach_func)
                self._menu_deactivate_id = self._menu.connect("deactivate",
                    self._on_menu_deactivate)
            else:
                self.set_sensitive(False)
        
        self.notify("menu")
    
    def get_menu(self):
        return self._menu
    
    menu = gproperty(type=object, getter=get_menu, setter=set_menu)
    
    def __init__(self, label=None, use_underline=True):
        gtk.ToggleButton.__init__(self, label, use_underline)
        
        self.connect("state-changed", self._on_state_changed)
        self.connect("toggled", self._on_toggled)
        self.connect("button-press-event", self._on_button_press_event)
        
        # TODO/heseveri: Need to disconnect signal handlers and detach menu when
        # the widget is destroyed?
    
    def _popup_menu_under_button(self, event=None):
        self.emit("show-menu")
        
        if not self._menu:
            return
        
        if event is None:
            button = 0
            time = gtk.get_current_event_time()
        else:
            button = event.button
            time = event.time
        
        self._menu.popup(None, None, self._menu_position_func, button, time)
    
    def _menu_position_func(self, button):
        direction = self.get_direction()
        menu_req_width, menu_req_height = self._menu.size_request()
        
        x, y = self.window.get_origin()
        x += self.allocation.x
        y += self.allocation.y + self.allocation.height
        
        return x, y, True
    
    def _menu_detach_func(self, menu, widget):
        self._menu = None
    
    def _on_state_changed(self, widget, state):
        if not self.is_sensitive() and self._menu:
            self._menu.deactivate()
    
    def _on_toggled(self, widget):
        if not self._menu:
            return
        
        if self.get_active() and not self._menu.get_visible():
            # We get here only when a menu is activated by a key press, so that
            # we can select the first menu item
            self._popup_menu_under_button()
            self._menu.select_first(False)
    
    def _on_button_press_event(self, widget, event):
        if event.button == 1:
            self._popup_menu_under_button()
            self.set_active(True)
            return True
        else:
            return False
    
    def _on_menu_deactivate(self, menu):
        self.set_active(False)
