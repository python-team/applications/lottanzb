# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""
Various functions that run simple message dialogs based on `gtk.MessageDialog`.

Code partially coped from Kiwi: Copyright (C) 2005-2007 Async Open Source
"""

import gtk

__all__ = ["error", "info", "warning", "yesno"]

_BUTTONS = (gtk.BUTTONS_NONE, gtk.BUTTONS_OK, gtk.BUTTONS_CLOSE,
     gtk.BUTTONS_CANCEL, gtk.BUTTONS_YES_NO, gtk.BUTTONS_OK_CANCEL)

def _messagedialog(dialog_type, title, message=None, parent=None,
    buttons=gtk.BUTTONS_OK, default=gtk.RESPONSE_NONE):
    if buttons not in _BUTTONS:
        buttons = gtk.BUTTONS_NONE
    
    if parent and not isinstance(parent, gtk.Window):
        raise TypeError("parent must be a gtk.Window subclass")
    
    dialog = gtk.MessageDialog(flags=gtk.DIALOG_MODAL, type=dialog_type,
        buttons=buttons)
    dialog.set_property("text", title)
    
    if message:
        if isinstance(message, basestring):
            dialog.format_secondary_text(message)
        else:
            raise TypeError("message must be a string, not %r" % message)
    
    if default != gtk.RESPONSE_NONE:
        dialog.set_default_response(default)
    
    if parent:
        dialog.set_transient_for(parent)
        dialog.set_modal(True)
    
    response = dialog.run()
    dialog.destroy()
    
    return response

def _simple(message_type, title, message=None, parent=None, buttons=gtk.BUTTONS_OK,
    default=gtk.RESPONSE_NONE):
    if buttons == gtk.BUTTONS_OK:
        default = gtk.RESPONSE_OK
    
    return _messagedialog(message_type, title, message, parent=parent,
        buttons=buttons, default=default)

def error(title, message=None, parent=None, buttons=gtk.BUTTONS_OK,
    default=gtk.RESPONSE_NONE):
    return _simple(gtk.MESSAGE_ERROR, title, message, parent=parent,
       buttons=buttons, default=default)

def info(title, message=None, parent=None, buttons=gtk.BUTTONS_OK,
    default=gtk.RESPONSE_NONE):
    return _simple(gtk.MESSAGE_INFO, title, message, parent=parent,
       buttons=buttons, default=default)

def warning(title, message=None, parent=None, buttons=gtk.BUTTONS_OK,
    default=gtk.RESPONSE_NONE):
    return _simple(gtk.MESSAGE_WARNING, title, message, parent=parent,
       buttons=buttons, default=default)

def yesno(text, parent=None, default=gtk.RESPONSE_YES,
    buttons=gtk.BUTTONS_YES_NO):
    return _messagedialog(gtk.MESSAGE_WARNING, text, None, parent,
         buttons=buttons, default=default)
