# Copyright (C) 2010-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk
import webbrowser
import locale

from os.path import isdir
from subprocess import Popen

from lottanzb.resources import is_installed, get_help

__all__ = ["open_help"]

def open_help(topic=""):
    """Display the application help content.
    
    'yelp' is used for displaying it if its installed. Otherwise, the function
    falls back to the LottaNZB website.
    
    The optional `topic' argument can be used to specify a topic ID of the
    Mallard-based documentation. Omitting it will cause the help overview to
    be displayed.
    """
    
    try:
        if not is_installed():
            locales = ["C"]
            current_locale = locale.getlocale()[0]
            
            if isinstance(current_locale, str) and current_locale != "C":
                locales = [current_locale] + locales
            
            for help_dir in [get_help(l) for l in locales]:
                if isdir(help_dir):
                    Popen(["yelp", ".#" + topic], cwd=help_dir)
                    return
        else:
            default_screen = gtk.gdk.screen_get_default()
            event_time = gtk.get_current_event_time()
            
            gtk.show_uri(default_screen, "ghelp:lottanzb#" + topic, event_time)
    except:
        webbrowser.open_new_tab("http://www.lottanzb.org/help/%s" % topic)
