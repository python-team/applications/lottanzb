# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk
import sys
import atexit

import logging
LOG = logging.getLogger(__name__)

from optparse import OptionParser
from os.path import abspath

from lottanzb import __version__
from lottanzb.core.component import ComponentManager
from lottanzb.core import environ
from lottanzb.core.environ import _

class App(ComponentManager):
    dbus_service = None
    
    def run(self):
        try:
            self.quit(self.main())
        except KeyboardInterrupt:
            raise SystemExit
    
    def main(self):
        parser = OptionParser(
            usage="%prog [FILES...]",
            version="LottaNZB %s" % __version__
        )
        
        parser.add_option("-d", "--debug", action="store_true", dest="debug",
            help="show debug messages", default=False)
        
        options, nzb_files = parser.parse_args()
        nzb_files = [abspath(nzb_file) for nzb_file in nzb_files]
        
        environ.setup_translation()
        environ.setup_resources()
        environ.setup_thread_excepthook()
        
        # Lazy import
        from lottanzb.core.ipc import DBusService, AlreadyRunningException
        
        # Check if another instance of LottaNZB is already running.
        try:
            self.dbus_service = DBusService(self)
        except AlreadyRunningException:
            # Connect to the already running instance of LottaNZB.
            # Present the main window and enqueue NZB files passed as command
            # line parameters.
            interface = DBusService.get_remote_interface()
            
            if nzb_files:
                interface.add_files(nzb_files)
            
            interface.present()
            
            # This instance of LottaNZB is not required anymore.
            return 0
        else:
            # Enqueue NZB files passed as command line parameters.
            if nzb_files:
                self.dbus_service.add_files(nzb_files)
        
        atexit.register(self.on_quit)
        
        # Lazy import
        from lottanzb.core.log import LoggingComponent
        from lottanzb.gui import GUIComponent
        
        logging_component = self.load(LoggingComponent)
        
        if options.debug:
            logging_component.set_console_level(logging.DEBUG)
        
        LOG.info(_("Starting LottaNZB %s...") % __version__)
        
        self.load(GUIComponent)
        
        gtk.main()
        
        return 0
    
    def quit(self, code=0):
        sys.exit(code)
    
    def on_quit(self):
        self.unload_all()
        
        LOG.info(_("Exiting..."))
