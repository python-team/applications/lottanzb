
# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import dbus.service
import dbus.mainloop.glib

import logging
LOG = logging.getLogger(__name__)

from threading import Lock

from lottanzb.core.environ import _
from lottanzb.backend.hubs.general import GeneralHub
from lottanzb.gui.main import MainWindow

class AlreadyRunningException(Exception):
    def __str__(self):
        return _("LottaNZB is already running on this computer.")


class DBusService(dbus.service.Object):
    PATH = "/org/lottanzb"
    NAMESPACE = "org.lottanzb"
    
    def __init__(self, component_manager):
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        
        # Check if LottaNZB is already running.
        bus = dbus.SessionBus()
        obj = bus.get_object("org.freedesktop.DBus", "/org/freedesktop/DBus")
        interface = dbus.Interface(obj, "org.freedesktop.DBus")
        
        if interface.NameHasOwner(self.NAMESPACE):
            raise AlreadyRunningException()
        
        LOG.debug("Setting up D-Bus interface...")
        
        bus_name = dbus.service.BusName(self.NAMESPACE, bus=dbus.SessionBus())
        
        dbus.service.Object.__init__(self, bus_name, self.PATH)
        
        # Queue that stores the name of all NZB files that couldn't be enqueued
        # so far, probably because LottaNZB isn't connected to an instance of
        # SABnzbd yet.
        self._nzb_queue = []
        
        # Make the access to the above queue mutually exclusive.
        self._nzb_lock = Lock()
        
        # Wait for the `backend.hubs.general.GeneralHub` to be loaded so that
        # the NZB files in `_nzb_queue` can be encoded.
        def on_loaded(*args):
            self._try_to_add_files()
        
        self._component_manager = component_manager
        self._component_manager.connect_async("component-loaded", on_loaded)
    
    @dbus.service.method(dbus_interface=NAMESPACE)
    def present(self):
        """Bring the main window to the front."""
        
        main_window = self._component_manager.get(MainWindow)
        
        if main_window:
            main_window.show()
    
    @dbus.service.method(dbus_interface=NAMESPACE, in_signature="as")
    def add_files(self, files):
        """Enqueue an array of NZB files."""
        
        self._try_to_add_files(files)
    
    def _try_to_add_files(self, files=None):
        """
        Try to enqueue the files passed as an argument and those in `nzb_queue`.
        
        If the `backend.hubs.general.GeneralHub` is ready, enqueue all files.
        Add the files passed as an argument to the `nzb_queue` otherwise.
        """
        
        general_hub = self._component_manager.get(GeneralHub)
        
        with self._nzb_lock:
            if files:
                self._nzb_queue.extend(files)
            
            if general_hub:
                for a_file in self._nzb_queue:
                    general_hub.add_file(a_file)
                
                self._nzb_queue = []
    
    @classmethod
    def get_remote_interface(cls):
        """
        Get the interface that can be used to call D-Bus methods of an already
        running instance of LottaNZB.
        """
        
        bus = dbus.SessionBus()
        proxy = bus.get_object(cls.NAMESPACE, cls.PATH)
        interface = dbus.Interface(proxy, cls.NAMESPACE)
        
        return interface
