# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import locale
import gettext
import threading
import sys

import logging
LOG = logging.getLogger(__name__)

from shutil import move
from os.path import isdir, expanduser

from lottanzb import resources

__all__ = ["DOMAIN", "_", "setup_resources", "setup_translation",
    "setup_thread_excepthook"]

DOMAIN = "lottanzb"

_ = lambda message: gettext.dgettext(DOMAIN, message)

def setup_resources():
    old_config_dir = expanduser("~/.lottanzb")
    new_config_dir = resources.get_config()
    
    if isdir(old_config_dir) and not isdir(new_config_dir):
        LOG.info(_("Moving configuration directory from %s to %s..."),
            old_config_dir, new_config_dir)
        
        move(old_config_dir, new_config_dir)
    
    resources.create_user_dirs()

def setup_translation():
    # Python's 'locale' module doesn't provide all methods on some operating
    # systems like FreeBSD.
    # The exceptions blocks will prevent the application from crashing in this
    # case. However, some menu entries will not be translated.
    
    try:
        # TODO: Is this necessary at all?
        locale.setlocale(locale.LC_ALL, "")
    except AttributeError:
        pass
    
    for module in (gettext, locale):
        try:
            module.bindtextdomain(DOMAIN, resources.get_locale())
            module.bind_textdomain_codeset(DOMAIN, "UTF-8")
            module.textdomain(DOMAIN)
        except AttributeError:
            pass

def setup_thread_excepthook():
    """Work-around for `sys.excepthook' thread bug
    http://bugs.python.org/issue1230540.
    
    Based on:
    http://spyced.blogspot.com/2007/06/workaround-for-sysexcepthook-bug.html
    """
    
    init_old = threading.Thread.__init__
    
    def init(self, *args, **kwargs):
        init_old(self, *args, **kwargs)
        run_old = self.run
        
        def run_with_except_hook(*args, **kw):
            try:
                run_old(*args, **kw)
            except (KeyboardInterrupt, SystemExit):
                raise
            except:
                sys.excepthook(*sys.exc_info())
        
        self.run = run_with_except_hook
    
    threading.Thread.__init__ = init
