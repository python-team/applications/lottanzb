#!/usr/bin/env python

# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import os

from glob import glob
from imp import find_module, load_module
from subprocess import call
from os.path import join, isfile, exists, isdir, dirname, splitext, basename

from distutils import log
from distutils.core import Command
from distutils.dir_util import remove_tree
from distutils.errors import DistutilsFileError, DistutilsArgError
from distutils.command.install_lib import install_lib
from distutilsextra.auto import (setup, clean_build_tree, install_auto,
    sdist_auto)

from unittest import TextTestRunner, TestLoader

from lottanzb import __version__ as version

def remove(path, dry_run=False):
    """
    Remove a certain file or directory.
    
    Similar to the remove_tree function provided by the distutils module,
    it has an optional dry_run parameter. If set to true,
    no files are removed at all.
    """
    
    if not exists(path):
        log.debug("%s does not exist.", path)
        return
    
    if isfile(path):
        try:
            log.info("removing %s", path)
            
            if not dry_run:
                os.remove(path)
        except OSError:
            raise DistutilsFileError("Unable to remove %s.", path)
    elif isdir(path):
        remove_tree(path, dry_run=dry_run)


class CleanCommand(clean_build_tree):
    def run(self):
        clean_build_tree.run(self)
        
        # Remove pyc files.
        for root, dirs, files in os.walk("."): # @UnusedVariable
            for a_file in files:
                if a_file.endswith(".pyc") or a_file.endswith("~"):
                    self.remove(join(root, a_file))
        
        # The 'dist' directory contains a clean LottaNZB branch after running
        # 'util/release'. Running 'python setup.py install' afterwards will
        # fail because of that.
        self.remove("dist")
        self.remove("MANIFEST")
        self.remove(".xml2po.mo")
        self.remove("po/POTFILES.in")
    
    def remove(self, path):
        """
        Wraps the remove function, so that the dry_run parameter doesn't need
        to be set whenever it's called.
        """
        
        remove(path, self.dry_run)


class InstallWrapper(install_auto):
    """
    Doesn't contain much more than the complete_installation method, used to
    complete the GNOME integration or to reverse it, respectively.
    """
    
    DEFAULT_RECORD_FILE = "INSTALLED_FILES"
    
    def complete_installation(self):
        """
        Try to register LottaNZB as an application used to handle NZB files,
        make sure NZB files and the LottaNZB menu entry get a shiny icon
        and register the help content.
        """
        
        mime_dir = join(self.install_data, "share", "mime")
        icon_dir = join(self.install_data, "share", "icons", "hicolor")
        
        def try_to_call(cmd):
            try:
                if not self.dry_run:
                    call(cmd)
            except OSError:
                log.warn("Could not call %s.", cmd)
        
        log.info("updating MIME types")
        try_to_call(["update-mime-database", mime_dir])
        
        log.info("updating desktop database")
        try_to_call(["update-desktop-database"])
        
        log.info("updating icon cache %s", icon_dir)
        try_to_call(["gtk-update-icon-cache", icon_dir])
     
    def remove(self, path):
        """
        Wraps the remove function, so that the dry_run parameter doesn't need
        to be set whenever it's called.
        """
        
        remove(path, self.dry_run)


class InstallCommand(InstallWrapper):
    user_options = InstallWrapper.user_options + [
        ("packaging-mode", "p", "don't perform post-installation operations"),
        ("upgrade", "u", "upgrade existing installations without confirmation")
    ]
    
    boolean_options = InstallWrapper.boolean_options + [
        "packaging-mode", "upgrade"
    ]
    
    def initialize_options(self):
        self.packaging_mode = False
        self.upgrade = False
        self.record = None
        
        InstallWrapper.initialize_options(self)
    
    def finalize_options(self):
        if self.upgrade:
            self.force = True
        
        if not self.record and not self.packaging_mode:
            self.record = self.DEFAULT_RECORD_FILE
        
        # Don't install the application to /usr/local on Debian/Ubuntu.
        # The problem is that numerous things related to the GNOME desktop
        # integration will cease to work otherwise.
        if not getattr(self, "install_layout", None):
            self.install_layout = "deb"
        
        InstallWrapper.finalize_options(self)
    
    def run(self):
        if not self.packaging_mode:
            self.upgrade_existing_installation()
        
        InstallWrapper.run(self)
        
        if not self.packaging_mode:
            self.complete_installation()
    
    def upgrade_existing_installation(self):
        """
        Looks for exiting installations of LottaNZB in the target installation
        directory, which is usually /usr/lib/python2.x/site-packages.
        
        If the user didn't specify the force or upgrade flag explicitly,
        request a confirmation for the removal of all found installation dirs.
        
        Data files aren't removed, but overwritten by setting the force flag.
        """
        
        lotta_dirs = glob(join(self.install_purelib, "lottanzb*"))
        name = self.distribution.get_name()
        installed_version = ""
        
        try:
            module_info = find_module(name, [self.install_purelib])
            module = load_module(name, *module_info)
        except ImportError:
            pass
        else:
            if hasattr(module, "__version__"):
                installed_version = module.__version__
            else:
                installed_version = "<= 0.3"
        
        if installed_version:
            log.info("An existing installation of LottaNZB %s has been "
                "detected on your system.", installed_version)
            log.info("To avoid conflicts with the new version, the following "
                "folders will be removed:")
            
            for lotta_dir in lotta_dirs:
                log.info(" * %s", lotta_dir)
            
            if not self.upgrade and not self.force:
                selection = raw_input("\nWould you like to upgrade to LottaNZB "
                    "%s? [Y/n]: " % version)
                
                if selection and selection.lower() != "y":
                    raise DistutilsArgError("Aborting installation...")
            
            log.info("Upgrading to LottaNZB %s...", version)
            
            for lotta_dir in lotta_dirs:
                self.remove(lotta_dir)
            
            self.force = True


class InstallLibCommand(install_lib):
    def get_outputs(self):
        files = install_lib.get_outputs(self)
        files.extend([self.platform_file_name])
        files.extend(self._bytecode_filenames([self.platform_file_name]))
        
        return files
    
    def install(self):
        files = install_lib.install(self)
        
        log.info("creating platform file %s", self.platform_file_name)
        
        if not self._dry_run:
            self.mkpath(dirname(self.platform_file_name))
            
            name = self.distribution.get_name()
            
            platform_file = open(self.platform_file_name, "w")
            platform_in = open(self.platform_file_name_in, "r").read()
            
            install_command = self.distribution.get_command_obj("install")
            data_dir = join(install_command.install_data, "share")
            
            # Make sure that the platform file contains the paths where the
            # built package will be installed to and not the ones of the
            # build environment.
            usr_index = data_dir.find("/usr")
            
            if usr_index != -1:
                data_dir = data_dir[usr_index:]
            
            replacement_map = {
                "{DATA_DIR}": join(data_dir, name),
                "{HELP_DIR}": join(data_dir, "gnome", "help", name),
                "{LOCALE_DIR}": join(data_dir, "locale")
            }
            
            for key, value in replacement_map.items():
                platform_in = platform_in.replace(key, value)
            
            platform_file.write(platform_in)
            platform_file.close()
        
        return files
    
    @property
    def platform_file_name(self):
        name = self.distribution.get_name()
        
        return join(self.install_dir, name, "resources/platform.py")
    
    @property
    def platform_file_name_in(self):
        name = self.distribution.get_name()
        
        return join(name, "resources/platform.py.in")


class UpgradeCommand(InstallCommand):
    description = "upgrade existing installation"
    
    def finalize_options(self):
        InstallCommand.finalize_options(self)
        
        self.upgrade = True


class UninstallCommand(InstallWrapper):
    description = "will uninstall the installed package"
    
    def finalize_options(self):
        InstallWrapper.finalize_options(self)
        
        if not self.record:
            self.record = self.DEFAULT_RECORD_FILE
    
    def run(self):
        try:
            files = open(self.record, "r").readlines()
        except:
            raise DistutilsFileError("Could not find list of installed files: "
                "%s" % self.record)
        
        for a_file in [a_file.strip() for a_file in files]:
            self.remove(a_file)
        
        self.remove(self.record)
        self.complete_installation()
        
        log.info("\nLottaNZB has been uninstalled successfully.")


class TestCommand(Command):
    description = "run all tests in the tests directory"
    user_options = []
    
    def initialize_options(self):
        self._dir = os.getcwd()
    
    def finalize_options(self):
        pass
    
    def run(self):
        testfiles = []
        
        for t in glob(join(self._dir, "tests", "*.py")):
            if not t.endswith("__init__.py"):
                testfiles.append(".".join(["tests", splitext(basename(t))[0]]))
        
        tests = TestLoader().loadTestsFromNames(testfiles)
        
        t = TextTestRunner(verbosity=1)
        t.run(tests)


class SourceDistributionCommand(sdist_auto):
    filter_prefix = sdist_auto.filter_prefix + [
        "help/po", "util", ".pydevproject", ".project", "tests"]
    
    def add_defaults(self):
        sdist_auto.add_defaults(self)
        
        self.filelist.append("lottanzb/resources/platform.py.in")


options = {
    "name"             : "lottanzb",
    "version"          : version,
    "description"      : "LottaNZB - Usenet Downloader",
    "long_description" : ("LottaNZB aims to simplify and automate the download "
                          "of binary news from the Usenet. You can tell "
                          "LottaNZB what to download using NZB files, which "
                          "are created by many Usenet search engines. LottaNZB "
                          "integrates nicely with GNOME desktops, but is not "
                          "limited to them and uses the mature SABnzbd "
                          "software as its foundation."),
    "author"           : "LottaNZB Development Team",
    "author_email"     : "avirulence@lottanzb.org",
    "url"              : "http://www.lottanzb.org/",
    "license"          : "GPL",
    "keywords"         : ["usenet", "nzb", "download", "sabnzbd", "frontend",
                          "gtk"],
    "provides"         : ["lottanzb"],
    "requires"         : ["configobj", "dbus", "gtk"],
    "packages"         : [root for root, dirs, files in os.walk("lottanzb")],
    "cmdclass"         : {
                            "clean": CleanCommand,
                            "install": InstallCommand,
                            "install_lib": InstallLibCommand,
                            "uninstall": UninstallCommand,
                            "upgrade": UpgradeCommand,
                            "test": TestCommand,
                            "sdist": SourceDistributionCommand
                         },
    "classifiers"      : [
                            "Development Status :: 5 - Production/Stable",
                            "Environment :: X11 Applications :: Gnome",
                            "Environment :: X11 Applications :: GTK",
                            "Intended Audience :: End Users/Desktop",
                            "License :: OSI Approved :: "
                            "GNU General Public License (GPL)",
                            "Operating System :: POSIX :: Linux",
                            "Programming Language :: Python",
                            "Topic :: Communications :: Usenet News"
                         ]
}

setup(**options)
