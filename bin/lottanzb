#!/usr/bin/env python

import sys

def die(message):
    """
    Try to display a message if the software requirements are not satisfied
    and shut down the application afterwards.
    """
    
    try:
        from subprocess import call
        call(["zenity", "--error", "--text", message])
    except (ImportError, OSError):
        pass
    
    sys.stderr.write(message)
    sys.exit(1)

if sys.version_info < (2, 6):
    die("LottaNZB requires Python 2.6 or greater.")

try:
    import dbus
except ImportError:
    die("LottaNZB requires the Python module 'dbus'.")

try:
    import pygtk
    pygtk.require("2.0")
    
    import gtk
    assert gtk.pygtk_version[:2] >= (2, 16)
except (ImportError, AssertionError):
    die("LottaNZB requires PyGTK 2.16 or greater.")

from lottanzb.core import App

if __name__ == "__main__":
    app = App()
    app.run()
